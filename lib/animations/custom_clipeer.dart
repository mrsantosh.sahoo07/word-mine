import 'package:flutter/material.dart';

class PopUpCustomPainter extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.03213697, size.height * 0.2197551);
    path_0.cubicTo(
        size.width * 0.01657449,
        size.height * 0.3666610,
        size.width * 0.005394318,
        size.height * 0.5803664,
        size.width * 0.02652878,
        size.height * 0.7759041);
    path_0.cubicTo(
        size.width * 0.03644864,
        size.height * 0.8676849,
        size.width * 0.09254293,
        size.height * 0.9316712,
        size.width * 0.1576665,
        size.height * 0.9405788);
    path_0.cubicTo(
        size.width * 0.3224119,
        size.height * 0.9631199,
        size.width * 0.5814491,
        size.height * 0.9871849,
        size.width * 0.8146526,
        size.height * 0.9759281);
    path_0.cubicTo(
        size.width * 0.8882854,
        size.height * 0.9723733,
        size.width * 0.9494913,
        size.height * 0.8976815,
        size.width * 0.9601365,
        size.height * 0.7968185);
    path_0.cubicTo(
        size.width * 0.9807246,
        size.height * 0.6017945,
        size.width * 0.9959851,
        size.height * 0.3382086,
        size.width * 0.9663176,
        size.height * 0.1695897);
    path_0.cubicTo(
        size.width * 0.9537146,
        size.height * 0.09795137,
        size.width * 0.9062357,
        size.height * 0.05751336,
        size.width * 0.8571191,
        size.height * 0.05005000);
    path_0.cubicTo(
        size.width * 0.6687618,
        size.height * 0.02142829,
        size.width * 0.3560099,
        size.height * -0.002315007,
        size.width * 0.1399821,
        size.height * 0.05882842);
    path_0.cubicTo(
        size.width * 0.08227940,
        size.height * 0.07516027,
        size.width * 0.04054069,
        size.height * 0.1404253,
        size.width * 0.03213697,
        size.height * 0.2197551);
    path_0.close();

    Paint paint_0_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.02977667;
    paint_0_stroke.color = const Color(0xffFACECE).withOpacity(1.0);
    // canvas.drawPath(path_0, paint_0_stroke);

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = const Color(0xffFACECE).withOpacity(1.0);
    // canvas.drawPath(path_0, paint_0_fill);
    return path_0;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}

class MyCustomClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.9426014, size.height * 0.04340955);
    path_0.cubicTo(
        size.width * 0.7005524,
        size.height * -0.007731376,
        size.width * 0.2149678,
        size.height * -0.01920256,
        size.width * 0.05054266,
        size.height * 0.03908989);
    path_0.cubicTo(
        size.width * 0.03059105,
        size.height * 0.04616320,
        size.width * 0.01591622,
        size.height * 0.07827528,
        size.width * 0.01261404,
        size.height * 0.1184222);
    path_0.cubicTo(
        size.width * -0.008969860,
        size.height * 0.3808315,
        size.width * 0.001256766,
        size.height * 0.6905140,
        size.width * 0.01291757,
        size.height * 0.8717725);
    path_0.cubicTo(
        size.width * 0.01597007,
        size.height * 0.9192191,
        size.width * 0.03431189,
        size.height * 0.9561854,
        size.width * 0.05792462,
        size.height * 0.9624944);
    path_0.cubicTo(
        size.width * 0.3121734,
        size.height * 1.030424,
        size.width * 0.7266168,
        size.height * 0.9918961,
        size.width * 0.9442084,
        size.height * 0.9576376);
    path_0.cubicTo(
        size.width * 0.9689510,
        size.height * 0.9537416,
        size.width * 0.9894168,
        size.height * 0.9172416,
        size.width * 0.9924601,
        size.height * 0.8677697);
    path_0.cubicTo(
        size.width * 1.006147,
        size.height * 0.6452725,
        size.width * 0.9986182,
        size.height * 0.3347500,
        size.width * 0.9901678,
        size.height * 0.1410601);
    path_0.cubicTo(
        size.width * 0.9879245,
        size.height * 0.08963624,
        size.width * 0.9681622,
        size.height * 0.04881039,
        size.width * 0.9426014,
        size.height * 0.04340955);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = const Color(0xffFACECE).withOpacity(1.0);
    // canvas.drawPath(path_0, paint_0_fill);
    return path_0;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return false;
  }
}
