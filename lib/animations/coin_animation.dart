import 'dart:math';
import 'package:flutter/material.dart';

class AtoBAnimation extends StatefulWidget {
  final Offset startPosition;
  final Offset endPosition;
  final double opacity;
  final double dxCurveAnimation;
  final double dyCurveAnimation;
  final Duration duration;
  final Widget child;

  const AtoBAnimation(
      {super.key,
      required this.startPosition,
      required this.endPosition,
      this.opacity = 1.0,
      this.dxCurveAnimation = 100,
      this.dyCurveAnimation = 250,
      this.duration = const Duration(milliseconds: 1000),
      required this.child});

  @override
  _AtoBAnimationPageState createState() => _AtoBAnimationPageState();
}

class _AtoBAnimationPageState extends State<AtoBAnimation>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller; // Animation controller
  late Animation<double> _animation; // animation
  double? left; // The left of the small dot (dynamic calculation)
  double? top; // Small far point right (dynamic calculation)

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(duration: widget.duration, vsync: this);
    _animation = Tween(begin: 0.0, end: 1.0).animate(_controller);

    var x0 = widget.startPosition.dx;
    var y0 = widget.startPosition.dy;

    var x1 = widget.startPosition.dx - widget.dxCurveAnimation;
    var y1 = widget.startPosition.dy - widget.dyCurveAnimation;

    var x2 = widget.endPosition.dx;
    var y2 = widget.endPosition.dy;

    _animation.addListener(() {
// Value for second-order Bezier curve
      var t = _animation.value;
      if (mounted) {
        setState(() {
          left = pow(1 - t, 2) * x0 + 2 * t * (1 - t) * x1 + pow(t, 2) * x2;
          top = pow(1 - t, 2) * y0 + 2 * t * (1 - t) * y1 + pow(t, 2) * y2;
        });
      }
    });

    // Initialize the position of the widget
    left = widget.startPosition.dx;
    top = widget.startPosition.dy;

    // The animation starts when the widget is displayed
    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Use Stack -> Positioned to control the position of the Widget
    return Stack(
      children: <Widget>[
        Positioned(
            left: left,
            top: top,
            child: Opacity(
              opacity: widget.opacity,
              child: Material(color: Colors.transparent, child: widget.child),
            ))
      ],
    );
  }
}

/// coin animation
class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  final GlobalKey _targetkey = GlobalKey();
  Offset _endOffset = Offset.zero;

  final GlobalKey _startkey = GlobalKey();
  Offset _startOffset = Offset.zero;

  _incrementCart() {
    OverlayEntry? _overlayEntry = OverlayEntry(builder: (_) {
      return AtoBAnimation(
        startPosition: _startOffset,
        //animation Start Point
        endPosition: _endOffset,
        //Animation end Point
        dxCurveAnimation: 200,
        dyCurveAnimation: 50,
        duration: const Duration(milliseconds: 1000),
        opacity: 1,
        child: Image.asset(
          "assets/images/coin.png",
          height: 60,
          width: 60,
        ),
      );
    });
    // Show Overlay
    Overlay.of(context).insert(_overlayEntry);
    // wait for the animation to end
    Future.delayed(Duration(milliseconds: 1000), () {
      _overlayEntry?.remove();
      _overlayEntry = null;
    });

    setState(() {
      _counter++;
    });
    if (_counter < 10) {
      Future.delayed(const Duration(milliseconds: 100), () {
        _incrementCart();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((c) {
      // Get the location of the "shopping cart"
      _endOffset = (_targetkey.currentContext?.findRenderObject() as RenderBox)
          .localToGlobal(Offset.zero);
      // Get the position of the current widget when clicked, and pass in overlayEntry
      _startOffset = (_startkey.currentContext?.findRenderObject() as RenderBox)
          .localToGlobal(Offset.zero);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(""),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SizedBox(
            height: 100,
          ),
          // IconButton(
          //     key: _targetkey,
          //     onPressed: (){}, icon: Icon(Icons.access_alarm)),
          Container(
            key: _targetkey,
            child: _counter != 0
                ? Stack(
                    children: [
                      Icon(Icons.shopping_bag, size: 100),
                      ClipOval(
                        child: Container(
                          color: Colors.yellow,
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              "$_counter",
                              style: TextStyle(color: Colors.red, fontSize: 35),
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                : Icon(Icons.shopping_bag, size: 100),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        key: _startkey,
        onPressed: _incrementCart,
        child: Icon(Icons.add),
      ),
    );
  }
}
