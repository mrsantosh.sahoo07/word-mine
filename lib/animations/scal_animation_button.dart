import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../games/word_game/setting/setting_controller.dart';

class ScaleAnimationButton extends StatefulWidget {
  final Widget? child;
  final Function()? onTap;

  const ScaleAnimationButton({super.key, this.child, this.onTap});

  @override
  State<ScaleAnimationButton> createState() => _ScaleAnimationButtonState();
}

class _ScaleAnimationButtonState extends State<ScaleAnimationButton>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;
  late Animation<double> _animation2;
  bool clickAnimation = false;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 800),
    );
    _animation = Tween<double>(begin: 1.0, end: 0.95).animate(_controller)
      ..addListener(() {
        setState(() {});
      });
    _animation2 = Tween<double>(begin: 0.7, end: 1)
        .chain(CurveTween(curve: Curves.elasticIn))
        .animate(_controller)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          clickAnimation = false;
        }
      });

    _animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        _controller.forward();
      }
    });

    _controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanDown: (t) {
        clickAnimation = true;
        setState(() {});
      },
      onTap: () {
        if (widget.onTap != null) {
          widget.onTap!();
        }
      },
      child: Transform.scale(
        scale: clickAnimation ? _animation2.value : _animation.value,
        child: widget.child ?? const SizedBox(),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    _animation.removeListener(() {});
    _animation.removeStatusListener((status) {});
    _animation2.removeListener(() {});
    _animation2.removeStatusListener((status) {});
    super.dispose();
  }
}




