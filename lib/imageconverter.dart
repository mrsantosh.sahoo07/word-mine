import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class PdfViewr extends StatefulWidget {
  const PdfViewr({super.key});

  @override
  State<PdfViewr> createState() => _PdfViewrState();
}

class _PdfViewrState extends State<PdfViewr> {
  late bool _canShowPdf;
  final PdfViewerController _pdfViewerController = PdfViewerController();

  @override
  void initState() {
    Future<dynamic>.delayed(const Duration(milliseconds: 600), () {
      if (super.mounted) {
        final FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.requestFocus(FocusNode());
        }
      }
    });
    super.initState();
    _canShowPdf = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("data"),),

      // body:
      // SfPdfViewer.network(
      //   // "assets/images/flutter_succinctly.pdf",
      //   'https://cdn.syncfusion.com/content/PDFViewer/flutter-succinctly.pdf',
      //   canShowPaginationDialog: true,
      //   enableTextSelection: true,
      // ),
    );
  }
}

//
//
// import 'package:flutter/material.dart';
// /*
//  * Copyright (C) 2017, David PHAM-VAN <dev.nfet.net@gmail.com>
//  *
//  * Licensed under the Apache License, Version 2.0 (the "License");
//  * you may not use this file except in compliance with the License.
//  * You may obtain a copy of the License at
//  *
//  *     http://www.apache.org/licenses/LICENSE-2.0
//  *
//  * Unless required by applicable law or agreed to in writing, software
//  * distributed under the License is distributed on an "AS IS" BASIS,
//  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  * See the License for the specific language governing permissions and
//  * limitations under the License.
//  */
//
// import 'dart:async';
// import 'dart:io';
//
// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// // import 'package:open_file/open_file.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:pdf/pdf.dart';
// import 'package:pdf/widgets.dart' as pw;
// import 'package:printing/printing.dart';
// import 'package:url_launcher/url_launcher.dart' as ul;
//
// import 'dart:async';
// import 'dart:typed_data';
//
// import 'package:pdf/pdf.dart';
//
// /*
//  * Copyright (C) 2017, David PHAM-VAN <dev.nfet.net@gmail.com>
//  *
//  * Licensed under the Apache License, Version 2.0 (the "License");
//  * you may not use this file except in compliance with the License.
//  * You may obtain a copy of the License at
//  *
//  *     http://www.apache.org/licenses/LICENSE-2.0
//  *
//  * Unless required by applicable law or agreed to in writing, software
//  * distributed under the License is distributed on an "AS IS" BASIS,
//  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  * See the License for the specific language governing permissions and
//  * limitations under the License.
//  */
//
// import 'dart:async';
// import 'dart:math';
// import 'dart:typed_data';
//
// import 'package:flutter/services.dart' show rootBundle;
// import 'package:pdf/pdf.dart';
// import 'package:pdf/widgets.dart' as pw;
// import 'package:printing/printing.dart';
//
// /*
//  * Copyright (C) 2017, David PHAM-VAN <dev.nfet.net@gmail.com>
//  *
//  * Licensed under the Apache License, Version 2.0 (the "License");
//  * you may not use this file except in compliance with the License.
//  * You may obtain a copy of the License at
//  *
//  *     http://www.apache.org/licenses/LICENSE-2.0
//  *
//  * Unless required by applicable law or agreed to in writing, software
//  * distributed under the License is distributed on an "AS IS" BASIS,
//  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  * See the License for the specific language governing permissions and
//  * limitations under the License.
//  */
//
// import 'dart:math';
// import 'dart:typed_data';
//
// import 'package:pdf/pdf.dart';
// import 'package:pdf/widgets.dart' as pw;
// import 'package:printing/printing.dart';
//
// /*
//  * Copyright (C) 2017, David PHAM-VAN <dev.nfet.net@gmail.com>
//  *
//  * Licensed under the Apache License, Version 2.0 (the "License");
//  * you may not use this file except in compliance with the License.
//  * You may obtain a copy of the License at
//  *
//  *     http://www.apache.org/licenses/LICENSE-2.0
//  *
//  * Unless required by applicable law or agreed to in writing, software
//  * distributed under the License is distributed on an "AS IS" BASIS,
//  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  * See the License for the specific language governing permissions and
//  * limitations under the License.
//  */
//
// import 'dart:typed_data';
//
// import 'package:flutter/services.dart' show rootBundle;
// import 'package:intl/intl.dart';
// import 'package:pdf/pdf.dart';
// import 'package:pdf/widgets.dart' as pw;
// import 'package:printing/printing.dart';
//
// /*
//  * Copyright (C) 2017, David PHAM-VAN <dev.nfet.net@gmail.com>
//  *
//  * Licensed under the Apache License, Version 2.0 (the "License");
//  * you may not use this file except in compliance with the License.
//  * You may obtain a copy of the License at
//  *
//  *     http://www.apache.org/licenses/LICENSE-2.0
//  *
//  * Unless required by applicable law or agreed to in writing, software
//  * distributed under the License is distributed on an "AS IS" BASIS,
//  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  * See the License for the specific language governing permissions and
//  * limitations under the License.
//  */
//
// import 'dart:typed_data';
//
// import 'package:flutter/services.dart' show rootBundle;
// import 'package:pdf/pdf.dart';
// import 'package:pdf/widgets.dart' as pw;
// import 'package:printing/printing.dart';
//
// /*
//  * Copyright (C) 2017, David PHAM-VAN <dev.nfet.net@gmail.com>
//  *
//  * Licensed under the Apache License, Version 2.0 (the "License");
//  * you may not use this file except in compliance with the License.
//  * You may obtain a copy of the License at
//  *
//  *     http://www.apache.org/licenses/LICENSE-2.0
//  *
//  * Unless required by applicable law or agreed to in writing, software
//  * distributed under the License is distributed on an "AS IS" BASIS,
//  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  * See the License for the specific language governing permissions and
//  * limitations under the License.
//  */
//
// import 'dart:typed_data';
//
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart' show rootBundle;
// import 'package:pdf/pdf.dart';
// import 'package:pdf/widgets.dart' as pw;
// import 'package:printing/printing.dart';
// import 'package:vector_math/vector_math_64.dart';
//
//
// Future<Uint8List> generateCertificate(
//     PdfPageFormat pageFormat, CustomData data) async {
//   final lorem = pw.LoremText();
//   final pdf = pw.Document();
//
//   final libreBaskerville = await PdfGoogleFonts.libreBaskervilleRegular();
//   final libreBaskervilleItalic = await PdfGoogleFonts.libreBaskervilleItalic();
//   final libreBaskervilleBold = await PdfGoogleFonts.libreBaskervilleBold();
//   final robotoLight = await PdfGoogleFonts.robotoLight();
//   final medail = await rootBundle.loadString('assets/medail.svg');
//   final swirls = await rootBundle.loadString('assets/swirls.svg');
//   final swirls1 = await rootBundle.loadString('assets/swirls1.svg');
//   final swirls2 = await rootBundle.loadString('assets/swirls2.svg');
//   final swirls3 = await rootBundle.loadString('assets/swirls3.svg');
//   final garland = await rootBundle.loadString('assets/garland.svg');
//
//   pdf.addPage(
//     pw.Page(
//       build: (context) => pw.Column(
//         children: [
//           pw.Spacer(flex: 2),
//           pw.RichText(
//             text: pw.TextSpan(
//                 style: pw.TextStyle(
//                   fontWeight: pw.FontWeight.bold,
//                   fontSize: 25,
//                 ),
//                 children: [
//                   const pw.TextSpan(text: 'CERTIFICATE '),
//                   pw.TextSpan(
//                     text: 'of',
//                     style: pw.TextStyle(
//                       fontStyle: pw.FontStyle.italic,
//                       fontWeight: pw.FontWeight.normal,
//                     ),
//                   ),
//                   const pw.TextSpan(text: ' ACHIEVEMENT'),
//                 ]),
//           ),
//           pw.Spacer(),
//           pw.Text(
//             'THIS ACKNOWLEDGES THAT',
//             style: pw.TextStyle(
//               font: robotoLight,
//               fontSize: 10,
//               letterSpacing: 2,
//               wordSpacing: 2,
//             ),
//           ),
//           pw.SizedBox(
//             width: 300,
//             child: pw.Divider(color: PdfColors.grey, thickness: 1.5),
//           ),
//           pw.Text(
//             data.name,
//             textAlign: pw.TextAlign.center,
//             style: pw.TextStyle(
//               fontWeight: pw.FontWeight.bold,
//               fontSize: 20,
//             ),
//           ),
//           pw.SizedBox(
//             width: 300,
//             child: pw.Divider(color: PdfColors.grey, thickness: 1.5),
//           ),
//           pw.Text(
//             'HAS SUCCESSFULLY COMPLETED THE',
//             style: pw.TextStyle(
//               font: robotoLight,
//               fontSize: 10,
//               letterSpacing: 2,
//               wordSpacing: 2,
//             ),
//           ),
//           pw.SizedBox(height: 10),
//           pw.Row(
//             mainAxisAlignment: pw.MainAxisAlignment.center,
//             children: [
//               pw.SvgImage(
//                 svg: swirls,
//                 height: 10,
//               ),
//               pw.Padding(
//                 padding: const pw.EdgeInsets.symmetric(horizontal: 10),
//                 child: pw.Text(
//                   'Flutter PDF Demo',
//                   style: const pw.TextStyle(
//                     fontSize: 10,
//                   ),
//                 ),
//               ),
//               pw.Transform(
//                 transform: Matrix4.diagonal3Values(-1, 1, 1),
//                 adjustLayout: true,
//                 child: pw.SvgImage(
//                   svg: swirls,
//                   height: 10,
//                 ),
//               ),
//             ],
//           ),
//           pw.Spacer(),
//           pw.SvgImage(
//             svg: swirls2,
//             width: 150,
//           ),
//           pw.Spacer(),
//           pw.Row(
//             crossAxisAlignment: pw.CrossAxisAlignment.start,
//             children: [
//               pw.Flexible(
//                 child: pw.Text(
//                   lorem.paragraph(40),
//                   style: const pw.TextStyle(fontSize: 6),
//                   textAlign: pw.TextAlign.justify,
//                 ),
//               ),
//               pw.SizedBox(width: 100),
//               pw.SvgImage(
//                 svg: medail,
//                 width: 100,
//               ),
//             ],
//           ),
//         ],
//       ),
//       pageTheme: pw.PageTheme(
//         pageFormat: pageFormat,
//         theme: pw.ThemeData.withFont(
//           base: libreBaskerville,
//           italic: libreBaskervilleItalic,
//           bold: libreBaskervilleBold,
//         ),
//         buildBackground: (context) => pw.FullPage(
//           ignoreMargins: true,
//           child: pw.Container(
//             margin: const pw.EdgeInsets.all(10),
//             decoration: pw.BoxDecoration(
//               border: pw.Border.all(
//                   color: const PdfColor.fromInt(0xffe435), width: 1),
//             ),
//             child: pw.Container(
//               margin: const pw.EdgeInsets.all(5),
//               decoration: pw.BoxDecoration(
//                 border: pw.Border.all(
//                     color: const PdfColor.fromInt(0xffe435), width: 5),
//               ),
//               width: double.infinity,
//               height: double.infinity,
//               child: pw.Stack(
//                 alignment: pw.Alignment.center,
//                 children: [
//                   pw.Positioned(
//                     top: 5,
//                     child: pw.SvgImage(
//                       svg: swirls1,
//                       height: 60,
//                     ),
//                   ),
//                   pw.Positioned(
//                     bottom: 5,
//                     child: pw.Transform(
//                       transform: Matrix4.diagonal3Values(1, -1, 1),
//                       adjustLayout: true,
//                       child: pw.SvgImage(
//                         svg: swirls1,
//                         height: 60,
//                       ),
//                     ),
//                   ),
//                   pw.Positioned(
//                     top: 5,
//                     left: 5,
//                     child: pw.SvgImage(
//                       svg: swirls3,
//                       height: 160,
//                     ),
//                   ),
//                   pw.Positioned(
//                     top: 5,
//                     right: 5,
//                     child: pw.Transform(
//                       transform: Matrix4.diagonal3Values(-1, 1, 1),
//                       adjustLayout: true,
//                       child: pw.SvgImage(
//                         svg: swirls3,
//                         height: 160,
//                       ),
//                     ),
//                   ),
//                   pw.Positioned(
//                     bottom: 5,
//                     left: 5,
//                     child: pw.Transform(
//                       transform: Matrix4.diagonal3Values(1, -1, 1),
//                       adjustLayout: true,
//                       child: pw.SvgImage(
//                         svg: swirls3,
//                         height: 160,
//                       ),
//                     ),
//                   ),
//                   pw.Positioned(
//                     bottom: 5,
//                     right: 5,
//                     child: pw.Transform(
//                       transform: Matrix4.diagonal3Values(-1, -1, 1),
//                       adjustLayout: true,
//                       child: pw.SvgImage(
//                         svg: swirls3,
//                         height: 160,
//                       ),
//                     ),
//                   ),
//                   pw.Padding(
//                     padding: const pw.EdgeInsets.only(
//                       top: 120,
//                       left: 80,
//                       right: 80,
//                       bottom: 80,
//                     ),
//                     child: pw.SvgImage(
//                       svg: garland,
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//       ),
//     ),
//   );
//
//   return pdf.save();
// }
// Future<Uint8List> generateDocument(
//     PdfPageFormat format, CustomData data) async {
//   final doc = pw.Document(pageMode: PdfPageMode.outlines);
//
//   final font1 = data.testing
//       ? pw.Font.helvetica()
//       : await PdfGoogleFonts.openSansRegular();
//   final font2 = data.testing
//       ? pw.Font.helveticaBold()
//       : await PdfGoogleFonts.openSansBold();
//   final shape = await rootBundle.loadString('assets/document.svg');
//   final swirls = await rootBundle.loadString('assets/swirls2.svg');
//
//   doc.addPage(
//     pw.Page(
//       pageTheme: pw.PageTheme(
//         pageFormat: format.copyWith(
//           marginBottom: 0,
//           marginLeft: 0,
//           marginRight: 0,
//           marginTop: 0,
//         ),
//         orientation: pw.PageOrientation.portrait,
//         buildBackground: (context) =>
//             pw.SvgImage(svg: shape, fit: pw.BoxFit.fill),
//         theme: pw.ThemeData.withFont(
//           base: font1,
//           bold: font2,
//         ),
//       ),
//       build: (context) {
//         return pw.Padding(
//           padding: const pw.EdgeInsets.only(
//             left: 60,
//             right: 60,
//             bottom: 30,
//           ),
//           child: pw.Column(
//             children: [
//               pw.Spacer(),
//               pw.RichText(
//                   text: pw.TextSpan(children: [
//                     pw.TextSpan(
//                       text: '${DateTime.now().year}\n',
//                       style: pw.TextStyle(
//                         fontWeight: pw.FontWeight.bold,
//                         color: PdfColors.grey600,
//                         fontSize: 40,
//                       ),
//                     ),
//                     pw.TextSpan(
//                       text: 'Portable Document Format',
//                       style: pw.TextStyle(
//                         fontWeight: pw.FontWeight.bold,
//                         fontSize: 40,
//                       ),
//                     ),
//                   ])),
//               pw.Spacer(),
//               pw.Container(
//                 alignment: pw.Alignment.topRight,
//                 height: 150,
//                 child: pw.PdfLogo(),
//               ),
//               pw.Spacer(flex: 2),
//               pw.Align(
//                 alignment: pw.Alignment.topLeft,
//                 child: pw.UrlLink(
//                   destination: 'https://wikipedia.org/wiki/PDF',
//                   child: pw.Text(
//                     'https://wikipedia.org/wiki/PDF',
//                     style: const pw.TextStyle(
//                       color: PdfColors.pink100,
//                     ),
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         );
//       },
//     ),
//   );
//
//   doc.addPage(
//     pw.Page(
//       theme: pw.ThemeData.withFont(
//         base: font1,
//         bold: font2,
//       ),
//       pageFormat: format.copyWith(marginBottom: 1.5 * PdfPageFormat.cm),
//       orientation: pw.PageOrientation.portrait,
//       build: (context) {
//         return pw.Column(
//           children: [
//             pw.Center(
//               child: pw.Text('Table of content',
//                   style: pw.Theme.of(context).header0),
//             ),
//             pw.SizedBox(height: 20),
//             pw.TableOfContent(),
//             pw.Spacer(),
//             pw.Center(
//                 child: pw.SvgImage(
//                     svg: swirls, width: 100, colorFilter: PdfColors.grey)),
//             pw.Spacer(),
//           ],
//         );
//       },
//     ),
//   );
//
//   doc.addPage(pw.MultiPage(
//       theme: pw.ThemeData.withFont(
//         base: font1,
//         bold: font2,
//       ),
//       pageFormat: format.copyWith(marginBottom: 1.5 * PdfPageFormat.cm),
//       orientation: pw.PageOrientation.portrait,
//       crossAxisAlignment: pw.CrossAxisAlignment.start,
//       header: (pw.Context context) {
//         if (context.pageNumber == 1) {
//           return pw.SizedBox();
//         }
//         return pw.Container(
//             alignment: pw.Alignment.centerRight,
//             margin: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
//             padding: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
//             decoration: const pw.BoxDecoration(
//                 border: pw.Border(
//                     bottom: pw.BorderSide(width: 0.5, color: PdfColors.grey))),
//             child: pw.Text('Portable Document Format',
//                 style: pw.Theme.of(context)
//                     .defaultTextStyle
//                     .copyWith(color: PdfColors.grey)));
//       },
//       footer: (pw.Context context) {
//         return pw.Container(
//             alignment: pw.Alignment.centerRight,
//             margin: const pw.EdgeInsets.only(top: 1.0 * PdfPageFormat.cm),
//             child: pw.Text(
//                 'Page ${context.pageNumber} of ${context.pagesCount}',
//                 style: pw.Theme.of(context)
//                     .defaultTextStyle
//                     .copyWith(color: PdfColors.grey)));
//       },
//       build: (pw.Context context) => <pw.Widget>[
//         pw.Header(
//             level: 0,
//             title: 'Portable Document Format',
//             child: pw.Row(
//                 mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
//                 children: <pw.Widget>[
//                   pw.Text('Portable Document Format', textScaleFactor: 2),
//                   pw.PdfLogo()
//                 ])),
//         pw.Paragraph(
//             text:
//             'The Portable Document Format (PDF) is a file format developed by Adobe in the 1990s to present documents, including text formatting and images, in a manner independent of application software, hardware, and operating systems. Based on the PostScript language, each PDF file encapsulates a complete description of a fixed-layout flat document, including the text, fonts, vector graphics, raster images and other information needed to display it. PDF was standardized as an open format, ISO 32000, in 2008, and no longer requires any royalties for its implementation.'),
//         pw.Paragraph(
//             text:
//             'Today, PDF files may contain a variety of content besides flat text and graphics including logical structuring elements, interactive elements such as annotations and form-fields, layers, rich media (including video content) and three dimensional objects using U3D or PRC, and various other data formats. The PDF specification also provides for encryption and digital signatures, file attachments and metadata to enable workflows requiring these features.'),
//         pw.Header(level: 1, text: 'History and standardization'),
//         pw.Paragraph(
//             text:
//             "Adobe Systems made the PDF specification available free of charge in 1993. In the early years PDF was popular mainly in desktop publishing workflows, and competed with a variety of formats such as DjVu, Envoy, Common Ground Digital Paper, Farallon Replica and even Adobe's own PostScript format."),
//         pw.Paragraph(
//             text:
//             'PDF was a proprietary format controlled by Adobe until it was released as an open standard on July 1, 2008, and published by the International Organization for Standardization as ISO 32000-1:2008, at which time control of the specification passed to an ISO Committee of volunteer industry experts. In 2008, Adobe published a Public Patent License to ISO 32000-1 granting royalty-free rights for all patents owned by Adobe that are necessary to make, use, sell, and distribute PDF compliant implementations.'),
//         pw.Paragraph(
//             text:
//             "PDF 1.7, the sixth edition of the PDF specification that became ISO 32000-1, includes some proprietary technologies defined only by Adobe, such as Adobe XML Forms Architecture (XFA) and JavaScript extension for Acrobat, which are referenced by ISO 32000-1 as normative and indispensable for the full implementation of the ISO 32000-1 specification. These proprietary technologies are not standardized and their specification is published only on Adobe's website. Many of them are also not supported by popular third-party implementations of PDF."),
//         pw.Paragraph(
//             text:
//             'On July 28, 2017, ISO 32000-2:2017 (PDF 2.0) was published. ISO 32000-2 does not include any proprietary technologies as normative references.'),
//         pw.Header(level: 1, text: 'Technical foundations'),
//         pw.Paragraph(text: 'The PDF combines three technologies:'),
//         pw.Bullet(
//             text:
//             'A subset of the PostScript page description programming language, for generating the layout and graphics.'),
//         pw.Bullet(
//             text:
//             'A font-embedding/replacement system to allow fonts to travel with the documents.'),
//         pw.Bullet(
//             text:
//             'A structured storage system to bundle these elements and any associated content into a single file, with data compression where appropriate.'),
//         pw.Header(level: 2, text: 'PostScript'),
//         pw.Paragraph(
//             text:
//             'PostScript is a page description language run in an interpreter to generate an image, a process requiring many resources. It can handle graphics and standard features of programming languages such as if and loop commands. PDF is largely based on PostScript but simplified to remove flow control features like these, while graphics commands such as lineto remain.'),
//         pw.Paragraph(
//             text:
//             'Often, the PostScript-like PDF code is generated from a source PostScript file. The graphics commands that are output by the PostScript code are collected and tokenized. Any files, graphics, or fonts to which the document refers also are collected. Then, everything is compressed to a single file. Therefore, the entire PostScript world (fonts, layout, measurements) remains intact.'),
//         pw.Column(
//             crossAxisAlignment: pw.CrossAxisAlignment.start,
//             children: <pw.Widget>[
//               pw.Paragraph(
//                   text:
//                   'As a document format, PDF has several advantages over PostScript:'),
//               pw.Bullet(
//                   text:
//                   'PDF contains tokenized and interpreted results of the PostScript source code, for direct correspondence between changes to items in the PDF page description and changes to the resulting page appearance.'),
//               pw.Bullet(
//                   text:
//                   'PDF (from version 1.4) supports graphic transparency; PostScript does not.'),
//               pw.Bullet(
//                   text:
//                   'PostScript is an interpreted programming language with an implicit global state, so instructions accompanying the description of one page can affect the appearance of any following page. Therefore, all preceding pages in a PostScript document must be processed to determine the correct appearance of a given page, whereas each page in a PDF document is unaffected by the others. As a result, PDF viewers allow the user to quickly jump to the final pages of a long document, whereas a PostScript viewer needs to process all pages sequentially before being able to display the destination page (unless the optional PostScript Document Structuring Conventions have been carefully complied with).'),
//             ]),
//         pw.Header(level: 1, text: 'Content'),
//         pw.Paragraph(
//             text:
//             'A PDF file is often a combination of vector graphics, text, and bitmap graphics. The basic types of content in a PDF are:'),
//         pw.Bullet(
//             text:
//             'Text stored as content streams (i.e., not encoded in plain text)'),
//         pw.Bullet(
//             text:
//             'Vector graphics for illustrations and designs that consist of shapes and lines'),
//         pw.Bullet(
//             text:
//             'Raster graphics for photographs and other types of image'),
//         pw.Bullet(text: 'Multimedia objects in the document'),
//         pw.Paragraph(
//             text:
//             'In later PDF revisions, a PDF document can also support links (inside document or web page), forms, JavaScript (initially available as plugin for Acrobat 3.0), or any other types of embedded contents that can be handled using plug-ins.'),
//         pw.Paragraph(
//             text:
//             'PDF 1.6 supports interactive 3D documents embedded in the PDF - 3D drawings can be embedded using U3D or PRC and various other data formats.'),
//         pw.Paragraph(
//             text:
//             'Two PDF files that look similar on a computer screen may be of very different sizes. For example, a high resolution raster image takes more space than a low resolution one. Typically higher resolution is needed for printing documents than for displaying them on screen. Other things that may increase the size of a file is embedding full fonts, especially for Asiatic scripts, and storing text as graphics. '),
//         pw.Header(
//             level: 1, text: 'File formats and Adobe Acrobat versions'),
//         pw.Paragraph(
//             text:
//             'The PDF file format has changed several times, and continues to evolve, along with the release of new versions of Adobe Acrobat. There have been nine versions of PDF and the corresponding version of the software:'),
//         pw.TableHelper.fromTextArray(
//           context: context,
//           data: const <List<String>>[
//             <String>['Date', 'PDF Version', 'Acrobat Version'],
//             <String>['1993', 'PDF 1.0', 'Acrobat 1'],
//             <String>['1994', 'PDF 1.1', 'Acrobat 2'],
//             <String>['1996', 'PDF 1.2', 'Acrobat 3'],
//             <String>['1999', 'PDF 1.3', 'Acrobat 4'],
//             <String>['2001', 'PDF 1.4', 'Acrobat 5'],
//             <String>['2003', 'PDF 1.5', 'Acrobat 6'],
//             <String>['2005', 'PDF 1.6', 'Acrobat 7'],
//             <String>['2006', 'PDF 1.7', 'Acrobat 8'],
//             <String>['2008', 'PDF 1.7', 'Acrobat 9'],
//             <String>['2009', 'PDF 1.7', 'Acrobat 9.1'],
//             <String>['2010', 'PDF 1.7', 'Acrobat X'],
//             <String>['2012', 'PDF 1.7', 'Acrobat XI'],
//             <String>['2017', 'PDF 2.0', 'Acrobat DC'],
//           ],
//         ),
//         pw.Padding(padding: const pw.EdgeInsets.all(10)),
//         pw.Paragraph(
//             text:
//             'Text is available under the Creative Commons Attribution Share Alike License.')
//       ]));
//
//   return await doc.save();
// }
// Future<Uint8List> generateInvoice(
//     PdfPageFormat pageFormat, CustomData data) async {
//   final lorem = pw.LoremText();
//
//   final products = <Product>[
//     Product('19874', lorem.sentence(4), 3.99, 2),
//     Product('98452', lorem.sentence(6), 15, 2),
//     Product('28375', lorem.sentence(4), 6.95, 3),
//     Product('95673', lorem.sentence(3), 49.99, 4),
//     Product('23763', lorem.sentence(2), 560.03, 1),
//     Product('55209', lorem.sentence(5), 26, 1),
//     Product('09853', lorem.sentence(5), 26, 1),
//     Product('23463', lorem.sentence(5), 34, 1),
//     Product('56783', lorem.sentence(5), 7, 4),
//     Product('78256', lorem.sentence(5), 23, 1),
//     Product('23745', lorem.sentence(5), 94, 1),
//     Product('07834', lorem.sentence(5), 12, 1),
//     Product('23547', lorem.sentence(5), 34, 1),
//     Product('98387', lorem.sentence(5), 7.99, 2),
//   ];
//
//   final invoice = Invoice(
//     invoiceNumber: '982347',
//     products: products,
//     customerName: 'Abraham Swearegin',
//     customerAddress: '54 rue de Rivoli\n75001 Paris, France',
//     paymentInfo:
//     '4509 Wiseman Street\nKnoxville, Tennessee(TN), 37929\n865-372-0425',
//     tax: .15,
//     baseColor: PdfColors.teal,
//     accentColor: PdfColors.blueGrey900,
//   );
//
//   return await invoice.buildPdf(pageFormat);
// }
//
// class Invoice {
//   Invoice({
//     required this.products,
//     required this.customerName,
//     required this.customerAddress,
//     required this.invoiceNumber,
//     required this.tax,
//     required this.paymentInfo,
//     required this.baseColor,
//     required this.accentColor,
//   });
//
//   final List<Product> products;
//   final String customerName;
//   final String customerAddress;
//   final String invoiceNumber;
//   final double tax;
//   final String paymentInfo;
//   final PdfColor baseColor;
//   final PdfColor accentColor;
//
//   static const _darkColor = PdfColors.blueGrey800;
//   static const _lightColor = PdfColors.white;
//
//   PdfColor get _baseTextColor => baseColor.isLight ? _lightColor : _darkColor;
//
//   PdfColor get _accentTextColor => baseColor.isLight ? _lightColor : _darkColor;
//
//   double get _total =>
//       products.map<double>((p) => p.total).reduce((a, b) => a + b);
//
//   double get _grandTotal => _total * (1 + tax);
//
//   String? _logo;
//
//   String? _bgShape;
//
//   Future<Uint8List> buildPdf(PdfPageFormat pageFormat) async {
//     // Create a PDF document.
//     final doc = pw.Document();
//
//     _logo = await rootBundle.loadString('assets/logo.svg');
//     _bgShape = await rootBundle.loadString('assets/invoice.svg');
//
//     // Add page to the PDF
//     doc.addPage(
//       pw.MultiPage(
//         pageTheme: _buildTheme(
//           pageFormat,
//           await PdfGoogleFonts.robotoRegular(),
//           await PdfGoogleFonts.robotoBold(),
//           await PdfGoogleFonts.robotoItalic(),
//         ),
//         header: _buildHeader,
//         footer: _buildFooter,
//         build: (context) => [
//           _contentHeader(context),
//           _contentTable(context),
//           pw.SizedBox(height: 20),
//           _contentFooter(context),
//           pw.SizedBox(height: 20),
//           _termsAndConditions(context),
//         ],
//       ),
//     );
//
//     // Return the PDF file content
//     return doc.save();
//   }
//
//   pw.Widget _buildHeader(pw.Context context) {
//     return pw.Column(
//       children: [
//         pw.Row(
//           crossAxisAlignment: pw.CrossAxisAlignment.start,
//           children: [
//             pw.Expanded(
//               child: pw.Column(
//                 children: [
//                   pw.Container(
//                     height: 50,
//                     padding: const pw.EdgeInsets.only(left: 20),
//                     alignment: pw.Alignment.centerLeft,
//                     child: pw.Text(
//                       'INVOICE',
//                       style: pw.TextStyle(
//                         color: baseColor,
//                         fontWeight: pw.FontWeight.bold,
//                         fontSize: 40,
//                       ),
//                     ),
//                   ),
//                   pw.Container(
//                     decoration: pw.BoxDecoration(
//                       borderRadius:
//                       const pw.BorderRadius.all(pw.Radius.circular(2)),
//                       color: accentColor,
//                     ),
//                     padding: const pw.EdgeInsets.only(
//                         left: 40, top: 10, bottom: 10, right: 20),
//                     alignment: pw.Alignment.centerLeft,
//                     height: 50,
//                     child: pw.DefaultTextStyle(
//                       style: pw.TextStyle(
//                         color: _accentTextColor,
//                         fontSize: 12,
//                       ),
//                       child: pw.GridView(
//                         crossAxisCount: 2,
//                         children: [
//                           pw.Text('Invoice #'),
//                           pw.Text(invoiceNumber),
//                           pw.Text('Date:'),
//                           pw.Text(_formatDate(DateTime.now())),
//                         ],
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//             pw.Expanded(
//               child: pw.Column(
//                 mainAxisSize: pw.MainAxisSize.min,
//                 children: [
//                   pw.Container(
//                     alignment: pw.Alignment.topRight,
//                     padding: const pw.EdgeInsets.only(bottom: 8, left: 30),
//                     height: 72,
//                     child:
//                     _logo != null ? pw.SvgImage(svg: _logo!) : pw.PdfLogo(),
//                   ),
//                   // pw.Container(
//                   //   color: baseColor,
//                   //   padding: pw.EdgeInsets.only(top: 3),
//                   // ),
//                 ],
//               ),
//             ),
//           ],
//         ),
//         if (context.pageNumber > 1) pw.SizedBox(height: 20)
//       ],
//     );
//   }
//
//   pw.Widget _buildFooter(pw.Context context) {
//     return pw.Row(
//       mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
//       crossAxisAlignment: pw.CrossAxisAlignment.end,
//       children: [
//         pw.Container(
//           height: 20,
//           width: 100,
//           child: pw.BarcodeWidget(
//             barcode: pw.Barcode.pdf417(),
//             data: 'Invoice# $invoiceNumber',
//             drawText: false,
//           ),
//         ),
//         pw.Text(
//           'Page ${context.pageNumber}/${context.pagesCount}',
//           style: const pw.TextStyle(
//             fontSize: 12,
//             color: PdfColors.white,
//           ),
//         ),
//       ],
//     );
//   }
//
//   pw.PageTheme _buildTheme(
//       PdfPageFormat pageFormat, pw.Font base, pw.Font bold, pw.Font italic) {
//     return pw.PageTheme(
//       pageFormat: pageFormat,
//       theme: pw.ThemeData.withFont(
//         base: base,
//         bold: bold,
//         italic: italic,
//       ),
//       buildBackground: (context) => pw.FullPage(
//         ignoreMargins: true,
//         child: pw.SvgImage(svg: _bgShape!),
//       ),
//     );
//   }
//
//   pw.Widget _contentHeader(pw.Context context) {
//     return pw.Row(
//       crossAxisAlignment: pw.CrossAxisAlignment.start,
//       children: [
//         pw.Expanded(
//           child: pw.Container(
//             margin: const pw.EdgeInsets.symmetric(horizontal: 20),
//             height: 70,
//             child: pw.FittedBox(
//               child: pw.Text(
//                 'Total: ${_formatCurrency(_grandTotal)}',
//                 style: pw.TextStyle(
//                   color: baseColor,
//                   fontStyle: pw.FontStyle.italic,
//                 ),
//               ),
//             ),
//           ),
//         ),
//         pw.Expanded(
//           child: pw.Row(
//             children: [
//               pw.Container(
//                 margin: const pw.EdgeInsets.only(left: 10, right: 10),
//                 height: 70,
//                 child: pw.Text(
//                   'Invoice to:',
//                   style: pw.TextStyle(
//                     color: _darkColor,
//                     fontWeight: pw.FontWeight.bold,
//                     fontSize: 12,
//                   ),
//                 ),
//               ),
//               pw.Expanded(
//                 child: pw.Container(
//                   height: 70,
//                   child: pw.RichText(
//                       text: pw.TextSpan(
//                           text: '$customerName\n',
//                           style: pw.TextStyle(
//                             color: _darkColor,
//                             fontWeight: pw.FontWeight.bold,
//                             fontSize: 12,
//                           ),
//                           children: [
//                             const pw.TextSpan(
//                               text: '\n',
//                               style: pw.TextStyle(
//                                 fontSize: 5,
//                               ),
//                             ),
//                             pw.TextSpan(
//                               text: customerAddress,
//                               style: pw.TextStyle(
//                                 fontWeight: pw.FontWeight.normal,
//                                 fontSize: 10,
//                               ),
//                             ),
//                           ])),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ],
//     );
//   }
//
//   pw.Widget _contentFooter(pw.Context context) {
//     return pw.Row(
//       crossAxisAlignment: pw.CrossAxisAlignment.start,
//       children: [
//         pw.Expanded(
//           flex: 2,
//           child: pw.Column(
//             crossAxisAlignment: pw.CrossAxisAlignment.start,
//             children: [
//               pw.Text(
//                 'Thank you for your business',
//                 style: pw.TextStyle(
//                   color: _darkColor,
//                   fontWeight: pw.FontWeight.bold,
//                 ),
//               ),
//               pw.Container(
//                 margin: const pw.EdgeInsets.only(top: 20, bottom: 8),
//                 child: pw.Text(
//                   'Payment Info:',
//                   style: pw.TextStyle(
//                     color: baseColor,
//                     fontWeight: pw.FontWeight.bold,
//                   ),
//                 ),
//               ),
//               pw.Text(
//                 paymentInfo,
//                 style: const pw.TextStyle(
//                   fontSize: 8,
//                   lineSpacing: 5,
//                   color: _darkColor,
//                 ),
//               ),
//             ],
//           ),
//         ),
//         pw.Expanded(
//           flex: 1,
//           child: pw.DefaultTextStyle(
//             style: const pw.TextStyle(
//               fontSize: 10,
//               color: _darkColor,
//             ),
//             child: pw.Column(
//               crossAxisAlignment: pw.CrossAxisAlignment.start,
//               children: [
//                 pw.Row(
//                   mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
//                   children: [
//                     pw.Text('Sub Total:'),
//                     pw.Text(_formatCurrency(_total)),
//                   ],
//                 ),
//                 pw.SizedBox(height: 5),
//                 pw.Row(
//                   mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
//                   children: [
//                     pw.Text('Tax:'),
//                     pw.Text('${(tax * 100).toStringAsFixed(1)}%'),
//                   ],
//                 ),
//                 pw.Divider(color: accentColor),
//                 pw.DefaultTextStyle(
//                   style: pw.TextStyle(
//                     color: baseColor,
//                     fontSize: 14,
//                     fontWeight: pw.FontWeight.bold,
//                   ),
//                   child: pw.Row(
//                     mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
//                     children: [
//                       pw.Text('Total:'),
//                       pw.Text(_formatCurrency(_grandTotal)),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ],
//     );
//   }
//
//   pw.Widget _termsAndConditions(pw.Context context) {
//     return pw.Row(
//       crossAxisAlignment: pw.CrossAxisAlignment.end,
//       children: [
//         pw.Expanded(
//           child: pw.Column(
//             crossAxisAlignment: pw.CrossAxisAlignment.start,
//             children: [
//               pw.Container(
//                 decoration: pw.BoxDecoration(
//                   border: pw.Border(top: pw.BorderSide(color: accentColor)),
//                 ),
//                 padding: const pw.EdgeInsets.only(top: 10, bottom: 4),
//                 child: pw.Text(
//                   'Terms & Conditions',
//                   style: pw.TextStyle(
//                     fontSize: 12,
//                     color: baseColor,
//                     fontWeight: pw.FontWeight.bold,
//                   ),
//                 ),
//               ),
//               pw.Text(
//                 pw.LoremText().paragraph(40),
//                 textAlign: pw.TextAlign.justify,
//                 style: const pw.TextStyle(
//                   fontSize: 6,
//                   lineSpacing: 2,
//                   color: _darkColor,
//                 ),
//               ),
//             ],
//           ),
//         ),
//         pw.Expanded(
//           child: pw.SizedBox(),
//         ),
//       ],
//     );
//   }
//
//   pw.Widget _contentTable(pw.Context context) {
//     const tableHeaders = [
//       'SKU#',
//       'Item Description',
//       'Price',
//       'Quantity',
//       'Total'
//     ];
//
//     return pw.TableHelper.fromTextArray(
//       border: null,
//       cellAlignment: pw.Alignment.centerLeft,
//       headerDecoration: pw.BoxDecoration(
//         borderRadius: const pw.BorderRadius.all(pw.Radius.circular(2)),
//         color: baseColor,
//       ),
//       headerHeight: 25,
//       cellHeight: 40,
//       cellAlignments: {
//         0: pw.Alignment.centerLeft,
//         1: pw.Alignment.centerLeft,
//         2: pw.Alignment.centerRight,
//         3: pw.Alignment.center,
//         4: pw.Alignment.centerRight,
//       },
//       headerStyle: pw.TextStyle(
//         color: _baseTextColor,
//         fontSize: 10,
//         fontWeight: pw.FontWeight.bold,
//       ),
//       cellStyle: const pw.TextStyle(
//         color: _darkColor,
//         fontSize: 10,
//       ),
//       rowDecoration: pw.BoxDecoration(
//         border: pw.Border(
//           bottom: pw.BorderSide(
//             color: accentColor,
//             width: .5,
//           ),
//         ),
//       ),
//       headers: List<String>.generate(
//         tableHeaders.length,
//             (col) => tableHeaders[col],
//       ),
//       data: List<List<String>>.generate(
//         products.length,
//             (row) => List<String>.generate(
//           tableHeaders.length,
//               (col) => products[row].getIndex(col),
//         ),
//       ),
//     );
//   }
// }
//
// String _formatCurrency(double amount) {
//   return '\$${amount.toStringAsFixed(2)}';
// }
//
// String _formatDate(DateTime date) {
//   final format = DateFormat.yMMMd('en_US');
//   return format.format(date);
// }
//
// class Product {
//   const Product(
//       this.sku,
//       this.productName,
//       this.price,
//       this.quantity,
//       );
//
//   final String sku;
//   final String productName;
//   final double price;
//   final int quantity;
//   double get total => price * quantity;
//
//   String getIndex(int index) {
//     switch (index) {
//       case 0:
//         return sku;
//       case 1:
//         return productName;
//       case 2:
//         return _formatCurrency(price);
//       case 3:
//         return quantity.toString();
//       case 4:
//         return _formatCurrency(total);
//     }
//     return '';
//   }
// }
// Future<Uint8List> generateReport(
//     PdfPageFormat pageFormat, CustomData data) async {
//   const tableHeaders = ['Category', 'Budget', 'Expense', 'Result'];
//
//   const dataTable = [
//     ['Phone', 80, 95],
//     ['Internet', 250, 230],
//     ['Electricity', 300, 375],
//     ['Movies', 85, 80],
//     ['Food', 300, 350],
//     ['Fuel', 650, 550],
//     ['Insurance', 250, 310],
//   ];
//
//   // Some summary maths
//   final budget = dataTable
//       .map((e) => e[1] as num)
//       .reduce((value, element) => value + element);
//   final expense = dataTable
//       .map((e) => e[2] as num)
//       .reduce((value, element) => value + element);
//
//   const baseColor = PdfColors.cyan;
//
//   // Create a PDF document.
//   final document = pw.Document();
//
//   final theme = pw.ThemeData.withFont(
//     base: await PdfGoogleFonts.openSansRegular(),
//     bold: await PdfGoogleFonts.openSansBold(),
//   );
//
//   // Top bar chart
//   final chart1 = pw.Chart(
//     left: pw.Container(
//       alignment: pw.Alignment.topCenter,
//       margin: const pw.EdgeInsets.only(right: 5, top: 10),
//       child: pw.Transform.rotateBox(
//         angle: pi / 2,
//         child: pw.Text('Amount'),
//       ),
//     ),
//     overlay: pw.ChartLegend(
//       position: const pw.Alignment(-.7, 1),
//       decoration: pw.BoxDecoration(
//         color: PdfColors.white,
//         border: pw.Border.all(
//           color: PdfColors.black,
//           width: .5,
//         ),
//       ),
//     ),
//     grid: pw.CartesianGrid(
//       xAxis: pw.FixedAxis.fromStrings(
//         List<String>.generate(
//             dataTable.length, (index) => dataTable[index][0] as String),
//         marginStart: 30,
//         marginEnd: 30,
//         ticks: true,
//       ),
//       yAxis: pw.FixedAxis(
//         [0, 100, 200, 300, 400, 500, 600, 700],
//         format: (v) => '\$$v',
//         divisions: true,
//       ),
//     ),
//     datasets: [
//       pw.BarDataSet(
//         color: PdfColors.blue100,
//         legend: tableHeaders[2],
//         width: 15,
//         offset: -10,
//         borderColor: baseColor,
//         data: List<pw.PointChartValue>.generate(
//           dataTable.length,
//               (i) {
//             final v = dataTable[i][2] as num;
//             return pw.PointChartValue(i.toDouble(), v.toDouble());
//           },
//         ),
//       ),
//       pw.BarDataSet(
//         color: PdfColors.amber100,
//         legend: tableHeaders[1],
//         width: 15,
//         offset: 10,
//         borderColor: PdfColors.amber,
//         data: List<pw.PointChartValue>.generate(
//           dataTable.length,
//               (i) {
//             final v = dataTable[i][1] as num;
//             return pw.PointChartValue(i.toDouble(), v.toDouble());
//           },
//         ),
//       ),
//     ],
//   );
//
//   // Left curved line chart
//   final chart2 = pw.Chart(
//     right: pw.ChartLegend(),
//     grid: pw.CartesianGrid(
//       xAxis: pw.FixedAxis([0, 1, 2, 3, 4, 5, 6]),
//       yAxis: pw.FixedAxis(
//         [0, 200, 400, 600],
//         divisions: true,
//       ),
//     ),
//     datasets: [
//       pw.LineDataSet(
//         legend: 'Expense',
//         drawSurface: true,
//         isCurved: true,
//         drawPoints: false,
//         color: baseColor,
//         data: List<pw.PointChartValue>.generate(
//           dataTable.length,
//               (i) {
//             final v = dataTable[i][2] as num;
//             return pw.PointChartValue(i.toDouble(), v.toDouble());
//           },
//         ),
//       ),
//     ],
//   );
//
//   // Data table
//   final table = pw.TableHelper.fromTextArray(
//     border: null,
//     headers: tableHeaders,
//     data: List<List<dynamic>>.generate(
//       dataTable.length,
//           (index) => <dynamic>[
//         dataTable[index][0],
//         dataTable[index][1],
//         dataTable[index][2],
//         (dataTable[index][1] as num) - (dataTable[index][2] as num),
//       ],
//     ),
//     headerStyle: pw.TextStyle(
//       color: PdfColors.white,
//       fontWeight: pw.FontWeight.bold,
//     ),
//     headerDecoration: const pw.BoxDecoration(
//       color: baseColor,
//     ),
//     rowDecoration: const pw.BoxDecoration(
//       border: pw.Border(
//         bottom: pw.BorderSide(
//           color: baseColor,
//           width: .5,
//         ),
//       ),
//     ),
//     cellAlignment: pw.Alignment.centerRight,
//     cellAlignments: {0: pw.Alignment.centerLeft},
//   );
//
//   // Add page to the PDF
//   document.addPage(
//     pw.Page(
//       pageFormat: pageFormat,
//       theme: theme,
//       build: (context) {
//         // Page layout
//         return pw.Column(
//           children: [
//             pw.Text('Budget Report',
//                 style: const pw.TextStyle(
//                   color: baseColor,
//                   fontSize: 40,
//                 )),
//             pw.Divider(thickness: 4),
//             pw.Expanded(flex: 3, child: chart1),
//             pw.Divider(),
//             pw.Expanded(flex: 2, child: chart2),
//             pw.SizedBox(height: 10),
//             pw.Row(
//               crossAxisAlignment: pw.CrossAxisAlignment.start,
//               children: [
//                 pw.Expanded(
//                     child: pw.Column(children: [
//                       pw.Container(
//                         alignment: pw.Alignment.centerLeft,
//                         padding: const pw.EdgeInsets.only(bottom: 10),
//                         child: pw.Text(
//                           'Expense By Sub-Categories',
//                           style: const pw.TextStyle(
//                             color: baseColor,
//                             fontSize: 16,
//                           ),
//                         ),
//                       ),
//                       pw.Text(
//                         'Total expenses are broken into different categories for closer look into where the money was spent.',
//                         textAlign: pw.TextAlign.justify,
//                       )
//                     ])),
//                 pw.SizedBox(width: 10),
//                 pw.Expanded(
//                   child: pw.Column(
//                     children: [
//                       pw.Container(
//                         alignment: pw.Alignment.centerLeft,
//                         padding: const pw.EdgeInsets.only(bottom: 10),
//                         child: pw.Text(
//                           'Spent vs. Saved',
//                           style: const pw.TextStyle(
//                             color: baseColor,
//                             fontSize: 16,
//                           ),
//                         ),
//                       ),
//                       pw.Text(
//                         'Budget was originally \$$budget. A total of \$$expense was spent on the month of January which exceeded the overall budget by \$${expense - budget}',
//                         textAlign: pw.TextAlign.justify,
//                       )
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ],
//         );
//       },
//     ),
//   );
//
//   // Second page with a pie chart
//   document.addPage(
//     pw.Page(
//       pageFormat: pageFormat,
//       theme: theme,
//       build: (context) {
//         const chartColors = [
//           PdfColors.blue300,
//           PdfColors.green300,
//           PdfColors.amber300,
//           PdfColors.pink300,
//           PdfColors.cyan300,
//           PdfColors.purple300,
//           PdfColors.lime300,
//         ];
//
//         return pw.Column(
//           children: [
//             pw.Flexible(
//               child: pw.Chart(
//                 title: pw.Text(
//                   'Expense breakdown',
//                   style: const pw.TextStyle(
//                     color: baseColor,
//                     fontSize: 20,
//                   ),
//                 ),
//                 grid: pw.PieGrid(),
//                 datasets: List<pw.Dataset>.generate(dataTable.length, (index) {
//                   final data = dataTable[index];
//                   final color = chartColors[index % chartColors.length];
//                   final value = (data[2] as num).toDouble();
//                   final pct = (value / expense * 100).round();
//                   return pw.PieDataSet(
//                     legend: '${data[0]}\n$pct%',
//                     value: value,
//                     color: color,
//                     legendStyle: const pw.TextStyle(fontSize: 10),
//                   );
//                 }),
//               ),
//             ),
//             table,
//           ],
//         );
//       },
//     ),
//   );
//
//   // Return the PDF file content
//   return document.save();
// }
// const PdfColor green = PdfColor.fromInt(0xff9ce5d0);
// const PdfColor lightGreen = PdfColor.fromInt(0xffcdf1e7);
// const sep = 120.0;
//
// Future<Uint8List> generateResume(PdfPageFormat format, CustomData data) async {
//   final doc = pw.Document(title: 'My Résumé', author: 'David PHAM-VAN');
//
//   final profileImage = pw.MemoryImage(
//     (await rootBundle.load('assets/profile.jpg')).buffer.asUint8List(),
//   );
//
//   final pageTheme = await _myPageTheme(format);
//
//   doc.addPage(
//     pw.MultiPage(
//       pageTheme: pageTheme,
//       build: (pw.Context context) => [
//         pw.Partitions(
//           children: [
//             pw.Partition(
//               child: pw.Column(
//                 crossAxisAlignment: pw.CrossAxisAlignment.start,
//                 children: <pw.Widget>[
//                   pw.Container(
//                     padding: const pw.EdgeInsets.only(left: 30, bottom: 20),
//                     child: pw.Column(
//                       crossAxisAlignment: pw.CrossAxisAlignment.start,
//                       children: <pw.Widget>[
//                         pw.Text('Parnella Charlesbois',
//                             textScaleFactor: 2,
//                             style: pw.Theme.of(context)
//                                 .defaultTextStyle
//                                 .copyWith(fontWeight: pw.FontWeight.bold)),
//                         pw.Padding(padding: const pw.EdgeInsets.only(top: 10)),
//                         pw.Text('Electrotyper',
//                             textScaleFactor: 1.2,
//                             style: pw.Theme.of(context)
//                                 .defaultTextStyle
//                                 .copyWith(
//                                 fontWeight: pw.FontWeight.bold,
//                                 color: green)),
//                         pw.Padding(padding: const pw.EdgeInsets.only(top: 20)),
//                         pw.Row(
//                           crossAxisAlignment: pw.CrossAxisAlignment.start,
//                           mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
//                           children: <pw.Widget>[
//                             pw.Column(
//                               crossAxisAlignment: pw.CrossAxisAlignment.start,
//                               children: <pw.Widget>[
//                                 pw.Text('568 Port Washington Road'),
//                                 pw.Text('Nordegg, AB T0M 2H0'),
//                                 pw.Text('Canada, ON'),
//                               ],
//                             ),
//                             pw.Column(
//                               crossAxisAlignment: pw.CrossAxisAlignment.start,
//                               children: <pw.Widget>[
//                                 pw.Text('+1 403-721-6898'),
//                                 _UrlText('p.charlesbois@yahoo.com',
//                                     'mailto:p.charlesbois@yahoo.com'),
//                                 _UrlText(
//                                     'wholeprices.ca', 'https://wholeprices.ca'),
//                               ],
//                             ),
//                             pw.Padding(padding: pw.EdgeInsets.zero)
//                           ],
//                         ),
//                       ],
//                     ),
//                   ),
//                   _Category(title: 'Work Experience'),
//                   _Block(
//                       title: 'Tour bus driver',
//                       icon: const pw.IconData(0xe530)),
//                   _Block(
//                       title: 'Logging equipment operator',
//                       icon: const pw.IconData(0xe30d)),
//                   _Block(title: 'Foot doctor', icon: const pw.IconData(0xe3f3)),
//                   _Block(
//                       title: 'Unicorn trainer',
//                       icon: const pw.IconData(0xf0cf)),
//                   _Block(
//                       title: 'Chief chatter', icon: const pw.IconData(0xe0ca)),
//                   pw.SizedBox(height: 20),
//                   _Category(title: 'Education'),
//                   _Block(title: 'Bachelor Of Commerce'),
//                   _Block(title: 'Bachelor Interior Design'),
//                 ],
//               ),
//             ),
//             pw.Partition(
//               width: sep,
//               child: pw.Column(
//                 children: [
//                   pw.Container(
//                     height: pageTheme.pageFormat.availableHeight,
//                     child: pw.Column(
//                       crossAxisAlignment: pw.CrossAxisAlignment.center,
//                       mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
//                       children: <pw.Widget>[
//                         pw.ClipOval(
//                           child: pw.Container(
//                             width: 100,
//                             height: 100,
//                             color: lightGreen,
//                             child: pw.Image(profileImage),
//                           ),
//                         ),
//                         pw.Column(children: <pw.Widget>[
//                           _Percent(size: 60, value: .7, title: pw.Text('Word')),
//                           _Percent(
//                               size: 60, value: .4, title: pw.Text('Excel')),
//                         ]),
//                         pw.BarcodeWidget(
//                           data: 'Parnella Charlesbois',
//                           width: 60,
//                           height: 60,
//                           barcode: pw.Barcode.qrCode(),
//                           drawText: false,
//                         ),
//                       ],
//                     ),
//                   ),
//                 ],
//               ),
//             )
//           ],
//         ),
//       ],
//     ),
//   );
//   return doc.save();
// }
//
// Future<pw.PageTheme> _myPageTheme(PdfPageFormat format) async {
//   final bgShape = await rootBundle.loadString('assets/resume.svg');
//
//   format = format.applyMargin(
//       left: 2.0 * PdfPageFormat.cm,
//       top: 4.0 * PdfPageFormat.cm,
//       right: 2.0 * PdfPageFormat.cm,
//       bottom: 2.0 * PdfPageFormat.cm);
//   return pw.PageTheme(
//     pageFormat: format,
//     theme: pw.ThemeData.withFont(
//       base: await PdfGoogleFonts.openSansRegular(),
//       bold: await PdfGoogleFonts.openSansBold(),
//       icons: await PdfGoogleFonts.materialIcons(),
//     ),
//     buildBackground: (pw.Context context) {
//       return pw.FullPage(
//         ignoreMargins: true,
//         child: pw.Stack(
//           children: [
//             pw.Positioned(
//               child: pw.SvgImage(svg: bgShape),
//               left: 0,
//               top: 0,
//             ),
//             pw.Positioned(
//               child: pw.Transform.rotate(
//                   angle: pi, child: pw.SvgImage(svg: bgShape)),
//               right: 0,
//               bottom: 0,
//             ),
//           ],
//         ),
//       );
//     },
//   );
// }
//
// class _Block extends pw.StatelessWidget {
//   _Block({
//     required this.title,
//     this.icon,
//   });
//
//   final String title;
//
//   final pw.IconData? icon;
//
//   @override
//   pw.Widget build(pw.Context context) {
//     return pw.Column(
//         crossAxisAlignment: pw.CrossAxisAlignment.start,
//         children: <pw.Widget>[
//           pw.Row(
//               crossAxisAlignment: pw.CrossAxisAlignment.start,
//               children: <pw.Widget>[
//                 pw.Container(
//                   width: 6,
//                   height: 6,
//                   margin: const pw.EdgeInsets.only(top: 5.5, left: 2, right: 5),
//                   decoration: const pw.BoxDecoration(
//                     color: green,
//                     shape: pw.BoxShape.circle,
//                   ),
//                 ),
//                 pw.Text(title,
//                     style: pw.Theme.of(context)
//                         .defaultTextStyle
//                         .copyWith(fontWeight: pw.FontWeight.bold)),
//                 pw.Spacer(),
//                 if (icon != null) pw.Icon(icon!, color: lightGreen, size: 18),
//               ]),
//           pw.Container(
//             decoration: const pw.BoxDecoration(
//                 border: pw.Border(left: pw.BorderSide(color: green, width: 2))),
//             padding: const pw.EdgeInsets.only(left: 10, top: 5, bottom: 5),
//             margin: const pw.EdgeInsets.only(left: 5),
//             child: pw.Column(
//                 crossAxisAlignment: pw.CrossAxisAlignment.start,
//                 children: <pw.Widget>[
//                   pw.Lorem(length: 20),
//                 ]),
//           ),
//         ]);
//   }
// }
//
// class _Category extends pw.StatelessWidget {
//   _Category({required this.title});
//
//   final String title;
//
//   @override
//   pw.Widget build(pw.Context context) {
//     return pw.Container(
//       decoration: const pw.BoxDecoration(
//         color: lightGreen,
//         borderRadius: pw.BorderRadius.all(pw.Radius.circular(6)),
//       ),
//       margin: const pw.EdgeInsets.only(bottom: 10, top: 20),
//       padding: const pw.EdgeInsets.fromLTRB(10, 4, 10, 4),
//       child: pw.Text(
//         title,
//         textScaleFactor: 1.5,
//       ),
//     );
//   }
// }
//
// class _Percent extends pw.StatelessWidget {
//   _Percent({
//     required this.size,
//     required this.value,
//     required this.title,
//   });
//
//   final double size;
//
//   final double value;
//
//   final pw.Widget title;
//
//   static const fontSize = 1.2;
//
//   PdfColor get color => green;
//
//   static const backgroundColor = PdfColors.grey300;
//
//   static const strokeWidth = 5.0;
//
//   @override
//   pw.Widget build(pw.Context context) {
//     final widgets = <pw.Widget>[
//       pw.Container(
//         width: size,
//         height: size,
//         child: pw.Stack(
//           alignment: pw.Alignment.center,
//           fit: pw.StackFit.expand,
//           children: <pw.Widget>[
//             pw.Center(
//               child: pw.Text(
//                 '${(value * 100).round().toInt()}%',
//                 textScaleFactor: fontSize,
//               ),
//             ),
//             pw.CircularProgressIndicator(
//               value: value,
//               backgroundColor: backgroundColor,
//               color: color,
//               strokeWidth: strokeWidth,
//             ),
//           ],
//         ),
//       )
//     ];
//
//     widgets.add(title);
//
//     return pw.Column(children: widgets);
//   }
// }
//
// class _UrlText extends pw.StatelessWidget {
//   _UrlText(this.text, this.url);
//
//   final String text;
//   final String url;
//
//   @override
//   pw.Widget build(pw.Context context) {
//     return pw.UrlLink(
//       destination: url,
//       child: pw.Text(text,
//           style: const pw.TextStyle(
//             decoration: pw.TextDecoration.underline,
//             color: PdfColors.blue,
//           )),
//     );
//   }
// }
// class CustomData {
//   const CustomData({
//     this.name = '[your name]',
//     this.testing = false,
//   });
//
//   final String name;
//
//   final bool testing;
// }
// const examples = <Example>[
//   Example('RÉSUMÉ', 'resume.dart', generateResume),
//   Example('DOCUMENT', 'document.dart', generateDocument),
//   Example('INVOICE', 'invoice.dart', generateInvoice),
//   Example('REPORT', 'report.dart', generateReport),
//   // Example('CALENDAR', 'calendar.dart', generateCalendar),
//   Example('CERTIFICATE', 'certificate.dart', generateCertificate, true),
// ];
//
// typedef LayoutCallbackWithData = Future<Uint8List> Function(
//     PdfPageFormat pageFormat, CustomData data);
//
// class Example {
//   const Example(this.name, this.file, this.builder, [this.needsData = false]);
//
//   final String name;
//
//   final String file;
//
//   final LayoutCallbackWithData builder;
//
//   final bool needsData;
// }
//
// class MyApp extends StatefulWidget {
//   const MyApp({Key? key}) : super(key: key);
//
//   @override
//   MyAppState createState() {
//     return MyAppState();
//   }
// }
//
// class MyAppState extends State<MyApp> with SingleTickerProviderStateMixin {
//   int _tab = 0;
//   TabController? _tabController;
//
//   PrintingInfo? printingInfo;
//
//   var _data = const CustomData();
//   var _hasData = false;
//   var _pending = false;
//
//   @override
//   void initState() {
//     super.initState();
//     _init();
//   }
//
//   @override
//   void didChangeDependencies() {
//     super.didChangeDependencies();
//   }
//
//   Future<void> _init() async {
//     final info = await Printing.info();
//
//     _tabController = TabController(
//       vsync: this,
//       length: examples.length,
//       initialIndex: _tab,
//     );
//     _tabController!.addListener(() {
//       if (_tab != _tabController!.index) {
//         setState(() {
//           _tab = _tabController!.index;
//         });
//       }
//       if (examples[_tab].needsData && !_hasData && !_pending) {
//         _pending = true;
//         askName(context).then((value) {
//           if (value != null) {
//             setState(() {
//               _data = CustomData(name: value);
//               _hasData = true;
//               _pending = false;
//             });
//           }
//         });
//       }
//     });
//
//     setState(() {
//       printingInfo = info;
//     });
//   }
//
//   void _showPrintedToast(BuildContext context) {
//     ScaffoldMessenger.of(context).showSnackBar(
//       const SnackBar(
//         content: Text('Document printed successfully'),
//       ),
//     );
//   }
//
//   void _showSharedToast(BuildContext context) {
//     ScaffoldMessenger.of(context).showSnackBar(
//       const SnackBar(
//         content: Text('Document shared successfully'),
//       ),
//     );
//   }
//
//   Future<void> _saveAsFile(
//       BuildContext context,
//       LayoutCallback build,
//       PdfPageFormat pageFormat,
//       ) async {
//     final bytes = await build(pageFormat);
//
//     final appDocDir = await getApplicationDocumentsDirectory();
//     final appDocPath = appDocDir.path;
//     final file = File('$appDocPath/document.pdf');
//     print('Save as file ${file.path} ...');
//     await file.writeAsBytes(bytes);
//     // await OpenFile.open(file.path);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     pw.RichText.debug = true;
//
//     if (_tabController == null) {
//       return const Center(child: CircularProgressIndicator());
//     }
//
//     final actions = <PdfPreviewAction>[
//       if (!kIsWeb)
//         PdfPreviewAction(
//           icon: const Icon(Icons.save),
//           onPressed: _saveAsFile,
//         )
//     ];
//
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Flutter PDF Demo'),
//         bottom: TabBar(
//           controller: _tabController,
//           tabs: examples.map<Tab>((e) => Tab(text: e.name)).toList(),
//           isScrollable: true,
//         ),
//       ),
//       body: PdfPreview(
//         maxPageWidth: 700,
//         build: (format) => examples[_tab].builder(format, _data),
//         actions: actions,
//         onPrinted: _showPrintedToast,
//         onShared: _showSharedToast,
//       ),
//       floatingActionButton: FloatingActionButton(
//         // backgroundColor: Colors.deepOrange,
//         onPressed: _showSources,
//         child: const Icon(Icons.code),
//       ),
//     );
//   }
//
//   void _showSources() {
//     ul.launchUrl(
//       Uri.parse(
//         'https://github.com/DavBfr/dart_pdf/blob/master/demo/lib/examples/${examples[_tab].file}',
//       ),
//     );
//   }
//
//   Future<String?> askName(BuildContext context) {
//     return showDialog<String>(
//         barrierDismissible: false,
//         context: context,
//         builder: (context) {
//           final controller = TextEditingController();
//
//           return AlertDialog(
//             title: const Text('Please type your name:'),
//             contentPadding: const EdgeInsets.symmetric(horizontal: 20),
//             content: TextField(
//               decoration: const InputDecoration(hintText: '[your name]'),
//               controller: controller,
//             ),
//             actions: [
//               TextButton(
//                 onPressed: () {
//                   if (controller.text != '') {
//                     Navigator.pop(context, controller.text);
//                   }
//                 },
//                 child: const Text('OK'),
//               ),
//             ],
//           );
//         });
//   }
// }
//
//
//
//
// class App extends StatelessWidget {
//   const App({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     final scrollbarTheme = ScrollbarThemeData(
//       thumbVisibility: MaterialStateProperty.all(true),
//     );
//
//     return MaterialApp(
//       theme: ThemeData.light().copyWith(scrollbarTheme: scrollbarTheme),
//       darkTheme: ThemeData.dark().copyWith(scrollbarTheme: scrollbarTheme),
//       title: 'Flutter PDF Demo',
//       home: const MyApp(),
//     );
//   }
// }
