import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:path_drawing/path_drawing.dart';
import 'package:wordly/games/word_game/setting/persistence/local_storage_settings_persistence.dart';
import 'animations/page_navigation_animation.dart';
import 'games/word_game/ads/ads_controller.dart';
import 'games/word_game/ads/app_ads.dart';
import 'games/word_game/app_lifecycle/app_lifecycle.dart';
import 'games/word_game/audio/audio_controller.dart';
import 'games/word_game/in_app_purchase/in_app_purchase.dart';
import 'games/word_game/profile/profile_screen.dart';
import 'games/word_game/provider/widget_provider.dart';
import 'games/word_game/setting/persistence/settings_persistence.dart';
import 'games/word_game/setting/setting_controller.dart';
import 'games/word_game/setting/setting_screen.dart';
import 'games/word_game/view/coin_buy/coin_buy_screen.dart';
import 'games/word_game/view/dashboard/dashboard_screen.dart';
import 'games/word_game/view/dashboard/level_selection.dart';
import 'games/word_game/view/start_view/start_screen.dart';
import 'games/word_game/view/widgets/game_coin_widget.dart';
import 'games/word_game/view/win/win_screen.dart';
import 'word-mine/view/word_game.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_strategy/url_strategy.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setPathUrlStrategy();
  MobileAds.instance.initialize();
  // await GetStorage.init();
  AdsController? adsController;
  if (!kIsWeb && (Platform.isIOS || Platform.isAndroid)) {
    adsController = AdsController(MobileAds.instance);
    adsController.initialize();
  }
  // InAppPurchaseController? inAppPurchaseController;
  // if (!kIsWeb && (Platform.isIOS || Platform.isAndroid)) {
  //   inAppPurchaseController = InAppPurchaseController(InAppPurchase.instance)
  //     ..subscribe();
  //   inAppPurchaseController.restorePurchases();
  // }

  SystemChrome.setEnabledSystemUIMode(
    SystemUiMode.manual,
    overlays: [],
  ).then((value) => runApp(
          // MaterialApp(home: AppAds(adsType: AdsType.nativeAd,),)
          App(
        settingsPersistence: LocalStorageSettingsPersistence(),
        adsController: adsController,
      )));
}

class App extends StatefulWidget {
  const App({super.key, required this.settingsPersistence, this.adsController});

  static final _router = GoRouter(
    routes: [
      GoRoute(
        path: '/',
        name: '/',
        builder: (context, state) => const DashBoardScreen(
          key: Key('dashBoardScreen'),
          isSuccess: false,
        ),
        // builder: (context, state) => const SplashLoader(),
        routes: [
          GoRoute(
            path: 'dashBoardScreen',
            name: 'dashBoardScreen',
            pageBuilder: (context, state) => buildTransition<void>(
              child: DashBoardScreen(
                  key: const Key('dashBoardScreen'),
                  isSuccess: (state.extra ?? false) as bool),
              color: const Color(0xff002a6a),
            ),
          ),
          GoRoute(
            path: 'win',
            name: 'win',
            pageBuilder: (context, state) => buildTransition<void>(
              child: const MyAnimatedImage(key: Key('win')),
              color: const Color(0xff002a6a),
            ),
          ),
          GoRoute(
            path: 'start',
            name: 'start',
            pageBuilder: (context, state) => buildTransition<void>(
              child: const StartScreen(key: Key('start')),
              color: const Color(0xff002a6a),
            ),
          ),
          GoRoute(
            path: 'gameBoard',
            name: 'gameBoard',
            pageBuilder: (context, state) => buildTransition<void>(
              child: WordLy(wordLevelBox: state.extra as WordLevelBox),
              color: const Color(0xff002a6a),
            ),
          ),
          GoRoute(
            path: 'levelMap',
            name: 'levelMap',
            pageBuilder: (context, state) => buildTransition<void>(
              child: const LevelSelectionScreen(key: Key('levelMap')),
              color: const Color(0xff002a6a),
            ),
          ),
          GoRoute(
            path: 'settingScreen',
            name: 'settingScreen',
            pageBuilder: (context, state) => buildTransition<void>(
              child: const SettingScreen(key: Key('settingScreen')),
              color: const Color(0xff002a6a),
            ),
          ),
          GoRoute(
            path: 'profileScreen',
            name: 'profileScreen',
            pageBuilder: (context, state) => buildTransition<void>(
              child: const ProfileScreen(key: Key('profileScreen')),
              color: const Color(0xff002a6a),
            ),
          ),
        ],
      ),
    ],
  );
  final SettingsPersistence settingsPersistence;
  final AdsController? adsController;

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return AppLifecycleObserver(
      child: MultiProvider(
        providers: [
          // ChangeNotifierProvider(
          //   create: (context) {
          //     var progress = PlayerProgress(playerProgressPersistence);
          //     progress.getLatestFromStore();
          //     return progress;
          //   },
          // ),
          // Provider<GamesServicesController?>.value(
          //     value: gamesServicesController),
          // Provider<AdsController?>.value(value: adsController),
          // ChangeNotifierProvider<InAppPurchaseController?>.value(
          //     value: inAppPurchaseController),
          // Provider(
          //   create: (context) => Palette(),
          // ),
          Provider<SettingsController>(
            lazy: false,
            create: (context) => SettingsController(
              persistence: widget.settingsPersistence,
            )..loadStateFromPersistence(),
          ),
          ProxyProvider2<SettingsController, ValueNotifier<AppLifecycleState>,
              AudioController>(
            lazy: false,
            create: (context) => AudioController()..initialize(),
            update: (context, settings, lifecycleNotifier, audio) {
              if (audio == null) throw ArgumentError.notNull();
              audio.attachSettings(settings);
              audio.attachLifecycleNotifier(lifecycleNotifier);
              return audio;
            },
            dispose: (context, audio) => audio.dispose(),
          ),
          ChangeNotifierProvider(
            create: (_) => Counter(),
          ),
          ChangeNotifierProvider(
            create: (_) => WidgetsProvider(),
          ),
          ChangeNotifierProvider(
            create: (_) => CoinProvider(this),
          ),
          ChangeNotifierProvider(create: (context) => HelpProvider(this)),
          ChangeNotifierProvider(create: (context) => CoinProvider(this)),
          ChangeNotifierProvider(create: (context) => LiveProvider(this)),

        ],
        child: Builder(builder: (context) {
          // final palette = context.watch<Palette>();
          return MaterialApp.router(
            title: 'Flutter Demo',
            debugShowCheckedModeBanner: false,
            routeInformationProvider: App._router.routeInformationProvider,
            routeInformationParser: App._router.routeInformationParser,
            routerDelegate: App._router.routerDelegate,
            scaffoldMessengerKey: GlobalKeys.scaFoldMessageKey,
            // theme: ThemeData.from(
            //   colorScheme: ColorScheme.fromSeed(
            //     seedColor: palette.darkPen,
            //     background: palette.backgroundMain,
            //   ),
            //   textTheme: TextTheme(
            //     bodyMedium: TextStyle(
            //       color: palette.ink,
            //     ),
            //   ),
            // ),
          );
        }),
      ),
    );
  }
}

class GlobalKeys {
  GlobalKeys._privateConstructor();

  static final navigationKey = GlobalKey<NavigatorState>();
  static final scaFoldMessageKey = GlobalKey<ScaffoldMessengerState>();
}

class DottedBorderPainter extends CustomPainter {
  final Color color;
  final double strokeWidth;

  DottedBorderPainter({this.color = Colors.black, this.strokeWidth = 1.0});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = Colors.yellow
      ..style = PaintingStyle.stroke
      ..strokeWidth = 30.0;
    Paint paint2 = Paint()
      ..color = Color(0xff0275bc)
      ..style = PaintingStyle.stroke
      ..strokeWidth = 30.0;

    Path path = Path();
    path.moveTo(0, 0);
    path.lineTo(size.width + 2, 0);
    path.lineTo(size.width + 2, size.height + 2);
    path.lineTo(0, size.height + 2);
    path.close();
    canvas.drawPath(
      path,
      paint,
    );
    canvas.drawPath(
      dashPath(
        path,
        dashArray: CircularIntervalList([20, 20]),
      ),
      paint2,
    );

    // canvas.drawPath(
    //   dashPath(
    //     path,
    //     dashArray: CircularIntervalList([0.1]),
    //   ),
    //   paint,
    // );
  }

  @override
  bool shouldRepaint(DottedBorderPainter oldDelegate) => false;
}
