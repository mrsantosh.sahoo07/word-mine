import 'dart:async';
import 'dart:io';
import 'dart:math' as math;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:go_router/go_router.dart';
import 'package:google_generative_ai/google_generative_ai.dart';
import 'package:provider/provider.dart';
import '../../animations/coin_animation.dart';
import '../../animations/deal_widgets.dart';
import '../../games/word_game/audio/audio_controller.dart';
import '../../games/word_game/audio/sounds.dart';
import '../../games/word_game/setting/setting_controller.dart';
import '../../games/word_game/setting/setting_dialog.dart';
import '../../games/word_game/view/dashboard/dashboard_screen.dart';
import '../../games/word_game/view/dashboard/widgets/game_appbar.dart';
import '../widgets/drag_word_widgets.dart';

class WordLy extends StatefulWidget {
  const WordLy({super.key, required this.wordLevelBox});

  final WordLevelBox wordLevelBox;

  @override
  State<WordLy> createState() => _MyAppState();
}

class _MyAppState extends State<WordLy> with TickerProviderStateMixin {
  late AnimationController controller;
  late AnimationController loaderController;
  late Animation<double> loaderAnimation;
  late Animation<double> offsetAnimation;
  late AnimationController wordController;
  late Animation<double> wordOffsetAnimation;
  double animateSize = 2;
  List<WordLevelBox> wordLevelBoxList = [];
  final GlobalKey _startKey = GlobalKey();
  Offset _startOffset = Offset.zero;

  @override
  void initState() {
    controller = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this);
    loaderController = AnimationController(
        duration: const Duration(milliseconds: 5000), vsync: this);
    loaderAnimation = Tween(begin: 75.0, end: 430.0).animate(loaderController);
    loaderController.forward();
    offsetAnimation = Tween(begin: 0.0, end: animateSize)
        .chain(CurveTween(curve: Curves.elasticIn))
        .animate(controller)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        }
      });
    wordController = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this);
    wordOffsetAnimation = Tween(begin: 0.0, end: animateSize)
        .chain(CurveTween(curve: Curves.elasticIn))
        .animate(wordController)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          wordController.reverse();
        }
      });

    // generateWordBoxes(widget.index);
    WidgetsBinding.instance.addPostFrameCallback((c) {
      _startOffset = (_startKey.currentContext?.findRenderObject() as RenderBox)
          .localToGlobal(Offset.zero);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var viewPadding = MediaQuery.of(context).viewPadding;
    final audioController = context.read<AudioController>();
    final settings = context.read<SettingsController>();
    bool bigger =
        widget.wordLevelBox.paddingCount > widget.wordLevelBox.crossAxisCount;

    return PopScope(
      canPop: false,
      onPopInvoked: (didPop) {
        if (Platform.isAndroid) {
          showAnimateDialog(context, const QuiteDialog());
        }
      },
      child: Container(
        padding: Platform.isAndroid
            ? EdgeInsets.only(top: viewPadding.top != 0 ? viewPadding.top : 35)
            : null,
        color: const Color(0xff002a6a),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: gameAppBar(context),
          body: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            width: kIsWeb ? 400 : size.width,
            height: size.height,
            decoration: const BoxDecoration(
              gradient: RadialGradient(
                tileMode: TileMode.clamp,
                center: Alignment.center,
                radius: 0.8,
                colors: [
                  Color(0xff0275bc),
                  Color(0xff002a6a),
                ],
              ),
            ),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    physics: const NeverScrollableScrollPhysics(),
                    child: Column(
                      children: [
                        /// All word box view
                        DelayedDisplay(
                          delay: const Duration(milliseconds: 800),
                          child: GridView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            padding: EdgeInsets.symmetric(
                              horizontal: widget.wordLevelBox.paddingCount == 3
                                  ? 60
                                  : widget.wordLevelBox.paddingCount == 4
                                      ? (bigger ? 65 : 30)
                                      : widget.wordLevelBox.paddingCount == 5
                                          ? (bigger ? 55 : 30)
                                          : widget.wordLevelBox.paddingCount ==
                                                  6
                                              ? (bigger ? 50 : 30)
                                              : widget.wordLevelBox
                                                          .paddingCount ==
                                                      7
                                                  ? (bigger ? 45 : 30)
                                                  : 0,
                            ),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount:
                                  widget.wordLevelBox.crossAxisCount,
                            ),
                            itemCount: widget.wordLevelBox.wordBoxList.length,
                            itemBuilder: (context, index) {
                              WordBox wordBox =
                                  widget.wordLevelBox.wordBoxList[index];
                              // WordBox wordBox = wordBoxList[index];
                              return wordBox.showBox
                                  ? AppListItem(
                                      key: wordBox.globalKey,
                                      index: index,
                                      offsetAnimation: offsetAnimation,
                                      animateSize: animateSize,
                                      onClick: (key) {},
                                      wordBox: wordBox,
                                    )
                                  : Container(
                                      key: wordBox.globalKey,
                                    ); // Show empty container for blank space
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                /// Drag Words
                DelayedDisplay(
                  delay: const Duration(milliseconds: 1200),
                  child: AnimatedBuilder(
                      animation: wordController,
                      builder: (buildContext, child) {
                        bool isBlank = (context.watch<Counter>().word != "");
                        return GestureDetector(
                          key: _startKey,
                          onTap: () async {
                            // context.push('/win');
                            // showWinDialog(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                                left: wordOffsetAnimation.value + 24.0,
                                right: 24.0 - wordOffsetAnimation.value),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              border: isBlank
                                  ? Border.all(color: Colors.black)
                                  : Border.all(color: Colors.transparent),
                              color: isBlank
                                  ? const Color(0xFF149623)
                                  : Colors.transparent,
                            ),
                            child: Text(
                              // key: _startKey,
                              context.watch<Counter>().word,
                              style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w900,
                                letterSpacing: 2,
                                fontSize: 35,
                              ),
                            ),
                          ),
                        );
                      }),
                ),
                const SizedBox(height: 10),

                /// Drag  controller
                DelayedDisplay(
                  delay: const Duration(milliseconds: 1200),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 12.0),
                        child: GradientShadowButton(
                          strokeWidth: 0.4,
                          padding: const EdgeInsets.all(5),
                          gradientColors: GameColors.appBackgroundGradient,
                          onTap: () {
                            showAnimateDialog(
                              context,
                              ChatWidget(),
                            );
                          },
                          borderRadius: 500,
                          shadowWith: 1.5,
                          shadowColors: const Color(0xff1b6b00),
                          child: Image.asset(
                            "assets/images/genie.png",
                            height: 25,
                            width: 25,
                          ),
                        ),
                      ),
                      DragWordWidgets(
                        onPressed: (word) {
                          wordWithAnimation(word);
                        },
                        onEnd: (word) {
                          if (!widget.wordLevelBox.wordAnsList
                              .any((element) => element.word == word)) {
                            wordController.forward(from: 0.0);
                            Timer(const Duration(seconds: 1), () {
                              context.read<Counter>().addWord(word);
                            });
                          }
                        },
                        wordList: widget.wordLevelBox.dragWordList,
                        size: Size(size.width / 1.5, size.width / 1.5),
                      ),
                      SizedBox(
                        height: size.width / 1.5,
                        // Adjust this value as per your requirement
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ValueListenableBuilder<int>(
                              valueListenable:
                                  context.watch<SettingsController>().help,
                              builder: (context, value, child) =>
                                  GestureDetector(
                                onTap: () {
                                  getHelp(value);
                                },
                                child: AbsorbPointer(
                                  child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 12, right: 5),
                                        child: GradientShadowButton(
                                          strokeWidth: 0.4,
                                          padding: const EdgeInsets.all(5),
                                          gradientColors:
                                              GameColors.appBackgroundGradient,
                                          onTap: () {},
                                          borderRadius: 500,
                                          shadowWith: 1.5,
                                          shadowColors: const Color(0xff1b6b00),
                                          child: Image.asset(
                                            "assets/images/wHelper.png",
                                            height: 25,
                                            width: 25,
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        right: 0,
                                        top: 0,
                                        child: Container(
                                          padding: const EdgeInsets.all(5),
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.red,
                                          ),
                                          child: Text(
                                            value.toString(),
                                            style: const TextStyle(
                                              color: Colors.white,
                                              fontSize: 18,
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            GradientShadowButton(
                              strokeWidth: 0.5,
                              borderRadius: 100,
                              padding: const EdgeInsets.all(5),
                              gradientColors: GameColors.redGradient,
                              onTap: () {
                                showAnimateDialog(context, const QuiteDialog());
                              },
                              child: const Icon(Icons.exit_to_app,
                                  size: 25, color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 5),
              ],
            ),
          ),
          bottomNavigationBar: const ButtonNavigationAdsWidgets(),
        ),
      ),
    );
  }

  _addAnimationWidgets(Offset endOffset, {int quarterTurn = 0}) {
    OverlayEntry? overlayEntry = OverlayEntry(builder: (_) {
      return AtoBAnimation(
          startPosition: _startOffset,
          endPosition: endOffset,
          dxCurveAnimation: 0,
          dyCurveAnimation: 0,
          duration: const Duration(milliseconds: 1000),
          opacity: 1,
          child: overlayWidget(quarterTurn));
    });

    // Show Overlay
    Overlay.of(context).insert(overlayEntry);
    Future.delayed(const Duration(milliseconds: 1000), () {
      overlayEntry?.remove();
      overlayEntry = null;
    });
  }

  Widget overlayWidget(int query) {
    text(String letter) => Text(
          letter,
          style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w900,
            letterSpacing: 50,
            fontSize: 50,
          ),
        );
    var list = context.watch<Counter>().word.toUpperCase().split('');
    if (query == 0) {
      return Row(
        children: [
          for (var letter in list) ...[text(letter), const SizedBox(width: 5)]
        ],
      );
    } else {
      return Column(
        children: [
          for (var letter in list) ...[text(letter), const SizedBox(height: 5)]
        ],
      );
    }
  }

  getHelp(int value) {
    final settings = context.read<SettingsController>();
    int index = settings.wordLevelBoxList.value
        .indexWhere((v) => v == widget.wordLevelBox);
    WordLevelBox wordLevelBoxes = settings.wordLevelBoxList.value[index];
    List<WordBox> filteredList = wordLevelBoxes.wordBoxList
        .where((word) => word.showBox == true && word.isCorrect != true)
        .toList();
    // List<WordBox> filteredList = widget.wordLevelBox.wordBoxList
    //     .where((word) => word.showBox == true && word.isCorrect != true)
    //     .toList();
    List<int> indicesList = filteredList.map((word) => word.index).toList();
    if (indicesList.isNotEmpty) {
      int randomIndex = math.Random().nextInt(indicesList.length);
      WordBox randomObject = widget.wordLevelBox.wordBoxList
          .firstWhere((word) => word.index == indicesList[randomIndex]);
      for (var action in widget.wordLevelBox.wordBoxList) {
        if (action == randomObject) {
          setState(() {
            action.showBox = true;
            action.isCorrect = true;
          });
        }
      }
      settings.wordLevelBoxList.value[index] = wordLevelBoxes;
      // settings.saveGameLevel(settings.wordLevelBoxList.value);
      int helpCount = value;
      settings.setHelp(helpCount - 1);
      setState(() {});
    } else {
      levelComplete();
    }
  }

  wordWithAnimation(String word) async {
    context.read<Counter>().addWord(word);
    // int currentLevel = await GameStorage.getCurrentLevel();
    if (word.isEmpty) return;
    if (widget.wordLevelBox.wordAnsList
        .any((element) => element.word == word)) {
      WordAnswer? wordAnswer = widget.wordLevelBox.wordAnsList
          .where((element) => element.word == word)
          .first;
      List<int>? value = wordAnswer.wordIndex;
      final int index = value.first;
      final offset = getOffsets(index);
      if (value.every((element) =>
          widget.wordLevelBox.wordBoxList[element].isCorrect == true)) {
        for (var element in value) {
          widget.wordLevelBox.wordBoxList[element].showAnimation = true;
          setState(() {});
        }
        controller.forward(from: 0.0);
      } else {
        _addAnimationWidgets(offset, quarterTurn: wordAnswer.rotateQuery);
        await Future.delayed(const Duration(seconds: 1));
        for (var element in value) {
          widget.wordLevelBox.wordBoxList[element].isCorrect = true;
          setState(() {});
        }
      }
    } else {}
    levelComplete();
    await Future.delayed(const Duration(milliseconds: 2000));
    for (var element in widget.wordLevelBox.wordBoxList) {
      element.showAnimation = false;
    }
    setState(() {});
  }

  levelComplete() {
    bool levelComplete = widget.wordLevelBox.wordBoxList
        .every((element) => element.isCorrect == true);
    final audioController = context.read<AudioController>();
    if (levelComplete) {
      if (!mounted) return;
      audioController.playSfx(Sounds.win1);
      showDialog(
        context: context,
        builder: (_) => const FunkyOverlay(),
      );
      Future.delayed(const Duration(seconds: 1)).then((value) async {
        context.pop();
      });
      Future.delayed(const Duration(seconds: 2)).then((value) async {
        final settings = context.read<SettingsController>();
        int index = settings.wordLevelBoxList.value
            .indexWhere((v) => v == widget.wordLevelBox);
        showWinDialog(context, settings.level.value == index);
        if (settings.level.value == index) {
          settings.setGameLevel(settings.level.value + 1);
          settings.loadStateFromPersistence();
        }
        for (var word in widget.wordLevelBox.wordBoxList) {
          word.isCorrect = false;
        }
      });
    }
    setState(() {});
  }

  Offset getOffsets(int index) {
    var key = widget.wordLevelBox.wordBoxList[index].globalKey;
    RenderBox? renderBox =
        key?.currentContext?.findRenderObject() as RenderBox?;
    if (renderBox != null) {
      Offset offset = renderBox.localToGlobal(Offset.zero);
      return offset;
    } else {
      return Offset.zero;
    }
  }

  @override
  void dispose() {
    controller.dispose();
    offsetAnimation.removeListener(() {});
    loaderAnimation.removeListener(() {});
    wordController.dispose();
    loaderController.dispose();
    wordOffsetAnimation.removeListener(() {});
    super.dispose();
  }
}

class AppListItem extends StatelessWidget {
  final GlobalKey widgetKey = GlobalKey();
  final int index;
  final double animateSize;
  final WordBox wordBox;
  final Animation<double> offsetAnimation;
  final void Function(GlobalKey) onClick;

  AppListItem(
      {super.key,
      required this.onClick,
      required this.index,
      required this.animateSize,
      required this.offsetAnimation,
      required this.wordBox});

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: offsetAnimation,
      builder: (buildContext, child) {
        return GestureDetector(
          onTap: () {
            onClick(widgetKey);
          },
          child: Container(
            key: widgetKey,
            margin: wordBox.showAnimation == true
                ? EdgeInsets.only(
                    left: animateSize + offsetAnimation.value,
                    right: animateSize - offsetAnimation.value,
                    bottom: animateSize,
                    top: animateSize)
                : EdgeInsets.all(animateSize),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: Border.all(color: Colors.black),
              color: wordBox.showAnimation == true
                  ? Colors.green.shade400
                  : wordBox.isCorrect
                      ? const Color(0xFF149623)
                      : Colors.white38,
            ),
            alignment: Alignment.center,
            child: Text(
              wordBox.isCorrect ? (wordBox.name ?? '') : "",
              style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w900,
                fontSize: 30,
              ),
            ),
          ),
        );
      },
    );
  }
}

class Counter with ChangeNotifier, DiagnosticableTreeMixin {
  String _word = "";

  String get word => _word;

  void addWord(String words) {
    _word = words;
    notifyListeners();
  }
}

class WordLevelBox {
  WordLevelBox({
    this.name,
    this.wordBoxList = const [],
    this.wordAnsList = const [],
    this.crossAxisCount = 0,
    this.paddingCount = 0,
    this.dragWordList = const [],
  });

  String? name;
  List<WordBox> wordBoxList;
  List<WordAnswer> wordAnsList;
  List<String> dragWordList;
  int paddingCount;
  int crossAxisCount;

  // Convert a WordLevelBox object into a Map<String, dynamic>
  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'wordBoxList': wordBoxList.map((e) => e.toJson()).toList(),
      'wordAnsList': wordAnsList.map((e) => e.toJson()).toList(),
      'dragWordList': dragWordList,
      'paddingCount': paddingCount,
      'crossAxisCount': crossAxisCount,
    };
  }

  // Convert a Map<String, dynamic> into a WordLevelBox object
  factory WordLevelBox.fromJson(Map<String, dynamic> json) {
    return WordLevelBox(
      name: json['name'],
      wordBoxList: (json['wordBoxList'] as List<dynamic>)
          .map((e) => WordBox.fromJson(e))
          .toList(),
      wordAnsList: (json['wordAnsList'] as List<dynamic>)
          .map((e) => WordAnswer.fromJson(e))
          .toList(),
      dragWordList: List<String>.from(json['dragWordList'] ?? []),
      paddingCount: json['paddingCount'] ?? 0,
      crossAxisCount: json['crossAxisCount'] ?? 0,
    );
  }

  // Create a new WordLevelBox object with updated properties
  WordLevelBox copyWith({
    String? name,
    List<WordBox>? wordBoxList,
    List<WordAnswer>? wordAnsList,
    List<String>? dragWordList,
    int? paddingCount,
    int? crossAxisCount,
  }) {
    return WordLevelBox(
      name: name ?? this.name,
      wordBoxList: wordBoxList ?? this.wordBoxList,
      wordAnsList: wordAnsList ?? this.wordAnsList,
      dragWordList: dragWordList ?? this.dragWordList,
      paddingCount: paddingCount ?? this.paddingCount,
      crossAxisCount: crossAxisCount ?? this.crossAxisCount,
    );
  }

  // Override toString method for better readability
  @override
  String toString() {
    return 'WordLevelBox{name: $name, wordBoxList: $wordBoxList, wordAnsList: $wordAnsList, dragWordList: $dragWordList, paddingCount: $paddingCount, crossAxisCount: $crossAxisCount}';
  }
}

class WordBox {
  final String? name;
  bool showBox;
  bool isCorrect;
  bool showAnimation;
  GlobalKey? globalKey;
  final int index;

  WordBox({
    this.name,
    required this.index,
    this.showBox = false,
    this.globalKey,
    this.isCorrect = false,
    this.showAnimation = false,
  }) {
    globalKey = GlobalKey();
  }

  factory WordBox.fromJson(Map<String, dynamic> json) {
    return WordBox(
      name: json['name'],
      index: json['index'],
      showBox: json['showBox'],
      globalKey: GlobalKey(),
      // Note: GlobalKey cannot be serialized
      isCorrect: json['isCorrect'],
      showAnimation: json['showAnimation'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'showBox': showBox,
      'isCorrect': isCorrect,
      'showAnimation': showAnimation,
      'index': index,
      // 'globalKey': globalKey, // Note: GlobalKey cannot be serialized
    };
  }

  WordBox copyWith({
    String? name,
    bool? showBox,
    bool? isCorrect,
    bool? showAnimation,
    GlobalKey? globalKey,
    int? index,
  }) {
    return WordBox(
      name: name ?? this.name,
      index: index ?? this.index,
      showBox: showBox ?? this.showBox,
      globalKey: globalKey ?? this.globalKey,
      isCorrect: isCorrect ?? this.isCorrect,
      showAnimation: showAnimation ?? this.showAnimation,
    );
  }

  @override
  String toString() {
    return 'WordBox{name: $name, showBox: $showBox, isCorrect: $isCorrect, showAnimation: $showAnimation, globalKey: $globalKey, index: $index}';
  }
}

class WordAnswer {
  String word;
  List<int> wordIndex;
  int rotateQuery;

  WordAnswer({
    required this.word,
    required this.wordIndex,
    this.rotateQuery = 0,
  });

  factory WordAnswer.fromJson(Map<String, dynamic> json) {
    return WordAnswer(
      word: json['word'],
      wordIndex: List<int>.from(json['wordIndex']),
      rotateQuery: json['rotateQuery'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'word': word,
      'wordIndex': wordIndex,
      'rotateQuery': rotateQuery,
    };
  }

  WordAnswer copyWith({
    String? word,
    List<int>? wordIndex,
    int? rotateQuery,
  }) {
    return WordAnswer(
      word: word ?? this.word,
      wordIndex: wordIndex ?? this.wordIndex,
      rotateQuery: rotateQuery ?? this.rotateQuery,
    );
  }

  @override
  String toString() {
    return 'WordAnswer{word: $word, wordIndex: $wordIndex, rotateQuery: $rotateQuery}';
  }
}

class FunkyOverlay extends StatefulWidget {
  const FunkyOverlay({super.key});

  @override
  State<StatefulWidget> createState() => FunkyOverlayState();
}

class FunkyOverlayState extends State<FunkyOverlay>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> scaleAnimation;

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
              color: Colors.transparent,
              child: Image.asset("assets/images/congrans.png")),
        ),
      ),
    );
  }
}

String apiKey = "AIzaSyBwAWVwfPkp8fQi6OZFyc94NU6SdtNdSLY";

class ChatWidget extends StatefulWidget {
  final List<WordAnswer> wordList;

  const ChatWidget({super.key, this.wordList = const []});

  @override
  State<ChatWidget> createState() => _ChatWidgetState();
}

class _ChatWidgetState extends State<ChatWidget> {
  late final GenerativeModel _model;
  late final ChatSession _chat;
  final ScrollController _scrollController = ScrollController();
  final TextEditingController _textController = TextEditingController();
  final FocusNode _textFieldFocus = FocusNode(debugLabel: 'TextField');
  bool _loading = false;

  @override
  void initState() {
    super.initState();
    _model = GenerativeModel(
      model: 'gemini-1.5-flash-latest',
      // model: 'gemini-pro',
      apiKey: apiKey,
    );
    _chat = _model.startChat();
  }

  void _scrollDown() {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) => _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: const Duration(
          milliseconds: 750,
        ),
        curve: Curves.easeOutCirc,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final history = _chat.history.toList();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Gemini AI"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(0.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Expanded(
              child: ListView.builder(
                controller: _scrollController,
                itemBuilder: (context, idx) {
                  final content = history[idx];
                  final text = content.parts
                      .whereType<TextPart>()
                      .map<String>((e) => e.text)
                      .join('');
                  return MessageWidget(
                    text: text,
                    isFromUser: content.role == 'user',
                  );
                },
                itemCount: history.length,
              ),
            ),
            for (int i = 0; i < widget.wordList.length; i++)
              Padding(
                padding: const EdgeInsets.only(bottom: 2.0),
                child: Chip(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(20),
                          topLeft: Radius.circular(20))),
                  label: GestureDetector(
                    onTap: () {
                      _sendChatMessage(
                          "write random word include cat, act and write total 10 words");
                    },
                    child: const Text(
                      "Help me to find words",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  backgroundColor: const Color(0xff0275bc),
                ),
              ),
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 25,
                horizontal: 15,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      autofocus: true,
                      focusNode: _textFieldFocus,
                      decoration:
                          textFieldDecoration(context, 'Enter a prompt...'),
                      controller: _textController,
                      onSubmitted: (String value) {
                        _sendChatMessage(value);
                      },
                    ),
                  ),
                  const SizedBox.square(dimension: 15),
                  if (!_loading)
                    IconButton(
                      onPressed: () async {
                        _sendChatMessage(_textController.text);
                      },
                      icon: Icon(
                        Icons.send,
                        color: Theme.of(context).colorScheme.primary,
                      ),
                    )
                  else
                    const CircularProgressIndicator(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _sendChatMessage(String message) async {
    setState(() {
      _loading = true;
    });

    try {
      final response = await _chat.sendMessage(
        Content.text(message),
      );
      final text = response.text;

      if (text == null) {
        _showError('Empty response.');
        return;
      } else {
        setState(() {
          _loading = false;
          _scrollDown();
        });
      }
    } catch (e) {
      _showError(e.toString());
      setState(() {
        _loading = false;
      });
    } finally {
      _textController.clear();
      setState(() {
        _loading = false;
      });
      _textFieldFocus.requestFocus();
    }
  }

  void _showError(String message) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Something went wrong'),
          content: SingleChildScrollView(
            child: Text(message),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('OK'),
            )
          ],
        );
      },
    );
  }
}

class MessageWidget extends StatelessWidget {
  const MessageWidget({
    super.key,
    required this.text,
    required this.isFromUser,
  });

  final String text;
  final bool isFromUser;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment:
          isFromUser ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: [
        Flexible(
          child: Container(
            constraints: const BoxConstraints(maxWidth: 480),
            decoration: BoxDecoration(
              color: isFromUser
                  ? Theme.of(context).colorScheme.primaryContainer
                  : Theme.of(context).colorScheme.background,
              borderRadius: BorderRadius.circular(18),
            ),
            padding: const EdgeInsets.symmetric(
              vertical: 15,
              horizontal: 20,
            ),
            margin: const EdgeInsets.only(bottom: 8),
            child: MarkdownBody(data: text),
          ),
        ),
      ],
    );
  }
}

InputDecoration textFieldDecoration(BuildContext context, String hintText) =>
    InputDecoration(
      contentPadding: const EdgeInsets.all(15),
      hintText: hintText,
      border: OutlineInputBorder(
        borderRadius: const BorderRadius.all(
          Radius.circular(14),
        ),
        borderSide: BorderSide(
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: const BorderRadius.all(
          Radius.circular(14),
        ),
        borderSide: BorderSide(
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
    );
