import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../utils/typedef.dart';
import '../view/word_game.dart';
import 'custom_painter/pattern_painter.dart';

class DragWordWidgets extends StatefulWidget {
  const DragWordWidgets({
    Key? key,
    this.onPressed,
    this.onEnd,
    this.size=const Size(300, 300),
    this.wordList = const ["", "", "", ""],
  }) : super(key: key);
  final VoidCallbackWithData? onPressed;
  final VoidCallbackWithData? onEnd;
  final Size size;
  final List<String> wordList;

  @override
  State<DragWordWidgets> createState() => _HomePageState();
}

class _HomePageState extends State<DragWordWidgets> {
  List<int> selectedCirclesIndexes = [];
  Offset? currentOffset;
  String dragWord = "";
  bool dragAnimation = false;

  @override
  void initState() {
    callFirstTimeAnimation();
    super.initState();
  }

  callFirstTimeAnimation() async {
    await Future.delayed(const Duration(seconds: 2));
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanUpdate: (details) async {
        final renderbox = context.findRenderObject() as RenderBox;
        final position = renderbox.globalToLocal(details.globalPosition);
        setState(() {
          currentOffset = position;
        });
        for (var i = 0; i < widget.wordList.length; i++) {
          final circlePosition = getCurrentCirclePosition(
            renderbox.size,
            widget.wordList.length,
            i,
          );
          final distance = (circlePosition - position).distance;
          if (distance < 40 && !selectedCirclesIndexes.contains(i)) {
            setState(() {
              dragWord += widget.wordList[i].toUpperCase();
              selectedCirclesIndexes.add(i);

            });
            widget.onPressed!(dragWord.toUpperCase());
            // await GameAudioPlayer.play("audio/game_sounds/button_tap.mp3");
          }
        }
        // widget.onPressed!(dragWord);
      },
      onPanEnd: (details) {
        setState(() {
          currentOffset = null;
          selectedCirclesIndexes = [];
          dragWord="";
          // Timer(const Duration(seconds: 1), () {
            widget.onEnd!(dragWord);
          // });
          setState(() {});
          // widget.onPressed!(dragWord);
          // if (dragWord.isNotEmpty &&
          //     !(dragWord.length == widget.wordList.length &&
          //         widget.wordList.join() == dragWord)) {
          //   Future.delayed(const Duration(milliseconds: 500), () {
          //     setState(() {
          //       dragWord = "";
          //       widget.onPressed!("");
          //     });
          //   });
          // }
        });
      },
      onPanStart: (details) {
      },
      child: Stack(
        children: [
          Container(
            decoration:  const BoxDecoration(
              color: Colors.white24,
              shape: BoxShape.circle,
            ),
            child: CustomPaint(
              painter: PatternPainter(
                wordsList: widget.wordList,
                currentOffset: currentOffset,
                selectedCirclesIndexes: selectedCirclesIndexes,
              ),
              size: widget.size,
            ),
          ),
            // AnimatedPositioned(
            //   top: top,
            //   left: left,
            //   duration: const Duration(seconds: 1),
            //   child: GestureDetector(
            //     onTap: () {
            //
            //     },
            //     child: Image.asset("assets/images/animations/finger-tap.gif"),
            //   ),
            // )
        ],
      ),
    );
  }



  double? top = 100;
  double? left = 200;
}
