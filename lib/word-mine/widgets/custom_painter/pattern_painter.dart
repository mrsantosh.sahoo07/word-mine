import 'dart:math';

import 'package:flutter/material.dart';

class PatternPainter extends CustomPainter {
  PatternPainter({
    this.currentOffset,
    required this.selectedCirclesIndexes,
    required this.wordsList,
  });

  final Offset? currentOffset;
  final List<int> selectedCirclesIndexes;
  final List<String> wordsList;

  @override
  void paint(Canvas canvas, Size size) {
    final circlePaint = Paint()..style = PaintingStyle.fill;

    final activeCirclePaint = Paint()
      ..color = const Color(0xFF149623)
      ..strokeWidth = 3.0
      ..style = PaintingStyle.fill;

    for (int i = 0; i < wordsList.length; i++) {
      final position = getCurrentCirclePosition(size, wordsList.length, i);
      canvas.drawCircle(
        position,
        35,
        circlePaint
          ..color = selectedCirclesIndexes.contains(i)
              ? const Color(0xFF149623)
              : Colors.transparent,
      );
      if (selectedCirclesIndexes.contains(i)) {
        canvas.drawCircle(
          position,
          35,
          activeCirclePaint,
        );
      }
    }

    if (selectedCirclesIndexes.isNotEmpty) {
      for (var i = 0; i < selectedCirclesIndexes.length - 1; i++) {
        final p1 = getCurrentCirclePosition(
            size, wordsList.length, selectedCirclesIndexes[i]);
        final p2 = getCurrentCirclePosition(
            size, wordsList.length, selectedCirclesIndexes[i + 1]);
        canvas.drawLine(
          p1,
          p2,
          Paint()
            ..color = const Color(0xFF149623)
            ..strokeWidth = 15.0
            ..style = PaintingStyle.fill
            ..strokeCap = StrokeCap.round,
        );
      }
    }

    if (currentOffset != null && selectedCirclesIndexes.isNotEmpty) {
      final p1 = getCurrentCirclePosition(
          size, wordsList.length, selectedCirclesIndexes.last);
      canvas.drawLine(
        Offset(p1.dx, p1.dy),
        currentOffset!,
        Paint()
          ..color = const Color(0xFF149623)
          ..strokeWidth = 15.0
          ..strokeCap = StrokeCap.round,
      );
    }
    for (int i = 0; i < wordsList.length; i++) {
      final position = getCurrentCirclePosition(size, wordsList.length, i);
      // Draw text in the center of the circle
      final textPainter = TextPainter(
        text: TextSpan(
          text: wordsList[i].toUpperCase(),
          // Display circle index (or any text you want)
          style: TextStyle(
            color: selectedCirclesIndexes.contains(i)
                ? Colors.white
                : Colors.black,
            fontSize: 45,
            fontWeight: FontWeight.w900,
          ),
        ),
        textDirection: TextDirection.ltr,
      )..layout();
      textPainter.paint(
        canvas,
        Offset(
          position.dx - textPainter.width / 2,
          position.dy - textPainter.height / 2,
        ),
      );
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}

Offset getCurrentCirclePosition(Size size, int totalCircles, int i) {
  double radius = size.width/2.7;
  double centerX = size.width / 2;
  double centerY = size.height / 2;
  double angle = (2 * i * 3.14) / totalCircles;
  double x = centerX + (radius * cos(angle));
  double y = centerY + (radius * sin(angle));
  return Offset(x, y);
}
