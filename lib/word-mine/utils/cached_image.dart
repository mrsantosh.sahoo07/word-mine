import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CachedImage extends StatelessWidget {
  final String? imageUrl;
  final String? placeHolder;
  final BoxFit? fit;
  final double? height;
  final double? width;
  final Widget? placeholderWidget;
  final Duration fadeInDuration;
  final bool hasPlaceHolder;
  final Color? color;
  final Color? placeHolderColor;

  const CachedImage({
    super.key,
    required this.imageUrl,
    this.placeHolder,
    this.fit,
    this.placeholderWidget,
    this.height,
    this.width,
    this.fadeInDuration = Duration.zero,
    this.hasPlaceHolder = true,
    this.color,
    this.placeHolderColor,
  });

  @override
  Widget build(BuildContext context) {
    bool isNetworkImage = imageUrl!.startsWith('http') || imageUrl!.startsWith('https');

    if (isNetworkImage) {
      // If it's a network image, use the Image.network widget
      return imageUrl != null && imageUrl?.isNotEmpty == true
          ? CachedNetworkImage(
        imageUrl: imageUrl!,
        fit: fit,
        height: height,
        width: width,
        color: color,
        fadeInDuration: fadeInDuration,
        placeholder: (_, __) => !hasPlaceHolder
            ? const SizedBox.shrink()
            : placeholderWidget != null
            ? placeholderWidget!
            : _buildPlaceHolder(),
        errorWidget: (_, __, ___) => !hasPlaceHolder
            ? const SizedBox.shrink()
            : placeholderWidget != null
            ? placeholderWidget!
            : _buildPlaceHolder(),
      )
          : _buildPlaceHolder();
    } else {
      // If it's an asset image, use the Image.asset widget
      return Image.asset(
        imageUrl!,
        fit: fit,
        height: height,
        width: width,
        color: color,
      );
    }
  }

  Widget _buildPlaceHolder() => placeHolder != null
      ? Image.asset(
    placeHolder!,
    fit: fit,
    height: height,
    width: width,
  )
      : Container(
    color: placeHolderColor ?? Colors.grey.withOpacity(0.3),
    width: width,
    height: height,
  );
}
