import 'package:flutter/material.dart';

class DotKnotGame extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dot Knot Game',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Dot Knot Game'),
        ),
        body: DotGrid(),
      ),
    );
  }
}

class DotGrid extends StatefulWidget {
  @override
  _DotGridState createState() => _DotGridState();
}

class _DotGridState extends State<DotGrid> {
  List<Offset> lines = [];
  Offset? currentLineStart;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanStart: (details) {
        currentLineStart = details.localPosition;
      },
      onPanUpdate: (details) {
        setState(() {
          lines.add(currentLineStart??Offset(0, 0));
          lines.add(details.localPosition);
          currentLineStart = details.localPosition;
        });
      },
      onPanEnd: (details) {
        currentLineStart = null;
      },
      child: CustomPaint(
        painter: DotPainter(lines),
        child: Container(),
      ),
    );
  }
}

class DotPainter extends CustomPainter {
  final List<Offset> lines;

  DotPainter(this.lines);

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = Colors.blue
      ..strokeWidth = 5
      ..strokeCap = StrokeCap.round;

    for (int i = 0; i < lines.length; i += 2) {
      canvas.drawLine(lines[i], lines[i + 1], paint);
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}