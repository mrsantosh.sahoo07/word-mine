import 'package:flutter/material.dart';

class CircleModel {
  final int index;
  final List<int> drawLineIndex;
  final bool isRed;
  final bool isBlank;
  final Color color; // Added color property to CircleModel

  CircleModel({
    required this.index,
    this.isRed = false,
    this.isBlank = false,
    this.drawLineIndex = const [],
    required this.color,
  });

  CircleModel copyWith({
    int? index,
    List<int>? drawLineIndex,
    bool? isRed,
    bool? isBlank,
    Color? color,
  }) {
    return CircleModel(
      index: index ?? this.index,
      drawLineIndex: drawLineIndex ?? this.drawLineIndex,
      isRed: isRed ?? this.isRed,
      isBlank: isBlank ?? this.isBlank,
      color: color ?? this.color,
    );
  }
  @override
  String toString() {
    return 'CircleModel{index: $index, drawLineIndex: $drawLineIndex, isRed: $isRed, isBlank: $isBlank, color: $color}';
  }
}

class DrawLinesWidget extends StatefulWidget {
  @override
  _DrawLinesWidgetState createState() => _DrawLinesWidgetState();
}

class _DrawLinesWidgetState extends State<DrawLinesWidget> {
  int selectedCircleIndex = -1; // Initially no circle selected
  int startingIndex = -1; // Initially no circle selected
  Offset? currentOffset;
  List<int> connectedCircleIndices = []; // Store indices of connected circles

  List<CircleModel> circles = [
    CircleModel(index: 0, isBlank: true, color: Colors.green),
    CircleModel(index: 1, isBlank: true, color: Colors.red),
    CircleModel(index: 2, isBlank: true, color: Colors.blue),
    CircleModel(index: 3, isBlank: true, color: Colors.orange),
    CircleModel(index: 4, color: Colors.transparent),
    CircleModel(index: 5, color: Colors.transparent),
    CircleModel(index: 6, color: Colors.transparent),
    CircleModel(index: 7, color: Colors.transparent),
    CircleModel(index: 8, color: Colors.transparent),
    CircleModel(index: 9, color: Colors.transparent),
    CircleModel(index: 10, color: Colors.transparent),
    CircleModel(index: 11, color: Colors.transparent),
    CircleModel(index: 12, isBlank: true, color: Colors.green),
    CircleModel(index: 13, isBlank: true, color: Colors.red),
    CircleModel(index: 14, isBlank: true, color: Colors.blue),
    CircleModel(index: 15, isBlank: true, color: Colors.orange),
  ];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanStart: (details) {
        setState(() {
          selectedCircleIndex = getSelectedCircleIndex(details.localPosition);
          startingIndex = getSelectedCircleIndex(details.localPosition);
          if (!connectedCircleIndices
              .contains(getSelectedCircleIndex(details.localPosition)) &&
              circles[startingIndex].color != Colors.transparent) {
            connectedCircleIndices
                .add(getSelectedCircleIndex(details.localPosition));
          }
        });
      },
      onPanUpdate: (details) {
        setState(() {
          currentOffset = details.localPosition;
          int selectedCircleIndex =
          getSelectedCircleIndex(details.localPosition);
          if (!connectedCircleIndices.contains(selectedCircleIndex) &&
              circles[startingIndex].color != Colors.transparent) {
            connectedCircleIndices.add(selectedCircleIndex);
            circles[startingIndex] = circles[startingIndex].copyWith(drawLineIndex: connectedCircleIndices.toList());
          }
        });
      },
      onPanEnd: (details) {
        setState(() {
          connectedCircleIndices.clear();
          selectedCircleIndex = -1; // Reset selected circle
          currentOffset = null; // Clear current offset
        });
      },
      child: CustomPaint(
        painter: PatternPainter(
            circles: circles,
            selectedCirclesIndexes: connectedCircleIndices,
            currentOffset: currentOffset,
            selectedCircleIndex: selectedCircleIndex),
        size: Size(300, 300),
      ),
    );
  }

  int getSelectedCircleIndex(Offset position) {
    double gridWidth = 300 / 4; // Width of each grid cell
    double gridHeight = 300 / 4; // Height of each grid cell

    int column = (position.dx / gridWidth).floor(); // Calculate column index
    int row = (position.dy / gridHeight).floor(); // Calculate row index

    return row * 4 + column; // Calculate the index in a 4x4 grid
  }
}

class PatternPainter extends CustomPainter {
  final List<CircleModel> circles;
  final List<int> selectedCirclesIndexes;
  final int? selectedCircleIndex;
  final Offset? currentOffset;

  PatternPainter({
    required this.circles,
    required this.selectedCirclesIndexes,
    this.currentOffset,
    this.selectedCircleIndex,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final circlePaint = Paint()..style = PaintingStyle.fill;
    // Color lineColor=circles[selectedCircleIndex??0].color;
    final linePaint = Paint()
      ..color = const Color(0xFF149623)
      ..strokeWidth = 15.0
      ..style = PaintingStyle.fill
      ..strokeCap = StrokeCap.round;

    for (final circle in circles) {
      final position = getCurrentCirclePosition(size, circle.index);

      // Draw background box for each circle
      Paint boxPaint = Paint()
        ..style = PaintingStyle.stroke
        ..strokeWidth = 2.0
        ..color = Colors.grey;
      canvas.drawRect(
        Rect.fromCenter(
          center: position,
          width: 75,
          height: 75,
        ),
        boxPaint,
      );}

    for (final circle in circles) {
      final position = getCurrentCirclePosition(size, circle.index);
      // Use specific colors for circles
      Color circleColor = circle.color;
      canvas.drawCircle(
        position,
        25,
        circlePaint..color = circleColor,
      );
      // List<int> inList=[3,7,11,15];
      if (circle.drawLineIndex.isNotEmpty) {
        linePaint.color = circle.color;
        for (var i = 0; i < circle.drawLineIndex .length - 1; i++) {
          final p1 = getCurrentCirclePosition(size, circle.drawLineIndex[i]);
          final p2 =
          getCurrentCirclePosition(size, circle.drawLineIndex[i + 1]);
          canvas.drawLine(p1, p2, linePaint);
        }
      }
      // if (currentOffset != null && selectedCirclesIndexes.isNotEmpty) {
      //   final p1 = getCurrentCirclePosition(size, selectedCirclesIndexes.last);
      //   linePaint.color = circles[selectedCircleIndex ?? 0].color;
      //   canvas.drawLine(
      //     Offset(p1.dx, p1.dy),
      //     currentOffset!,
      //     linePaint,
      //   );
      // }
    }


    // if (selectedCirclesIndexes.isNotEmpty) {
    //   linePaint.color = circles[selectedCircleIndex ?? 0].color;
    //   for (var i = 0; i < selectedCirclesIndexes.length - 1; i++) {
    //     final p1 = getCurrentCirclePosition(size, selectedCirclesIndexes[i]);
    //     final p2 =
    //         getCurrentCirclePosition(size, selectedCirclesIndexes[i + 1]);
    //     canvas.drawLine(p1, p2, linePaint);
    //   }
    // }

    // // Draw current line if dragging
    // if (currentOffset != null && selectedCirclesIndexes.isNotEmpty) {
    //   final p1 = getCurrentCirclePosition(size, selectedCirclesIndexes.last);
    //   linePaint.color = circles[selectedCircleIndex ?? 0].color;
    //   canvas.drawLine(
    //     Offset(p1.dx, p1.dy),
    //     currentOffset!,
    //     linePaint,
    //   );
    // }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;

  Offset getCurrentCirclePosition(Size size, int index) {
    double gridWidth = size.width / 4; // Width of each grid cell
    double gridHeight = size.height / 4; // Height of each grid cell
    double centerX =
        (index % 4) * gridWidth + gridWidth / 2; // Calculate center X
    double centerY =
        (index ~/ 4) * gridHeight + gridHeight / 2; // Calculate center Y
    return Offset(centerX, centerY); // Return the center offset of the circle
  }
}