import 'dart:async';

import 'package:flutter/foundation.dart';

class WidgetsProvider with ChangeNotifier, DiagnosticableTreeMixin {
  WidgetsProvider() {}
  bool _isCoinAnimation = false;
  bool get isCoinAnimation => _isCoinAnimation;
  bool _isStarAnimation = false;
  bool get isStarAnimation => _isStarAnimation;

  setCoinAnimation(bool isCoin) {
    if(isCoin) {
      _isCoinAnimation = true;
      Timer(const Duration(seconds: 1), () {
        _isCoinAnimation = false;
        notifyListeners();
      });
    }else{
      _isStarAnimation = true;
      Timer(const Duration(seconds: 1), () {
        _isStarAnimation = false;
        notifyListeners();
      });
    }
    notifyListeners();
  }


  @override
  void dispose() {
    super.dispose();
  }
}
