import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:wordly/games/word_game/utils/ui_util.dart';
import '../../../animations/coin_animation.dart';
import '../audio/audio_controller.dart';
import '../utils/enums.dart';
import '../view/dashboard/dashboard_screen.dart';
import '../view/widgets/game_coin_widget.dart';

class DalyRewardScreen extends StatefulWidget {
  const DalyRewardScreen({super.key});

  @override
  State<DalyRewardScreen> createState() => _DalyLoginRewardScreenState();
}

class _DalyLoginRewardScreenState extends State<DalyRewardScreen>
    with TickerProviderStateMixin {
  bool showBox = false;
  bool aniShowBox = false;
  final GlobalKey _startKey = GlobalKey();
  Offset _startOffset = Offset.zero;
  final GlobalKey _coinKey = GlobalKey();
  Offset _coinOffset = Offset.zero;
  final GlobalKey _livedKey = GlobalKey();
  Offset _livedOffset = Offset.zero;
  final GlobalKey _helpKey = GlobalKey();
  Offset _helpOffset = Offset.zero;

  late AnimationController _controller;
  late AnimationController _textController;

  late Animation<double> _textAnimation;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((c) {
      _startOffset = (_startKey.currentContext?.findRenderObject() as RenderBox)
          .localToGlobal(Offset.zero);
    });
    WidgetsBinding.instance.addPostFrameCallback((c) {
      _coinOffset = (_coinKey.currentContext?.findRenderObject() as RenderBox)
          .localToGlobal(Offset.zero);
    });
    WidgetsBinding.instance.addPostFrameCallback((c) {
      _livedOffset = (_livedKey.currentContext?.findRenderObject() as RenderBox)
          .localToGlobal(Offset.zero);
    });
    WidgetsBinding.instance.addPostFrameCallback((c) {
      _helpOffset = (_helpKey.currentContext?.findRenderObject() as RenderBox)
          .localToGlobal(Offset.zero);
    });
    _controller = AnimationController(
      duration: const Duration(seconds: 3),
      vsync: this,
    );
    _textController = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    )..repeat(reverse: true);
    _textAnimation = Tween<double>(
      begin: -10.0,
      end: 10.0,
    ).animate(CurvedAnimation(
      parent: _textController,
      curve: Curves.linear,
    ));
  }


  @override
  void dispose() {
    _controller.dispose();
    _textController.dispose();
    super.dispose();
  }

  int animationCount = 0;

  addCoinAnimation(WidgetType widgetType, int count) async {
    setState(() {
      showBox = true;
    });
    final audioController = context.read<AudioController>();
    Future.delayed(Duration(milliseconds: animationCount == 0 ? 800 : 0))
        .then((onValue) async {
      List<OverlayEntry>? overlayEntry = List.generate(
        count,
        (index) => OverlayEntry(
          builder: (_) {
            Random random = Random();
            int randomNumber =
                random.nextInt(50) + (UiUtils.size.size.width / 3).toInt();
            int randomNumber2 =
                random.nextInt(101) + (UiUtils.size.size.height / 1.9).toInt();
            return AtoBAnimation(
              startPosition:
                  Offset(randomNumber.toDouble(), randomNumber2.toDouble()),
              endPosition: widgetType == WidgetType.coin
                  ? _coinOffset
                  : widgetType == WidgetType.live
                      ? _livedOffset
                      : widgetType == WidgetType.help
                          ? _helpOffset
                          : _coinOffset,
              dxCurveAnimation: 0,
              dyCurveAnimation: 0,
              duration: Duration(milliseconds: (index + 1) * 250),
              opacity: 1,
              child: Image.asset(
                widgetType == WidgetType.coin
                    ? "assets/images/coin3dd.png"
                    : widgetType == WidgetType.live
                        ? "assets/images/heart.png"
                        : widgetType == WidgetType.help
                            ? "assets/images/wHelper.png"
                            : "assets/images/coin3dd.png",
                height: 35,
                width: 35,
                fit: BoxFit.fill,
              ),
            );
          },
        ),
      );
      Overlay.of(context).insertAll(overlayEntry);
      for (int i = 0; i < overlayEntry.length; i++) {
        await Future.delayed(const Duration(milliseconds: 200));
        {
          var coinProvider = context.read<CoinProvider>();
          var liveProvider = context.read<LiveProvider>();
          var helpProvider = context.read<HelpProvider>();
          if (animationCount == 0) {
            liveProvider.startAnimation(context, widgetType: WidgetType.live);
          }  if (animationCount == 1) {
            coinProvider.startAnimation(context, widgetType: WidgetType.coin);
          }  if (animationCount == 2) {
            helpProvider.startAnimation(context, widgetType: WidgetType.help);
          }
        }
      }
      await Future.delayed(Duration(milliseconds: overlayEntry.length * 200),
          () {
        for (var overlay in overlayEntry ?? []) {
          overlay.remove();
        }
        overlayEntry = null;
      });
      {
        animationCount++;
        if (animationCount == 1) {
          addCoinAnimation(WidgetType.coin, 5);
        } else if (animationCount == 2) {
          addCoinAnimation(WidgetType.help, 2);
        }
        if(animationCount==3){
          await Future.delayed(const Duration(milliseconds: 200));
          context.pop();
        }
      }
    });

  }


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var viewPadding = MediaQuery.of(context).viewPadding;
    return Container(
      padding: Platform.isAndroid
          ? EdgeInsets.only(top: viewPadding.top != 0 ? viewPadding.top : 35)
          : null,
      color: const Color(0xff350e27),
      child: Scaffold(
        body: SizedBox(
          height: size.height,
          child: GestureDetector(
            onTap: () {
              addCoinAnimation(WidgetType.live, 2);
              _controller.reverse();
              _textController.reset();
            },
            child: SafeArea(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  const Align(
                    alignment: Alignment(0, -0.5),
                    child: Opacity(
                      opacity: 0.2,
                      child: SGText(
                        "Daly Rewards",
                        fontSize: 40,
                        shadowWith: 1.5,
                        letterSpacing: 0,
                        fontFamily: "Permanent Marker",
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CoinWidget(
                          widgetType: WidgetType.live,
                          key: _livedKey,
                          onTap: () async {},
                        ),
                        CoinWidget(
                          widgetType: WidgetType.coin,
                          key: _coinKey,
                          onTap: () async {},
                        ),
                        CoinWidget(
                          widgetType: WidgetType.help,
                          key: _helpKey,
                          onTap: () async {},
                        ),
                      ],
                    ),
                  ),
                  showBox
                      ? Image.asset(
                          "assets/images/re_2.gif",
                          height: size.height / 1.5,
                          width: size.width,
                        )
                      : Image.asset(
                          "assets/images/re_1.gif",
                          height: size.height / 1.5,
                          width: size.width,
                        ),
                  Padding(
                    padding: EdgeInsets.only(bottom: size.height * 0.15),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: AnimatedBuilder(
                        animation: _textController,
                        builder: (context, child) {
                          return Transform.translate(
                            offset: Offset(0, _textAnimation.value),
                            child: child,
                          );
                        },
                        child: SGText(
                          "Tap to open",
                          fontSize: 24,
                          shadowWith: 1.5,
                          letterSpacing: 0,
                          key: _startKey,
                          fontFamily: "ArchiveBlack",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
