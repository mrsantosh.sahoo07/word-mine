import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:super_tooltip/super_tooltip.dart';
import 'package:wordly/games/word_game/setting/setting_controller.dart';
import 'package:wordly/games/word_game/setting/setting_dialog.dart';

import '../ads/app_ads.dart';
import '../view/coin_buy/coin_buy_screen.dart';
import '../view/dashboard/dashboard_screen.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  final _controller2 = SuperTooltipController();


  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 2), // Adjust duration as needed
      vsync: this,
    )..repeat();

    playerNameGet();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();

  }

  late TextEditingController playerName;

 void playerNameGet() {
    var setting = context.read<SettingsController>();
    Random random = Random();
    int playerNumber=random.nextInt(10000);
    if(setting.playerName.value.isEmpty){
      setting.setPlayerName("${setting.playerName.value}$playerNumber");
    }
    playerName = TextEditingController(text: setting.playerName.value);
  }
  void setPlayerName(String name) {
    var setting = context.read<SettingsController>();
    setting.setPlayerName(name);
  }
  @override
  Widget build(BuildContext context) {

    var size = MediaQuery.of(context).size;
    var setting = context.watch<SettingsController>();
    var settings = context.read<SettingsController>();

    return Scaffold(
      body: Container(
        height: size.height,
        width: size.width,
        decoration: const BoxDecoration(
          gradient: RadialGradient(
            tileMode: TileMode.clamp,
            center: Alignment.center,
            radius: 0.8,
            colors: [
              Color(0xff175395),
              Color(0xff0d2655),
            ],
          ),
        ),
        child: Column(
          children: [
            Container(
              decoration: const BoxDecoration(
                gradient: RadialGradient(
                  tileMode: TileMode.clamp,
                  center: Alignment.bottomCenter,
                  radius: 1.5,
                  colors: [
                    Color(0xff015898),
                    Color(0xff013382),
                  ],
                ),
              ),
              height: size.height / 7.5,
              width: double.infinity,
              alignment: const Alignment(0.0, 0.8),
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0, bottom: 8),
                      child: GradientShadowButton(
                        strokeWidth: 0.4,
                        borderRadius: 8,
                        height: 40,
                        width: 40,
                        padding: const EdgeInsets.all(8),
                        gradientColors: GameColors.redGradient,
                        onTap: () {
                          context.pop();
                        },
                        child: Image.asset('assets/images/close_icon.png'),
                      ),
                    ),
                  ),
                  const SGText(
                    "Profile",
                    fontSize: 35,
                    shadowWith: 2,
                    letterSpacing: 1,
                  ),
                ],
              ),
            ),
            Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  tileMode: TileMode.clamp,
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xffb56540),
                    Color(0xff995337),
                    Color(0xff673926),
                  ],
                ),
              ),
              height: 10,
              width: double.infinity,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: AnimationLimiter(
                  child: ListView(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    padding: EdgeInsets.only(
                        top: 16,
                        bottom: 180,
                        left: size.width * 0.04,
                        right: size.width * 0.04),
                    children: [

                      GradientBorder2(
                        borderRadius: 40,
                        strokeWidth: 0.8,
                        child: Container(
                          height: 100,
                          child: Row(
                            children: [
                              Container(
                                height: 80,
                                width: 80,
                                margin: const EdgeInsets.only(left: 10),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    gradient:const LinearGradient(colors: GameColors.appBackgroundGradient) ,
                                    borderRadius: BorderRadius.circular(20),
                                    border: Border.all(
                                        color: Colors.green, width: 8)),
                                child:  SGText(
                                  playerName.text[0],
                                  fontSize: 30,
                                  shadowWith: 1,
                                  letterSpacing: 0,
                                  textGradiantColors: GameColors.whiteGradient,
                                  fontFamily: "ArchiveBlack",
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  margin: const EdgeInsets.only(left: 4,right: 20),
                                  decoration: BoxDecoration(
                                    color:   const Color(0xffffebbc),
                                    borderRadius: BorderRadius.circular(16),
                                  ),
                                  child: TextFormField(
                                    controller: playerName,
                                    textAlign: TextAlign.left,
                                    cursorColor: Colors.transparent,
                                    autofocus: false,
                                    style: const TextStyle(
                                        color:    Color(0xff013382),
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Roboto',
                                        fontSize: 28),
                                    onChanged: (v){
                                      setPlayerName(v);
                                    },
                                    decoration: const InputDecoration(
                                      // suffixIcon: Icon(Icons.edit),
                                      border: InputBorder.none,
                                      contentPadding: EdgeInsets.only(left: 10)
                                    ),

                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),

                    ],
                  ),
                ),
              ),
            ),
            // GestureDetector(
            //   onTap: () async {
            //     await _controller2.showTooltip();
            //   },
            //   child: SuperTooltip(
            //     showBarrier: false,
            //     showCloseButton: ShowCloseButton.inside,
            //     controller: _controller2,
            //     shadowBlurRadius: 0,
            //     content: const Text(
            //       "Lorem ipsum dolor sit amet, consetetur sadipscing elitr,",
            //       softWrap: true,
            //       style: TextStyle(
            //         color: Colors.black,
            //       ),
            //     ),
            //     child: Container(
            //       width: 40.0,
            //       height: 40.0,
            //       decoration: const BoxDecoration(
            //         shape: BoxShape.circle,
            //         color: Colors.blue,
            //       ),
            //       child: Icon(
            //         Icons.add,
            //         color: Colors.white,
            //       ),
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
      bottomNavigationBar: const ButtonNavigationAdsWidgets(),
    );
  }


}
