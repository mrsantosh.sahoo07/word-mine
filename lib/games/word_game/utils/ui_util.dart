
import 'package:flutter/material.dart';

class UiUtils{
  static var size= MediaQueryData.fromView(WidgetsBinding.instance.window);
  static void showSnackBar(String message) {
    final messenger = scaffoldMessengerKey.currentState;
    messenger?.showSnackBar(
      SnackBar(content: Text(message)),
    );
  }

  /// Use this when creating [MaterialApp] if you want [showSnackBar] to work.
static  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
  GlobalKey(debugLabel: 'scaffoldMessengerKey');
}