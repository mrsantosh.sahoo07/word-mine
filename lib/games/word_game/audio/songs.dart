const Set<Song> songs = {
  Song('bg_music_1.mp3', 'music_1', artist: 'Mr king'),
  Song('bg_music_2.mp3', 'music_2', artist: 'Mr king'),
};

class Song {
  final String filename;

  final String name;

  final String? artist;

  const Song(this.filename, this.name, {this.artist});

  @override
  String toString() => 'Song<$filename>';
}
