List<String> soundTypeToFilename(Sounds type) {
  switch (type) {
    case Sounds.buttonTap:
      return const [
        'tap_mini.mp3'
      ];
    case Sounds.dialog:
      return const [
        'popup.mp3',
      ];
    case Sounds.switched:
      return const [
        'switch.mp3',
      ];
    case Sounds.quit:
      return const [
        'quit.mp3',
      ];
    case Sounds.navigationPage:
      return const [
        'navigation_page.mp3',
      ];
    case Sounds.navigationPop:
      return const [
        'navigate_pop.mp3',
      ];
    case Sounds.closeButtonTap:
      return const [
        'close-button.mp3',
      ];
    case Sounds.coin:
      return const [
        'coin.mp3',
      ];
    case Sounds.win:
      return const [
        'win-shot.mp3',
      ];
    case Sounds.win1:
      return const [
        'win-1.mp3',
      ];
  }
}

/// Allows control over loudness of different SFX types.
double soundTypeToVolume(Sounds type) {
  switch (type) {
    case Sounds.buttonTap:
    case Sounds.dialog:
    case Sounds.switched:
    case Sounds.navigationPage:
    case Sounds.navigationPop:
    case Sounds.closeButtonTap:
    case Sounds.quit:
    case Sounds.coin:
    case Sounds.win:
    case Sounds.win1:
      return 1.0;
  }
}

enum Sounds {
  buttonTap,
  closeButtonTap,
  dialog,
  switched,
  navigationPage,
  navigationPop,
  quit,
  coin,
  win,
  win1,
}
