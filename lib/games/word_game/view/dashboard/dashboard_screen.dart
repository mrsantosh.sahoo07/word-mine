import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:neopop/neopop.dart';
import 'package:provider/provider.dart';
import 'package:wordly/games/word_game/setting/setting_controller.dart';
import '../../ads/app_ads.dart';
import '../../ads/banner_ad_widget.dart';
import '../../audio/audio_controller.dart';
import '../../audio/sounds.dart';
import '../../setting/setting_dialog.dart';
import '../../setting/setting_screen.dart';
import '../coin_buy/coin_buy_screen.dart';
import '../event/event_screen.dart';
import 'level_selection.dart';

class DashBoardScreen extends StatefulWidget {
  final bool isSuccess;

  const DashBoardScreen({super.key, this.isSuccess = false});

  @override
  State<DashBoardScreen> createState() => _LDashBoardScreenScreenState();
}

class _LDashBoardScreenScreenState extends State<DashBoardScreen> {
  int selectedIndex = 1;
  bool onChangeTab = false;
  List<Widget> screens = [];
  late PageController _pageController;

  @override
  void initState() {
    screens = [
      const EventScreen(),
      LevelSelectionScreen(isSuccess: widget.isSuccess),
      const CoinBuyScreen(level: "", fromDashBoard: true),
    ];
    _pageController =
        PageController(keepPage: true, initialPage: selectedIndex);
    getGameLevel();
    super.initState();
  }

  getGameLevel() {
    context.read<SettingsController>().gwtLevel();
  }

  List<String> tabList = [
    "event",
    "map",
    "shop",
  ];

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var viewPadding = MediaQuery.of(context).viewPadding;
    final audioController = context.read<AudioController>();
    return PopScope(
      canPop: false,
      child: Scaffold(
        extendBody: true,
        backgroundColor: Colors.transparent,
        bottomNavigationBar: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ColoredBox(
              color: Colors.transparent,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ...tabList.asMap().entries.map((e) => GestureDetector(
                        onTap: () async {
                          audioController.playSfx(Sounds.buttonTap);
                          setState(() {
                            selectedIndex = e.key;
                            onChangeTab = true;
                          });
                          await _pageController.animateToPage(
                            e.key,
                            duration: const Duration(milliseconds: 400),
                            curve: Curves.easeInOut,
                          );
                          setState(() => onChangeTab = false);
                        },
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Container(
                              width: size.width / tabList.length,
                              color: const Color(0xff73402b),
                              margin: const EdgeInsets.only(top: 15.0),
                              child: Stack(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                      color: e.key == selectedIndex
                                          ? const Color(0xff0e479d)
                                          : const Color(0xff5e3625),
                                      border: e.key == selectedIndex
                                          ? const Border.symmetric(
                                              vertical: BorderSide(
                                                  color: Color(0xffc2690c),
                                                  width: 6))
                                          : null,
                                    ),
                                    child: AnimatedPadding(
                                      padding: e.key == selectedIndex
                                          ? const EdgeInsets.only(bottom: 15)
                                          : const EdgeInsets.only(bottom: 6),
                                      duration:
                                          const Duration(milliseconds: 200),
                                      child: Container(
                                        padding: const EdgeInsets.only(
                                            left: 16, right: 16, bottom: 12),
                                        margin:
                                            const EdgeInsets.only(bottom: 2),
                                        child: SizedBox(
                                          height: 55,
                                          width: size.width / tabList.length,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(
                                        left: 16, right: 16, bottom: 12),
                                    decoration: BoxDecoration(
                                      color: e.key == selectedIndex
                                          ? const Color(0xff176ac6)
                                          : const Color(0xff73402b),
                                      borderRadius: BorderRadius.circular(4),
                                      border: e.key == selectedIndex
                                          ? const Border.symmetric(
                                              vertical: BorderSide(
                                                  color: Color(0xfff7d415),
                                                  width: 6))
                                          : Border.all(
                                              color: const Color(0xff8b5134),
                                              width: 2),
                                    ),
                                    child: SizedBox(
                                      // 'assets/images/${e.value}.png',
                                      height: 55,
                                      width: size.width / tabList.length,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            AnimatedPositioned(
                              duration: const Duration(milliseconds: 200),
                              top: e.key == selectedIndex ? 0 : null,
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/images/${e.value}.png',
                                    height: 50,
                                    width: size.width / tabList.length,
                                  ),
                                  if (e.key == selectedIndex) ...[
                                    const SizedBox(height: 4),
                                    SGText(
                                      e.value.toUpperCase().trim(),
                                      fontSize: 14,
                                      shadowWith: 0.8,
                                      letterSpacing: 1,
                                    )
                                  ]
                                ],
                              ),
                            ),
                          ],
                        ),
                      ))
                ],
              ),
            ),
            const ButtonNavigationAdsWidgets()
          ],
        ),
        body:
            // IndexedStack(index: selectedIndex, children: screens)
            PageView.builder(
          controller: _pageController,
          itemCount: screens.length,
          padEnds: false,
          onPageChanged: (index) async {
            _onTapBottomNavigationBar(index);
          },
          itemBuilder: (BuildContext context, int index) {
            return screens[index];
          },
        ),
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  void _onTapBottomNavigationBar(int index) {
    if (!onChangeTab) {
      setState(() {
        selectedIndex = index;
      });
    }
  }
}

Color gColor(String code) {
  int codes = int.parse("0xff$code");
  return Color(codes);
}

class ButtonNavigationAdsWidgets extends StatefulWidget {
  const ButtonNavigationAdsWidgets({super.key});

  @override
  State<ButtonNavigationAdsWidgets> createState() =>
      _ButtonNavigationAdsWidgetsState();
}

class _ButtonNavigationAdsWidgetsState
    extends State<ButtonNavigationAdsWidgets> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height / 13,
      width: double.infinity,
      decoration: const BoxDecoration(
        color: Color(0xff673323),
        border: Border(top: BorderSide(color: Color(0xff7b401e), width: 5)),
      ),
      // child: AppAds(adsType: AdsType.banner,),
    );
  }
}

class GradientBorder extends StatelessWidget {
  final double strokeWidth;
  final double borderRadius;
  final Widget child;

  const GradientBorder({
    super.key,
    this.strokeWidth = 1.0,
    this.borderRadius = 8.0,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return GradientPixelBorder(
      strokeWidth: strokeWidth,
      borderRadius: borderRadius,
      colors: [gColor("b6653c"), gColor("5f250e")],
      child: GradientPixelBorder(
        strokeWidth: strokeWidth,
        borderRadius: borderRadius,
        colors: [gColor('dc944d'), gColor("702f14")],
        child: GradientPixelBorder(
          strokeWidth: strokeWidth,
          borderRadius: borderRadius,
          colors: [gColor("c86e38"), gColor("843418")],
          child: GradientPixelBorder(
            strokeWidth: strokeWidth,
            borderRadius: borderRadius,
            colors: [gColor("c86d39"), gColor("912e19")],
            child: GradientPixelBorder(
              strokeWidth: strokeWidth,
              borderRadius: borderRadius,
              colors: [gColor("c05f31"), gColor("95371b")],
              child: GradientPixelBorder(
                strokeWidth: strokeWidth,
                borderRadius: borderRadius,
                colors: [gColor('c05f31'), gColor("a34724")],
                child: GradientPixelBorder(
                  strokeWidth: strokeWidth,
                  borderRadius: borderRadius,
                  colors: [gColor('c05f31'), gColor("a64f26")],
                  child: GradientPixelBorder(
                    strokeWidth: strokeWidth,
                    borderRadius: borderRadius,
                    colors: [gColor('993923'), gColor("aa5926")],
                    child: child,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class GradientBorder2 extends StatelessWidget {
  final double strokeWidth;
  final double borderRadius;
  final Widget child;

  const GradientBorder2({
    super.key,
    this.strokeWidth = 1.0,
    this.borderRadius = 12.0,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return GradientPixelBorder(
      borderRadius: borderRadius,
      strokeWidth: strokeWidth,
      colors: [gColor("c04a03"), gColor("c04a03")],
      child: GradientPixelBorder(
        borderRadius: borderRadius,
        strokeWidth: strokeWidth,
        colors: [gColor("faab00"), gColor("cd5a01")],
        child: GradientPixelBorder(
          borderRadius: borderRadius,
          strokeWidth: strokeWidth,
          colors: [gColor("febd01"), gColor("d36101")],
          child: GradientPixelBorder(
            borderRadius: borderRadius,
            strokeWidth: strokeWidth,
            colors: [gColor("febd01"), gColor("fb9200")],
            child: GradientPixelBorder(
              borderRadius: borderRadius,
              strokeWidth: strokeWidth,
              colors: [gColor("feb600"), gColor("fe9801")],
              child: GradientPixelBorder(
                borderRadius: borderRadius,
                strokeWidth: strokeWidth,
                colors: [gColor("e3a200"), gColor("fbab00")],
                child: GradientPixelBorder(
                  borderRadius: borderRadius,
                  strokeWidth: strokeWidth,
                  colors: [gColor("bb8906"), gColor("c77600")],
                  child: child,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class GradientPixelBorder extends StatelessWidget {
  final double strokeWidth;
  final List<Color> colors;
  final double borderRadius;
  final Widget child;

  const GradientPixelBorder({
    super.key,
    this.strokeWidth = 2.0,
    required this.colors,
    this.borderRadius = 0.0,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: _GradientBorderPainter(
        strokeWidth: strokeWidth,
        colors: colors,
        borderRadius: borderRadius,
      ),
      child: Padding(
        padding: EdgeInsets.all(strokeWidth),
        child: child,
      ),
    );
  }
}

class _GradientBorderPainter extends CustomPainter {
  final double strokeWidth;
  final List<Color> colors;
  final double borderRadius;

  _GradientBorderPainter({
    required this.strokeWidth,
    required this.colors,
    this.borderRadius = 5.0,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final Rect outerRect = Offset.zero & size;
    final Rect innerRect = outerRect.deflate(strokeWidth / 3.0);
    Gradient gradient = LinearGradient(
      tileMode: TileMode.clamp,
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: colors,
    );
    // Gradient gradient = const RadialGradient(
    //   tileMode: TileMode.clamp,
    //   center: Alignment.center,
    //   radius: 2,
    //   colors: [
    //     Color(0xffb56540),
    //     Color(0xff995337),
    //     Color(0xff673926),
    //   ],
    // );

    final Paint paint = Paint()
      ..strokeWidth = strokeWidth
      ..shader = gradient.createShader(outerRect)
      ..style = PaintingStyle.stroke;

    final Path path = Path()
      ..addRRect(
          RRect.fromRectAndRadius(innerRect, Radius.circular(borderRadius)))
      ..addRRect(
          RRect.fromRectAndRadius(outerRect, Radius.circular(borderRadius)))
      ..fillType = PathFillType.evenOdd;
    // path.moveTo(size.width * 0.9426014, size.height * 0.04340955);
    // path.cubicTo(
    //     size.width * 0.7005524,
    //     size.height * -0.007731376,
    //     size.width * 0.2149678,
    //     size.height * -0.01920256,
    //     size.width * 0.05054266,
    //     size.height * 0.03908989);
    // path.cubicTo(
    //     size.width * 0.03059105,
    //     size.height * 0.04616320,
    //     size.width * 0.01591622,
    //     size.height * 0.07827528,
    //     size.width * 0.01261404,
    //     size.height * 0.1184222);
    // path.cubicTo(
    //     size.width * -0.008969860,
    //     size.height * 0.3808315,
    //     size.width * 0.001256766,
    //     size.height * 0.6905140,
    //     size.width * 0.01291757,
    //     size.height * 0.8717725);
    // path.cubicTo(
    //     size.width * 0.01597007,
    //     size.height * 0.9192191,
    //     size.width * 0.03431189,
    //     size.height * 0.9561854,
    //     size.width * 0.05792462,
    //     size.height * 0.9624944);
    // path.cubicTo(
    //     size.width * 0.3121734,
    //     size.height * 1.030424,
    //     size.width * 0.7266168,
    //     size.height * 0.9918961,
    //     size.width * 0.9442084,
    //     size.height * 0.9576376);
    // path.cubicTo(
    //     size.width * 0.9689510,
    //     size.height * 0.9537416,
    //     size.width * 0.9894168,
    //     size.height * 0.9172416,
    //     size.width * 0.9924601,
    //     size.height * 0.8677697);
    // path.cubicTo(
    //     size.width * 1.006147,
    //     size.height * 0.6452725,
    //     size.width * 0.9986182,
    //     size.height * 0.3347500,
    //     size.width * 0.9901678,
    //     size.height * 0.1410601);
    // path.cubicTo(
    //     size.width * 0.9879245,
    //     size.height * 0.08963624,
    //     size.width * 0.9681622,
    //     size.height * 0.04881039,
    //     size.width * 0.9426014,
    //     size.height * 0.04340955);

    // path.close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}

class DottedBorder extends BoxBorder {
  final Color color;
  final double strokeWidth;
  final double gapSpace;
  final double dashLength;

  const DottedBorder({
    this.color = Colors.white,
    this.strokeWidth = 5.0,
    this.gapSpace = 10.0,
    this.dashLength = 10.0,
  });

  @override
  EdgeInsetsGeometry get dimensions => EdgeInsets.all(strokeWidth);

  BoxBorder copyWith(
      {Color? color, double? strokeWidth, PaintingStyle? style}) {
    return DottedBorder(
      color: color ?? this.color,
      strokeWidth: strokeWidth ?? this.strokeWidth,
      gapSpace: gapSpace,
      dashLength: dashLength,
    );
  }

  @override
  void paint(
    Canvas canvas,
    Rect rect, {
    TextDirection? textDirection,
    BorderRadiusGeometry? borderRadius,
    BoxShape shape = BoxShape.rectangle,
  }) {
    final Paint paint = Paint()
      ..color = color
      ..strokeWidth = strokeWidth
      ..style = PaintingStyle.stroke;

    double startX = rect.left;
    while (startX < rect.right) {
      canvas.drawLine(
        Offset(startX, rect.top),
        Offset(startX + dashLength, rect.top),
        paint,
      );
      startX += dashLength + gapSpace;
    }
  }

  @override
  ShapeBorder scale(double t) {
    return DottedBorder(
      color: color,
      strokeWidth: strokeWidth * t,
      gapSpace: gapSpace * t,
      dashLength: dashLength * t,
    );
  }

  @override
  // TODO: implement bottom
  BorderSide get bottom => throw UnimplementedError();

  @override
  // TODO: implement isUniform
  bool get isUniform => throw UnimplementedError();

  @override
  // TODO: implement top
  BorderSide get top => throw UnimplementedError();
}

class SGText extends StatelessWidget {
  final String test;
  final String fontFamily;
  final double? fontSize;
  final double shadowWith;
  final double letterSpacing;
  final TextStyle? textStyle;
  final List<Color> textGradiantColors;
  final Color shadowColors;

  const SGText(
    this.test, {
    this.textGradiantColors = GameColors.textYellowGradient,
    this.shadowColors = GameColors.brownBold,
    super.key,
    this.textStyle,
    this.fontFamily = "ArchiveBlack",
    this.fontSize,
    this.letterSpacing = 4,
    this.shadowWith = 3,
  });

  @override
  Widget build(BuildContext context) {
    return GradientText(
      test,
      gradientType: GradientType.linear,
      gradientDirection: GradientDirection.ttb,
      colors: textGradiantColors,
      style: TextStyle(
        fontSize: fontSize ?? 40,
        fontWeight: FontWeight.bold,
        fontFamily: fontFamily,
        letterSpacing: letterSpacing,
        shadows: [
          Shadow(
            color: shadowColors,
            offset: Offset(-shadowWith, -shadowWith),
            blurRadius: 0,
          ),
          Shadow(
            color: shadowColors,
            offset: Offset(shadowWith, shadowWith),
            blurRadius: 0,
          ),
          Shadow(
            color: shadowColors,
            offset: Offset(-shadowWith, shadowWith),
            blurRadius: 0,
          ),
          Shadow(
            color: shadowColors,
            offset: Offset(shadowWith, -shadowWith),
            blurRadius: 0,
          ),
        ],
      ),
    );
  }
}

enum ShimmerDirection { ltr, rtl, ttb, btt }

@immutable
class Shimmer extends StatefulWidget {
  final Widget child;
  final Duration period;
  final ShimmerDirection direction;
  final Gradient gradient;
  final int loop;
  final bool enabled;

  const Shimmer({
    super.key,
    required this.child,
    required this.gradient,
    this.direction = ShimmerDirection.ltr,
    this.period = const Duration(milliseconds: 1500),
    this.loop = 0,
    this.enabled = true,
  });

  ///
  /// A convenient constructor provides an easy and convenient way to create a
  /// [Shimmer] which [gradient] is [LinearGradient] made up of `baseColor` and
  /// `highlightColor`.
  ///
  Shimmer.fromColors({
    super.key,
    required this.child,
    required Color baseColor,
    required Color highlightColor,
    this.period = const Duration(milliseconds: 1500),
    this.direction = ShimmerDirection.ltr,
    this.loop = 0,
    this.enabled = true,
  }) : gradient = LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.centerRight,
          colors: <Color>[
            baseColor,
            baseColor,
            highlightColor,
            baseColor,
            baseColor
          ],
          // stops: const <double>[0.0, 0.35, 0.5, 0.65, 1.0],
        );

  @override
  _ShimmerState createState() => _ShimmerState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<Gradient>('gradient', gradient,
        defaultValue: null));
    properties.add(EnumProperty<ShimmerDirection>('direction', direction));
    properties.add(
        DiagnosticsProperty<Duration>('period', period, defaultValue: null));
    properties
        .add(DiagnosticsProperty<bool>('enabled', enabled, defaultValue: null));
    properties.add(DiagnosticsProperty<int>('loop', loop, defaultValue: 0));
  }
}

class _ShimmerState extends State<Shimmer> with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  int _count = 0;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this, duration: widget.period)
      ..addStatusListener((AnimationStatus status) {
        if (status != AnimationStatus.completed) {
          return;
        }
        _count++;
        if (widget.loop <= 0) {
          _controller.repeat();
        } else if (_count < widget.loop) {
          _controller.forward(from: 0.0);
        }
      });
    if (widget.enabled) {
      _controller.forward();
    }
  }

  @override
  void didUpdateWidget(Shimmer oldWidget) {
    if (widget.enabled) {
      _controller.forward();
    } else {
      _controller.stop();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      child: widget.child,
      builder: (BuildContext context, Widget? child) => _Shimmer(
        child: child,
        direction: widget.direction,
        gradient: widget.gradient,
        percent: _controller.value,
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

@immutable
class _Shimmer extends SingleChildRenderObjectWidget {
  final double percent;
  final ShimmerDirection direction;
  final Gradient gradient;

  const _Shimmer({
    Widget? child,
    required this.percent,
    required this.direction,
    required this.gradient,
  }) : super(child: child);

  @override
  _ShimmerFilter createRenderObject(BuildContext context) {
    return _ShimmerFilter(percent, direction, gradient);
  }

  @override
  void updateRenderObject(BuildContext context, _ShimmerFilter shimmer) {
    shimmer.percent = percent;
    shimmer.gradient = gradient;
    shimmer.direction = direction;
  }
}

class _ShimmerFilter extends RenderProxyBox {
  ShimmerDirection _direction;
  Gradient _gradient;
  double _percent;

  _ShimmerFilter(this._percent, this._direction, this._gradient);

  @override
  ShaderMaskLayer? get layer => super.layer as ShaderMaskLayer?;

  @override
  bool get alwaysNeedsCompositing => child != null;

  set percent(double newValue) {
    if (newValue == _percent) {
      return;
    }
    _percent = newValue;
    markNeedsPaint();
  }

  set gradient(Gradient newValue) {
    if (newValue == _gradient) {
      return;
    }
    _gradient = newValue;
    markNeedsPaint();
  }

  set direction(ShimmerDirection newDirection) {
    if (newDirection == _direction) {
      return;
    }
    _direction = newDirection;
    markNeedsLayout();
  }

  @override
  void paint(PaintingContext context, Offset offset) {
    if (child != null) {
      assert(needsCompositing);

      final double width = child!.size.width;
      final double height = child!.size.height;
      Rect rect;
      double dx, dy;
      if (_direction == ShimmerDirection.rtl) {
        dx = _offset(width, -width, _percent);
        dy = 0.0;
        rect = Rect.fromLTWH(dx - width, dy, 3 * width, height);
      } else if (_direction == ShimmerDirection.ttb) {
        dx = 0.0;
        dy = _offset(-height, height, _percent);
        rect = Rect.fromLTWH(dx, dy - height, width, 3 * height);
      } else if (_direction == ShimmerDirection.btt) {
        dx = 0.0;
        dy = _offset(height, -height, _percent);
        rect = Rect.fromLTWH(dx, dy - height, width, 3 * height);
      } else {
        dx = _offset(-width, width, _percent);
        dy = 0.0;
        rect = Rect.fromLTWH(dx - width, dy, 3 * width, height);
      }
      layer ??= ShaderMaskLayer();
      layer!
        ..shader = _gradient.createShader(rect)
        ..maskRect = offset & size
        ..blendMode = BlendMode.srcIn;
      context.pushLayer(layer!, super.paint, offset);
    } else {
      layer = null;
    }
  }

  double _offset(double start, double end, double percent) {
    return start + (end - start) * percent;
  }
}
