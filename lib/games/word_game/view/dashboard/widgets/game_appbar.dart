import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import '../../../../../animations/scal_animation_button.dart';
import '../../../audio/audio_controller.dart';
import '../../../audio/sounds.dart';
import '../../../setting/setting_controller.dart';
import '../../../setting/setting_dialog.dart';
import '../../../utils/enums.dart';
import '../../coin_buy/coin_buy_screen.dart';
import '../../widgets/game_coin_widget.dart';

gameAppBar(BuildContext context,
    {Function()? coinTap,
    GlobalKey? coinKey,
    GlobalKey? starKey,
    bool fromCoinScreen = false}) {
  final audioController = context.read<AudioController>();
  var size = MediaQuery.of(context).size;
  return AppBar(
    automaticallyImplyLeading: false,
    backgroundColor: const Color(0xff002a6a),
    titleSpacing: 16,
    shape: const ContinuousRectangleBorder(
      borderRadius: BorderRadius.only(
        bottomRight: Radius.circular(30.0),
        bottomLeft: Radius.circular(30.0),
      ),
    ),
    actions: [
      Padding(
        padding: const EdgeInsets.only(right: 12.0),
        child: GradientShadowButton(
          strokeWidth: 0.2,
          height: 40,
          width: 40,
          padding: const EdgeInsets.all(5),
          gradientColors: GameColors.appBackgroundGradient,
          onTap: () {
            context.push('/settingScreen');
          },
          child: const Icon(Icons.settings, color: Colors.white),
        ),
      ),
    ],
    centerTitle: false,
    title: CoinWidget(
      widgetType: WidgetType.coin,
      onTap: () async {
        audioController.playSfx(Sounds.buttonTap);
        if (coinTap != null) {
          coinTap();
        }
      },
    ),
  );
}
/*
     //   CoinStarButton(
      // globalKey: coinKey,
      // count: coin,
      // imgPath: "assets/images/coin.png",
      // onTap: () {
      //   audioController.playSfx(Sounds.buttonTap);
      //   if (coinTap != null) {
      //     coinTap();
      //   }
      // },
      // ),
    fromCoinScreen
            ? GestureDetector(
                onTap: () {
                  audioController.playSfx(Sounds.closeButtonTap);
                  context.pop();
                },
                child: const Icon(
                  Icons.close,
                  color: Colors.white,
                  size: 40,
                ))
            : ValueListenableBuilder<int>(
                valueListenable: context.watch<SettingsController>().star,
                builder: (context, star, child) => CoinStarButton(
                  globalKey: starKey,
                  count: star,
                  isCoin: false,
                  imgPath: "assets/images/star_win.png",
                  onTap: () {
                    context.pop();
                  },
                ),
              ),*/
