import 'dart:io';
import 'dart:math' as math;
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:gif_view/gif_view.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:path_drawing/path_drawing.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wordly/games/word_game/audio/sounds.dart';
import 'package:wordly/games/word_game/utils/ui_util.dart';
import '../../../../animations/coin_animation.dart';
import '../../../../animations/scal_animation_button.dart';
import '../../../../word-mine/view/word_game.dart';
import '../../audio/audio_controller.dart';
import '../../onboard/daly_login_reward_screen.dart';
import '../../setting/setting_controller.dart';
import '../../setting/setting_dialog.dart';
import '../../utils/enums.dart';
import '../coin_buy/coin_buy_screen.dart';
import '../widgets/game_coin_widget.dart';

class LevelSelectionScreen extends StatefulWidget {
  final bool isSuccess;

  const LevelSelectionScreen({super.key, this.isSuccess = false});

  @override
  State<LevelSelectionScreen> createState() => _LevelSelectionScreenState();
}

class _LevelSelectionScreenState extends State<LevelSelectionScreen>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  late ScrollController scrollController;
  late int completeLevel = 0;
  int selectedIndex = 1;
  Offset _startOffset = Offset.zero;
  final GlobalKey _startKey = GlobalKey();
  Offset _endOffset = Offset.zero;
  final GlobalKey _endKey = GlobalKey();
  late AnimationController _controller;

  @override
  void dispose() {
    _controller.dispose();
    scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    final settings = context.read<SettingsController>();
    settings.gwtLevel();
    completeLevel = settings.level.value;
    debugPrint(completeLevel.toString());
    scrollController = ScrollController();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 500), // Adjust duration as needed
      vsync: this,
    )..repeat();
    WidgetsBinding.instance.addPostFrameCallback((c) {
      _startOffset = (_startKey.currentContext?.findRenderObject() as RenderBox)
          .localToGlobal(Offset.zero);
    });
    WidgetsBinding.instance.addPostFrameCallback((c) {
      _endOffset = (_endKey.currentContext?.findRenderObject() as RenderBox)
          .localToGlobal(Offset.zero);
    });
    jump();
    addCoin();
    _checkLastShownDate();
    super.initState();
  }

  jump() async {
    await Future.delayed(const Duration(milliseconds: 1000));
    if (scrollController.hasClients && completeLevel > 2) {
      scrollController.animateTo(
        (((UiUtils.size.size.height * 0.5) / 3) * (completeLevel - 1) - 100),
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 1500),
      );
    }
  }

  addCoin() {
    if (widget.isSuccess) {
      Future.delayed(const Duration(milliseconds: 1000)).then((l) {
        addCoinAnimation();
      });
    }
  }

  Future<void> _checkLastShownDate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? lastShownDate = prefs.getString('lastShownDate');
    String today = DateFormat('yyyy-MM-dd').format(DateTime.now());
    if (lastShownDate != today) {
      await Future.delayed(const Duration(seconds: 2));
      {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => const DalyRewardScreen()));
        prefs.setString('lastShownDate', today);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var size = MediaQuery.of(context).size;
    return PopScope(
      canPop: false,
      child: Scaffold(
        extendBody: true,
        resizeToAvoidBottomInset: true,
        extendBodyBehindAppBar: true,
        body: Container(
          decoration: const BoxDecoration(
            gradient: RadialGradient(
              tileMode: TileMode.clamp,
              center: Alignment.center,
              radius: 0.8,
              colors: [
                Color(0xff0275bc),
                Color(0xff002a6a),
              ],
            ),
          ),
          child: Stack(
            children: [
              ValueListenableBuilder<List<WordLevelBox>>(
                valueListenable: context.watch<SettingsController>().wordLevelBoxList,
                builder: (context, list, child) =>   ListView.builder(
                  padding: const EdgeInsets.only(bottom: 130),
                  reverse: true,
                  controller: scrollController,
                  physics: const ClampingScrollPhysics(),
                  itemCount: list.length~/3,
                  itemBuilder: (BuildContext context, int index) {
                    return Stack(
                      children: [
                        /// images bg

                        Image.asset(
                          "assets/images/mpp_bg_3.png",
                          width: double.infinity,
                          fit: BoxFit.fitWidth,
                          height: size.height * 0.5,
                        ),

                        /// paint added
                        CustomPaint(
                          painter: FaceOutlinePainter(),
                          size: Size(size.width, size.height * 0.5),
                        ),
                        ...List.generate(
                          3,
                              (i) {
                            int currentIndex = index * 3 + i + 1;
                            return Positioned(
                              top: i % 3 == 2
                                  ? 0
                                  : i % 3 == 1
                                  ? (size.height * 0.18)
                                  : (size.height * 0.35),
                              left: i % 3 == 2
                                  ? (size.width / 1.7)
                                  : i % 3 == 1
                                  ? (size.width / 2)
                                  : (size.width / 5.5),
                              child: Column(
                                children: [
                                  // (!((completeLevel + 1) == currentIndex) &&
                                  //         ((completeLevel + 1) >= currentIndex))
                                  //     ? Stack(
                                  //         children: [
                                  //           for (int j = 0; j < 3; j++)
                                  //             Padding(
                                  //               padding: EdgeInsets.only(
                                  //                   top: j == 1 ? 0 : 10,
                                  //                   left: j == 0
                                  //                       ? 0
                                  //                       : (25 * j).toDouble()),
                                  //               child: Container(
                                  //                 decoration: const BoxDecoration(
                                  //                     color: Colors.transparent,
                                  //                     shape: BoxShape.circle),
                                  //                 child: Image.asset(
                                  //                   j % 3 == 2
                                  //                       ? "assets/images/blank_star.png"
                                  //                       : "assets/images/star_win.png",
                                  //                   height: 28,
                                  //                   width: 28,
                                  //                 ),
                                  //               ),
                                  //             ),
                                  //         ],
                                  //       )
                                  //     : const SizedBox(height: 30, width: 30),
                                  ValueListenableBuilder<int>(
                                    valueListenable:
                                    context.read<SettingsController>().level,
                                    builder: (context, value, child) =>
                                        GradientShadowButton(
                                          text: "$currentIndex",
                                          fontSize: 30,
                                          strokeWidth:
                                          ((value + 1) >= currentIndex) ? 0.6 : 0,
                                          shadowWith: 2,
                                          width: 80,
                                          height: 60,
                                          padding: EdgeInsets.zero,
                                          textGradiantColors:
                                          GameColors.textGreenGradient,
                                          shadowColors: GameColors.shadowGreen,
                                          showShimmer: ((value + 1) == currentIndex),
                                          gradientColors:
                                          ((value + 1) == currentIndex)
                                              ? GameColors.greenGradient
                                              : ((value + 1) >= currentIndex)
                                              ? GameColors.orangeGradient
                                              : GameColors.greyGradient,
                                          onTap: () {
                                            if (((value + 1) >= currentIndex)) {
                                              showAnimateDialog(
                                                context,
                                                PlayDialog(
                                                  level: "$currentIndex",
                                                  onTap: () {
                                                    final settings = context
                                                        .read<SettingsController>();
                                                    WordLevelBox wordLevelBox =
                                                    settings.wordLevelBoxList
                                                        .value[currentIndex - 1];
                                                    context.pop();
                                                    context.push('/gameBoard',
                                                        extra: wordLevelBox);
                                                  },
                                                ),
                                              );
                                            }
                                          },
                                        ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ],
                    );
                  },
                ),
              ),

              Positioned.fill(
                top: Platform.isAndroid ? 40 : 0,
                child: SafeArea(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GradientShadowButton(
                        strokeWidth: 0.2,
                        height: 40,
                        width: 40,
                        padding: const EdgeInsets.all(5),
                        gradientColors: GameColors.appBackgroundGradient,
                        onTap: () async {
                          context.push('/profileScreen');
                        },
                        child: const Icon(Icons.person, color: Colors.white),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: CoinWidget(
                          widgetType: WidgetType.live,
                          onTap: () {},
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: CoinWidget(
                          onTap: () async {},
                          widgetType: WidgetType.coin,
                          key: _endKey,
                        ),
                      ),
                      GradientShadowButton(
                        strokeWidth: 0.2,
                        height: 40,
                        width: 40,
                        padding: const EdgeInsets.all(5),
                        gradientColors: GameColors.appBackgroundGradient,
                        onTap: () {
                          context.push('/settingScreen');
                        },
                        child: const Icon(Icons.settings, color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 32.0, top: 0),
                      child: Image.asset(
                        "assets/images/coin3dd.png",
                        height: 30,
                        width: 30,
                        fit: BoxFit.fill,
                        color: Colors.transparent,
                        key: _startKey,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        bottomSheet: Container(
          height: size.height / 14,
          width: double.infinity,
          decoration: const BoxDecoration(
            color: Color(0xff673323),
            border: Border(top: BorderSide(color: Color(0xff7b401e), width: 6)),
          ),
          // child: AppAds(adsType: AdsType.banner,),
        ),
      ),
    );
  }

  addCoinAnimation() async {
    final audioController = context.read<AudioController>();
    List<OverlayEntry>? overlayEntry = List.generate(
      5,
      (index) => OverlayEntry(
        builder: (_) {
          Random random = Random();
          int randomNumber = random.nextInt(101) + 120;
          int randomNumber2 = random.nextInt(101) + 380;
          return AtoBAnimation(
            startPosition:
                Offset(randomNumber.toDouble(), randomNumber2.toDouble()),
            endPosition: _endOffset,
            dxCurveAnimation: 0,
            dyCurveAnimation: 0,
            duration: Duration(milliseconds: (index + 1) * 200),
            opacity: 1,
            child: Image.asset(
              "assets/images/coin3dd.png",
              height: 35,
              width: 35,
              fit: BoxFit.fill,
            ),
          );
        },
      ),
    );
    Overlay.of(context).insertAll(overlayEntry);
    final setting = context.read<SettingsController>();
    var coinProvider = context.read<CoinProvider>();

    for (int i = 0; i < overlayEntry.length; i++) {
      await Future.delayed(const Duration(milliseconds: 200)).then((onValue) {
        coinProvider.startAnimation(context, widgetType: WidgetType.coin);
      });
    }
    Future.delayed(Duration(milliseconds: overlayEntry.length * 200), () {
      for (var overlay in overlayEntry ?? []) {
        overlay.remove();
      }
      overlayEntry = null;
    });
  }
}

enum ShimmerDirection { ltr, rtl, ttb, btt }

@immutable
class Shimmer extends StatefulWidget {
  final Widget child;
  final Duration period;
  final ShimmerDirection direction;
  final Gradient gradient;
  final int loop;
  final bool enabled;

  const Shimmer({
    super.key,
    required this.child,
    required this.gradient,
    this.direction = ShimmerDirection.ltr,
    this.period = const Duration(milliseconds: 1500),
    this.loop = 0,
    this.enabled = true,
  });

  Shimmer.fromColors({
    super.key,
    required this.child,
    required Color baseColor,
    required Color highlightColor,
    this.period = const Duration(milliseconds: 1500),
    this.direction = ShimmerDirection.ltr,
    this.loop = 0,
    this.enabled = true,
  }) : gradient = LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.centerRight,
            colors: <Color>[
              baseColor,
              baseColor,
              highlightColor,
              baseColor,
              baseColor
            ],
            stops: const <double>[
              0.0,
              0.35,
              0.5,
              0.65,
              1.0
            ]);

  @override
  _ShimmerState createState() => _ShimmerState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<Gradient>('gradient', gradient,
        defaultValue: null));
    properties.add(EnumProperty<ShimmerDirection>('direction', direction));
    properties.add(
        DiagnosticsProperty<Duration>('period', period, defaultValue: null));
    properties
        .add(DiagnosticsProperty<bool>('enabled', enabled, defaultValue: null));
    properties.add(DiagnosticsProperty<int>('loop', loop, defaultValue: 0));
  }
}

class _ShimmerState extends State<Shimmer> with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  int _count = 0;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this, duration: widget.period)
      ..addStatusListener((AnimationStatus status) {
        if (status != AnimationStatus.completed) {
          return;
        }
        _count++;
        if (widget.loop <= 0) {
          _controller.repeat();
        } else if (_count < widget.loop) {
          _controller.forward(from: 0.0);
        }
      });
    if (widget.enabled) {
      _controller.forward();
    }
  }

  @override
  void didUpdateWidget(Shimmer oldWidget) {
    if (widget.enabled) {
      _controller.forward();
    } else {
      _controller.stop();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      child: widget.child,
      builder: (BuildContext context, Widget? child) => _Shimmer(
        direction: widget.direction,
        gradient: widget.gradient,
        percent: _controller.value,
        child: child,
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

@immutable
class _Shimmer extends SingleChildRenderObjectWidget {
  final double percent;
  final ShimmerDirection direction;
  final Gradient gradient;

  const _Shimmer({
    Widget? child,
    required this.percent,
    required this.direction,
    required this.gradient,
  }) : super(child: child);

  @override
  _ShimmerFilter createRenderObject(BuildContext context) {
    return _ShimmerFilter(percent, direction, gradient);
  }

  @override
  void updateRenderObject(BuildContext context, _ShimmerFilter shimmer) {
    shimmer.percent = percent;
    shimmer.gradient = gradient;
    shimmer.direction = direction;
  }
}

class _ShimmerFilter extends RenderProxyBox {
  ShimmerDirection _direction;
  Gradient _gradient;
  double _percent;

  _ShimmerFilter(this._percent, this._direction, this._gradient);

  @override
  ShaderMaskLayer? get layer => super.layer as ShaderMaskLayer?;

  @override
  bool get alwaysNeedsCompositing => child != null;

  set percent(double newValue) {
    if (newValue == _percent) {
      return;
    }
    _percent = newValue;
    markNeedsPaint();
  }

  set gradient(Gradient newValue) {
    if (newValue == _gradient) {
      return;
    }
    _gradient = newValue;
    markNeedsPaint();
  }

  set direction(ShimmerDirection newDirection) {
    if (newDirection == _direction) {
      return;
    }
    _direction = newDirection;
    markNeedsLayout();
  }

  @override
  void paint(PaintingContext context, Offset offset) {
    if (child != null) {
      assert(needsCompositing);

      final double width = child!.size.width;
      final double height = child!.size.height;
      Rect rect;
      double dx, dy;
      if (_direction == ShimmerDirection.rtl) {
        dx = _offset(width, -width, _percent);
        dy = 0.0;
        rect = Rect.fromLTWH(dx - width, dy, 3 * width, height);
      } else if (_direction == ShimmerDirection.ttb) {
        dx = 0.0;
        dy = _offset(-height, height, _percent);
        rect = Rect.fromLTWH(dx, dy - height, width, 3 * height);
      } else if (_direction == ShimmerDirection.btt) {
        dx = 0.0;
        dy = _offset(height, -height, _percent);
        rect = Rect.fromLTWH(dx, dy - height, width, 3 * height);
      } else {
        dx = _offset(-width, width, _percent);
        dy = 0.0;
        rect = Rect.fromLTWH(dx - width, dy, 3 * width, height);
      }
      layer ??= ShaderMaskLayer();
      layer!
        ..shader = _gradient.createShader(rect)
        ..maskRect = offset & size
        ..blendMode = BlendMode.srcIn;
      context.pushLayer(layer!, super.paint, offset);
    } else {
      layer = null;
    }
  }

  double _offset(double start, double end, double percent) {
    return start + (end - start) * percent;
  }
}

class FaceOutlinePainter extends CustomPainter {
  final bool isDarkMode;

  FaceOutlinePainter({this.isDarkMode = false});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = Colors.green
      ..style = PaintingStyle.stroke
      ..strokeWidth = 10.0;
    Paint paint2 = Paint()
      ..color = Colors.yellow
      ..style = PaintingStyle.stroke
      ..strokeWidth = 10.0;
    Path path = Path();

    path.moveTo(size.width / 2, 0); //Ax, Ay
    path.quadraticBezierTo(size.width, size.height / 4, size.width / 2,
        size.height / 2); //Bx, By, Cx, Cy
    path.quadraticBezierTo(
        0, 3 * size.height / 4, size.width / 2, size.height); //Dx, Dy, Ex, Ey
    canvas.drawPath(
      dashPath(
        path,
        dashArray: CircularIntervalList([10, 10]),
      ),
      paint2,
    );
    canvas.drawPath(
      dashPath(
        path,
        dashArray: CircularIntervalList([0.1]),
      ),
      paint,
    );
  }

  @override
  bool shouldRepaint(FaceOutlinePainter oldDelegate) => false;
}

// Image.asset(
//   "assets/images/game_map_2.png",
//   // height: size.height,
//   width: size.width,
//   fit: BoxFit.fitWidth,
// ),
// ...List.generate(
//   6,
//   (i) {
//     var count = 6;
//     int currentIndex = index * count + i + 1;
//     return Positioned(
//       top: i % count == 5
//           ? 0
//           : i % count == 4
//               ? 80
//               : i % count == 3
//                   ? 170
//                   : i % count == 2
//                       ? 250
//                       : i % count == 1
//                           ? 320
//                           : 390,
//       left: i % count == 5
//           ? 140
//           : i % count == 4
//               ? 170
//               : i % count == 3
//                   ? 80
//                   : i % count == 2
//                       ? 220
//                       : i % count == 1
//                           ? 90
//                           : 235,
//       child: Transform(
//         transform: Matrix4.rotationX(1),
//         child: ShadowButton(
//           onTap: () {},
//           elevation: 20,
//           borderRadius: 100,
//           shadowColor: Colors.deepOrange,
//           backgroundColor: Colors.orange,
//           margin: EdgeInsets.zero,
//           // shape: BoxShape.circle,
//           child: Text(
//             " ${currentIndex} ",
//             style: const TextStyle(
//                 fontSize: 40,
//                 color: Colors.white,
//                 fontWeight: FontWeight.w900),
//           ),
//         ),
//       ),
//     );
//   },
// ),

class NeoPopTiltedButtonShimmerPainter extends CustomPainter {
  const NeoPopTiltedButtonShimmerPainter({
    required this.tiltAngle,
    required this.plunkAngle,
    required this.depth,
    required this.shimmerWidth,
    required this.color,
    required this.ticker,
    required this.plunkColor,
  }) : super(repaint: ticker);

  /// Angle at which the shimmer is tilted.
  final double tiltAngle;

  /// Angle at which the plunk of the shimmer is tilted.
  final double plunkAngle;

  /// Width of the shimmer.
  final double shimmerWidth;

  /// Depth of the button
  final double depth;

  /// Color of the shimmer
  final Color color;

  /// Color of the shimmer over plunk
  final Color plunkColor;

  /// Ticker to animate the shimmer position and angle
  final Animation<double> ticker;

  @override
  void paint(Canvas canvas, Size size) {
    final smallShimmer = shimmerWidth / 2;
    const padding = 5.0;
    final width = size.width + padding + (2 * shimmerWidth);
    final perUnitTiltAngleChange = (math.pi - (2 * tiltAngle)) / size.width;
    final y = size.height;
    double x = width * ticker.value;

    /// dynamic tangent of the tilt angle
    final endTiltAngle = tiltAngle + (perUnitTiltAngleChange * width);
    final tanTheta =
        math.tan(tiltAngle + ((endTiltAngle - tiltAngle) * ticker.value));

    /// tangent of the (pi - shadow's tilt angle)
    final endPlunkAngle =
        plunkAngle + ((math.pi - (2 * plunkAngle)) * width / size.width);
    final tanAlpha =
        math.tan(plunkAngle + ((endPlunkAngle - plunkAngle) * ticker.value));

    final p = y / tanTheta;
    final q = depth / tanAlpha;

    final Path path = Path();
    final paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;

    // paint large shimmer
    canvas.drawPath(
      path
        ..addPolygon(
          [
            Offset(x, y),
            Offset(x + p, 0),
            Offset(x + p - shimmerWidth, 0),
            Offset(x - shimmerWidth, y),
          ],
          true,
        ),
      paint,
    );

    path.reset();
    paint.color = plunkColor;
    canvas.drawPath(
      path
        ..addPolygon(
          [
            Offset(x, y),
            Offset(x - shimmerWidth, y),
            Offset(x - shimmerWidth + q, y + depth),
            Offset(x + q, y + depth),
          ],
          true,
        ),
      paint,
    );

    // paint small shimmer
    x = x - shimmerWidth - padding;
    path.reset();
    paint.color = color;

    canvas.drawPath(
      path
        ..addPolygon(
          [
            Offset(x, y),
            Offset(x - smallShimmer, y),
            Offset(x - smallShimmer + p, 0),
            Offset(x + p, 0),
          ],
          true,
        ),
      paint,
    );

    path.reset();
    paint.color = plunkColor;
    canvas.drawPath(
      path
        ..addPolygon(
          [
            Offset(x, y),
            Offset(x - smallShimmer, y),
            Offset(x - smallShimmer + q, y + depth),
            Offset(x + q, y + depth),
          ],
          true,
        ),
      paint,
    );

    path.reset();
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}
