import 'dart:math';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:provider/provider.dart';
import 'package:wordly/games/word_game/setting/setting_dialog.dart';

import '../../../../animations/coin_animation.dart';
import '../../../../animations/scal_animation_button.dart';
import '../../audio/audio_controller.dart';
import '../../audio/sounds.dart';
import '../../onboard/daly_login_reward_screen.dart';
import '../../setting/setting_controller.dart';
import '../../utils/enums.dart';
import '../dashboard/dashboard_screen.dart';
import '../widgets/game_coin_widget.dart';

class CoinBuyScreen extends StatefulWidget {
  const CoinBuyScreen(
      {super.key, required this.level, this.fromDashBoard = false});

  final String level;
  final bool fromDashBoard;

  @override
  State<CoinBuyScreen> createState() => _CoinBuyScreenState();
}

class _CoinBuyScreenState extends State<CoinBuyScreen>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  final GlobalKey _startKey = GlobalKey();
  Offset _startOffset = Offset.zero;
  Offset _endOffset = Offset.zero;
  final GlobalKey _endKey = GlobalKey();
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((c) {
      _startOffset = (_startKey.currentContext?.findRenderObject() as RenderBox)
          .localToGlobal(Offset.zero);
    });
    WidgetsBinding.instance.addPostFrameCallback((c) {
      _endOffset = (_endKey.currentContext?.findRenderObject() as RenderBox)
          .localToGlobal(Offset.zero);
    });
    _controller = AnimationController(
      duration: const Duration(seconds: 2), // Adjust duration as needed
      vsync: this,
    )..repeat();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color(0xff002a6a),
      body: Container(
        width: kIsWeb ? 350 : size.width,
        height: size.height,
        decoration: const BoxDecoration(
          gradient: RadialGradient(
            tileMode: TileMode.clamp,
            center: Alignment.center,
            radius: 0.8,
            colors: [
              Color(0xff175395),
              Color(0xff0d2655),
            ],
          ),
        ),
        child: Column(
          children: [
            Container(
              decoration: const BoxDecoration(
                gradient: RadialGradient(
                  tileMode: TileMode.clamp,
                  center: Alignment.bottomCenter,
                  radius: 1.5,
                  colors: [
                    Color(0xff015898),
                    Color(0xff013382),
                  ],
                ),
              ),
              height: size.height / 7.5,
              width: double.infinity,
              alignment: const Alignment(0.0, 0.8),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8),
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CoinWidget(
                            key: _endKey,
                            widgetType: WidgetType.coin,
                          ),
                          CoinWidget(
                            widgetType: WidgetType.live,
                          ),
                        ],
                      ),
                    ),
                    const SGText("Shop", fontSize: 35),
                  ],
                ),
              ),
            ),
            Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  tileMode: TileMode.clamp,
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xffb56540),
                    Color(0xff995337),
                    Color(0xff673926),
                  ],
                ),
              ),
              height: 10,
              width: double.infinity,
            ),
            // const SizedBox(height: 20),
            ///
            Expanded(
              child: SingleChildScrollView(
                child: AnimationLimiter(
                  child: ListView(

                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    padding: const EdgeInsets.only(top: 16, bottom: 180),
                    children: [
                      // for (int i = 0; i < 1; i++)
                        // AnimationConfiguration.staggeredList(
                        // position: i,
                        // duration: const Duration(milliseconds: 575),
                        // child: SlideAnimation(
                        //   verticalOffset: 1500,
                        //   delay: const Duration(milliseconds: 150),
                        //   child:
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: 12, left: 8, right: 8),
                          child: GradientBorder2(

                            borderRadius: 12,
                            strokeWidth: 1,
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: const Color(0xff89690e), width: 1),
                                gradient: const LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: GameColors.greenGradient,
                                  tileMode: TileMode.repeated,
                                ),
                                borderRadius: BorderRadius.circular(12 - 1),
                              ),
                              alignment: Alignment.center,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: Column(
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 5),
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter,
                                          colors: [
                                            gColor('fbebd5'),
                                            gColor('f7e0bc'),
                                            gColor('deb890'),
                                          ],
                                        ),
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          Stack(
                                            alignment: Alignment.topCenter,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    bottom: 10.0),
                                                child: Image.asset(
                                                  "assets/images/sun_ray_bg_1.png",
                                                  height: 80,
                                                  width: 80,
                                                  color: Colors.white,
                                                ),
                                              ), // Rep
                                              Image.asset(
                                                "assets/images/coin_6.png",
                                                height: 70,
                                                width: 70,
                                              ),
                                              const Positioned(
                                                bottom: -5,
                                                child: SGText(
                                                  "3000",
                                                  textGradiantColors: GameColors
                                                      .textGreenGradient,
                                                  fontSize: 20,
                                                  shadowWith: 1.5,
                                                  letterSpacing: 2,
                                                ),
                                              ),
                                            ],
                                          ),
                                          Stack(
                                            alignment: Alignment.topCenter,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    bottom: 10.0),
                                                child: Image.asset(
                                                  "assets/images/sun_ray_bg_1.png",
                                                  height: 80,
                                                  width: 80,
                                                  color: Colors.amber,
                                                ),
                                              ), // Rep
                                              Image.asset(
                                                "assets/images/heart.png",
                                                height: 70,
                                                width: 70,
                                              ),
                                              const Positioned(
                                                bottom: -5,
                                                child: SGText(
                                                  "5",
                                                  textGradiantColors: GameColors
                                                      .textGreenGradient,
                                                  fontSize: 20,
                                                  shadowWith: 1.5,
                                                  letterSpacing: 2,
                                                ),
                                              ),
                                            ],
                                          ),
                                          Stack(
                                            alignment: Alignment.topCenter,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    bottom: 10.0),
                                                child: Image.asset(
                                                  "assets/images/sun_ray_bg_1.png",
                                                  height: 80,
                                                  width: 80,
                                                  color: Colors.white,
                                                ),
                                              ), // Rep
                                              Image.asset(
                                                "assets/images/wHelper.png",
                                                height: 70,
                                                width: 70,
                                              ),
                                              const Positioned(
                                                bottom: -5,
                                                child: SGText(
                                                  "10",
                                                  textGradiantColors: GameColors
                                                      .textGreenGradient,
                                                  fontSize: 20,
                                                  shadowWith: 1.5,
                                                  letterSpacing: 2,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      // height: 40,
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter,
                                          colors: [
                                            gColor('b455fa'),
                                            gColor('9c2fe8'),
                                            gColor('8b1bdb'),
                                            gColor('7815d4'),
                                          ],
                                        ),
                                      ),
                                      // height: 40,
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 8.0, vertical: 4),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            const SGText(
                                              "Special offer",
                                              textGradiantColors:
                                                  GameColors.whiteGradient,
                                              fontSize: 20,
                                              shadowWith: 1.5,
                                              letterSpacing: 2,
                                            ),
                                            GradientShadowButton(
                                              text: "₹199.00",
                                              fontSize: 18,
                                              borderRadius: 8,
                                              shadowWith: 1.5,
                                              strokeWidth: 0.5,
                                              showShimmer: true,
                                              // width: 80,
                                              // height: 60,
                                              textGradiantColors:
                                                  GameColors.whiteGradient,
                                              shadowColors:
                                                  const Color(0xff1b6b00),
                                              gradientColors:
                                                  GameColors.greenGradient,
                                              onTap: () async {
                                                final audioController = context
                                                    .read<AudioController>();
                                                var provider = context.read<LiveProvider>();

                                                List<OverlayEntry>?
                                                    overlayEntry =
                                                    List.generate(
                                                  5,
                                                  (index) => OverlayEntry(
                                                    builder: (_) {
                                                      Random random = Random();
                                                      int randomNumber =
                                                          random.nextInt(101) +
                                                              120;
                                                      int randomNumber2 =
                                                          random.nextInt(101) +
                                                              380;
                                                      return AtoBAnimation(
                                                        startPosition:
                                                            _startOffset,
                                                        // Offset(randomNumber.toDouble(), randomNumber2.toDouble()),
                                                        endPosition:
                                                            _endOffset,
                                                        dxCurveAnimation: 0,
                                                        dyCurveAnimation: 0,
                                                        duration: Duration(
                                                            milliseconds:
                                                                (index + 1) *
                                                                    200),
                                                        opacity: 1,
                                                        child: Image.asset(
                                                          "assets/images/coin3dd.png",
                                                          height: 45,
                                                          width: 45,
                                                          fit: BoxFit.fill,
                                                        ),
                                                      );
                                                    },
                                                  ),
                                                );
                                                Overlay.of(context)
                                                    .insertAll(overlayEntry);
                                                final setting = context
                                                    .read<SettingsController>();
                                                for (int i = 0;
                                                    i < overlayEntry.length;
                                                    i++) {
                                                  int coin = setting.coin.value;
                                                  await Future.delayed(
                                                          const Duration(
                                                              milliseconds:
                                                                  200))
                                                      .then((onValue) {
                                                    provider.startAnimation(context,widgetType: WidgetType.live);
                                                    // audioController.playSfx(Sounds.coin);
                                                  });
                                                  // setting.setCoin(coin + 1);
                                                }
                                                Future.delayed(
                                                    Duration(
                                                        milliseconds:
                                                            overlayEntry
                                                                    .length *
                                                                200), () {
                                                  for (var overlay
                                                      in overlayEntry ?? []) {
                                                    overlay.remove();
                                                  }
                                                  overlayEntry = null;
                                                });
                                                // showAnimateDialog(context,PlayDialog(level:"",onTap:(){ },),);

                                                // showAnimateDialog(context, QuiteDialog(onQuit: () {}));
                                                // context.pushReplacement('/dashBoardScreen');
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      //   ),
                      // ),
                    ],
                  ),
                ),
              ),
            ),

             SGText("Shop", fontSize: 35, key: _startKey,),

          ],
        ),
      ),
    );
  }
}






class ShadowButton extends StatefulWidget {
  final BoxShape shape;
  final double borderRadius;
  final Widget child;
  final Color backgroundColor;
  final Color shadowColor;
  final bool isSelected;
  final String name;
  final EdgeInsets? padding;
  final EdgeInsets? margin;
  final GestureTapCallback onTap;
  final double elevation;
  final Duration clickDuration;

  const ShadowButton(
      {this.shape = BoxShape.rectangle,
      this.borderRadius = 12,
      this.isSelected = false,
      this.name = "",
      this.padding,
      this.margin,
      this.elevation = 6.0,
      this.clickDuration = const Duration(milliseconds: 150),
      this.backgroundColor = Colors.white,
      this.shadowColor = Colors.black12,
      required this.onTap,
      required this.child,
      Key? key})
      : super(key: key);

  @override
  State<ShadowButton> createState() => _ShadowButtonState();
}

class _ShadowButtonState extends State<ShadowButton> {
  late double _elevation;

  @override
  void initState() {
    _elevation = widget.elevation;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        await Future.delayed(const Duration(milliseconds: 200));
        widget.onTap();
      },
      onTapDown: (_) async {
        setState(() => _elevation = 0);
        await Future.delayed(const Duration(milliseconds: 200));
        setState(() => _elevation = widget.elevation);
      },
      child: AnimatedContainer(
        padding: EdgeInsets.only(bottom: _elevation),
        margin: widget.margin ?? EdgeInsets.all(widget.isSelected ? 22 : 10),
        duration: const Duration(milliseconds: 350),
        decoration: BoxDecoration(
          color: widget.shadowColor,
          shape: widget.shape,
          borderRadius: widget.shape == BoxShape.rectangle
              ? BorderRadius.circular(widget.borderRadius)
              : null,
        ),
        child: Container(
          padding: widget.padding ?? const EdgeInsets.all(8),
          decoration: BoxDecoration(
            // border: Border.all(),
            shape: widget.shape,
            color: widget.backgroundColor,
            borderRadius: widget.shape == BoxShape.rectangle
                ? BorderRadius.circular(widget.borderRadius)
                : null,
          ),
          child: widget.child,
        ),
      ),
    );
  }
}


// class CoinWidget extends StatefulWidget {
//   final Function()? onTap;
//   final AnimationControllerHandler? controllerHandler;
//   final WidgetType widgetType;
//   final int coinCount;
//
//   const CoinWidget({
//     super.key,
//     this.widgetType = WidgetType.none,
//     this.onTap,
//     this.coinCount = 0,
//     this.controllerHandler,
//   });
//
//   @override
//   State<CoinWidget> createState() => _CoinWidgetState();
// }
//
// class _CoinWidgetState extends State<CoinWidget>
//     with SingleTickerProviderStateMixin {
//   late AnimationController _controller;
//   late Animation<double> _scaleAnimation;
//
//   @override
//   void initState() {
//     super.initState();
//     _controller = AnimationController(
//       duration: const Duration(milliseconds: 150),
//       vsync: this,
//     );
//
//     _scaleAnimation = Tween<double>(
//       begin: 1.0,
//       end: 1.3,
//     ).animate(CurvedAnimation(
//       parent: _controller,
//       curve: Curves.bounceInOut,
//     ));
//   }
//
//   void _incrementCounter(int length) async {
//     for (int i = 0; i < length; i++) {
//       _controller.forward().then((_) => _controller.reverse());
//       await Future.delayed(const Duration(milliseconds: 210));
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     var size = MediaQuery.of(context).size;
//
//     return ScaleAnimationButton2(
//       onTap: () {
//         // _incrementCounter(5);
//         if (widget.onTap != null) {
//           widget.onTap!();
//         }
//       },
//       controllerHandler: widget.controllerHandler,
//       widgetType: widget.widgetType,
//       // child:
//       // Stack(
//       //   children: [
//       //     Container(
//       //       margin: const EdgeInsets.only(bottom: 10, left: 18, top: 2),
//       //       padding: const EdgeInsets.all(1.5),
//       //       width: size.width / 5,
//       //       decoration: BoxDecoration(
//       //         color: const Color(0xffb67b52),
//       //         borderRadius: BorderRadius.circular(8),
//       //       ),
//       //       child: Container(
//       //         padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 1),
//       //         decoration: BoxDecoration(
//       //           borderRadius: BorderRadius.circular(8),
//       //           gradient: const LinearGradient(
//       //             begin: Alignment.topCenter,
//       //             end: Alignment.bottomCenter,
//       //             colors: [
//       //               Color(0xffffebbc),
//       //               Color(0xffe0c389),
//       //             ],
//       //           ),
//       //         ),
//       //         child: widget.widgetType == WidgetType.coin
//       //             ? ValueListenableBuilder<int>(
//       //                 valueListenable: context.watch<SettingsController>().coin,
//       //                 builder: (context, coin, child) =>
//       //                     animationText(coin.toString()))
//       //             : widget.widgetType == WidgetType.level
//       //                 ? ValueListenableBuilder<int>(
//       //                     valueListenable:
//       //                         context.watch<SettingsController>().level,
//       //                     builder: (context, level, child) =>
//       //                         animationText(level.toString()))
//       //                 : ValueListenableBuilder<int>(
//       //                     valueListenable:
//       //                         context.watch<SettingsController>().live,
//       //                     builder: (context, live, child) =>
//       //                         animationText(live.toString())),
//       //       ),
//       //     ),
//       //     Transform.scale(
//       //       scale: 1.1,
//       //       child: Image.asset(
//       //         widget.widgetType == WidgetType.coin
//       //             ? "assets/images/coin3dd.png"
//       //             : "assets/images/heart.png",
//       //         height: 30,
//       //         width: 30,
//       //         fit: BoxFit.fill,
//       //       ),
//       //     ),
//       //   ],
//       // ),
//     );
//   }
// }

/*  Expanded(
                  child: SingleChildScrollView(
                    child: AnimationLimiter(
                      child: GridView.count(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          crossAxisCount: 2,
                          children: [
                            for (int i = 0; i < 3; i++)
                              AnimationConfiguration.staggeredList(
                                position: i,
                                duration: const Duration(milliseconds: 575),
                                child: SlideAnimation(
                                  verticalOffset: 1500,
                                  delay: const Duration(milliseconds: 150),
                                  child: ClipPath(
                                    clipper: MyCustomClipper(),
                                    child: Container(
                                      margin: const EdgeInsets.all(5),
                                      height: 60,
                                      color: Colors.deepOrange,
                                      child: Container(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 30, vertical: 5),
                                        decoration: const BoxDecoration(
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.orange,
                                              spreadRadius: -10,
                                              blurRadius: 10,
                                            ),
                                          ],
                                        ),
                                        child: Stack(
                                          alignment: Alignment.center,
                                          children: [
                                            Transform.scale(
                                              scale: 1.2,
                                              child: AnimatedBuilder(
                                                animation: _controller,
                                                builder: (context, child) {
                                                  return Transform.rotate(
                                                    angle:
                                                        _controller.value * 3,
                                                    // Full rotation (2π) based on controller value
                                                    child: Image.asset(
                                                      "assets/images/sun_ray_bg_1.png",
                                                      fit: BoxFit.fill,
                                                      color: Colors.yellow,
                                                    ), // Replace 'assets/your_image.png' with your image path
                                                  );
                                                },
                                              ),
                                            ),
                                            Image.asset(
                                              "assets/images/coin_6.png",
                                              height: 150,
                                              width: 150,
                                              fit: BoxFit.fill,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  // ShadowButton(
                                  //   onTap: () {  },
                                  //   backgroundColor: Color(0xff1297d7),
                                  //   shadowColor: Color(0xff103c69),
                                  //   child: Stack(
                                  //     alignment: Alignment.center,
                                  //     children: [
                                  //       Transform.scale(
                                  //         scale: 1.2,
                                  //         child: Image.asset(
                                  //           "assets/images/sun_ray_bg_1.png",
                                  //           fit: BoxFit.fill,
                                  //           color: Colors.yellow,
                                  //         ),
                                  //       ),
                                  //       Image.asset(
                                  //         "assets/images/coin_6.png",
                                  //         height: 150,
                                  //         width: 150,
                                  //         fit: BoxFit.fill,
                                  //       ),
                                  //     ],
                                  //   ),
                                  // ),
                                ),
                              ),
                          ]
                          // List.generate(
                          //   7,
                          //   (int index) {
                          //     return AnimationConfiguration.staggeredGrid(
                          //       position: index,
                          //       duration: const Duration(milliseconds: 575),
                          //       columnCount: 2,
                          //       child: SlideAnimation(
                          //         verticalOffset: 1500,
                          //         delay: const Duration(milliseconds: 150),
                          //         child: Image.asset(
                          //           "assets/images/coin_6.png",
                          //           height: 100,
                          //           width: 100,
                          //           fit: BoxFit.fill,
                          //         ),
                          //       ),
                          //     );
                          //   },
                          // ),
                          ),
                    ),
                  ),
                ),*/
// AnimationLimiter(
//   child: Column(
//     children: AnimationConfiguration.toStaggeredList(
//         duration: const Duration(milliseconds: 575),
//         childAnimationBuilder: (widget) => SlideAnimation(
//               verticalOffset: 1500,
//               delay: const Duration(milliseconds: 150),
//               child: widget,
//             ),
//         children: List.generate(
//           6,
//           (index) => Container(
//             margin: const EdgeInsets.all(5),
//             color: Colors.deepOrange,
//             height: size.height / 5,
//             width: size.width,
//             child: Row(
//               children: [
//                 Image.asset(
//                   "assets/images/coin_6.png",
//                   height: 100,
//                   width: 100,
//                   fit: BoxFit.fill,
//                 ), Image.asset(
//                   "assets/images/sun_ray_bg_1.png",
//                   height: 100,
//                   width: 100,
//                   // fit: BoxFit.fill,
//                   color: Colors.yellow,
//                 ),
//               ],
//             ),
//           ),
//         ).toList()),
//   ),
// ),
