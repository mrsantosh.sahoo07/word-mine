import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import '../dashboard/dashboard_screen.dart';

class EventScreen extends StatefulWidget {
  const EventScreen({super.key});

  @override
  State<EventScreen> createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 2), // Adjust duration as needed
      vsync: this,
    )..repeat();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var size = MediaQuery.of(context).size;
    bool isLocked = true;
    return Scaffold(
      body: Container(
        height: size.height,
        width: size.width,
        decoration: const BoxDecoration(
          gradient: RadialGradient(
            tileMode: TileMode.clamp,
            center: Alignment.center,
            radius: 0.8,
            colors: [
              Color(0xff175395),
              Color(0xff0d2655),
            ],
          ),
        ),
        child: Column(
          children: [
            Container(
              decoration: const BoxDecoration(
                gradient: RadialGradient(
                  tileMode: TileMode.clamp,
                  center: Alignment.bottomCenter,
                  radius: 1.5,
                  colors: [
                    Color(0xff015898),
                    Color(0xff013382),
                  ],
                ),
              ),
              height: size.height / 7.5,
              width: double.infinity,
              alignment: const Alignment(0.0, 0.8),
              child: const SGText(
                "Event",
                fontSize: 35,
              ),
            ),
            Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  tileMode: TileMode.clamp,
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xffb56540),
                    Color(0xff995337),
                    Color(0xff673926),
                  ],
                ),
              ),
              height: 10,
              width: double.infinity,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: AnimationLimiter(
                  child: ListView(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      padding: const EdgeInsets.only(top: 16, bottom: 180),
                      children: [
                        // SizedBox(
                        //   height: size.height / 6,
                        // ),
                        // Center(
                        //     child: Image.asset('assets/images/coming_soon.png'))
                        for (int i = 0; i < 1; i++)
                          AnimationConfiguration.staggeredList(
                            position: i,
                            duration: const Duration(milliseconds: 575),
                            child: SlideAnimation(
                                verticalOffset: 1500,
                                delay: const Duration(milliseconds: 150),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(28),
                                  child: Container(
                                    width: double.infinity,
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 8, vertical: 5),
                                    padding: const EdgeInsets.only(bottom: 6),
                                    decoration: BoxDecoration(
                                      color: isLocked
                                          ? Colors.grey.shade900
                                          : const Color(0xff6a260c),
                                      borderRadius: BorderRadius.circular(18),
                                    ),
                                    child: DecoratedBox(
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          tileMode: TileMode.clamp,
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter,
                                          colors: isLocked
                                              ? [
                                                  Colors.grey.shade700,
                                                  Colors.grey.shade700
                                                ]
                                              : const [
                                                  Color(0xffe7a44c),
                                                  Color(0xffda8b50),
                                                  Color(0xffdc842f),
                                                  Color(0xffc87234),
                                                ],
                                        ),
                                        border: Border.all(
                                          color: isLocked
                                              ? Colors.grey.shade700
                                              : const Color(0xffecc378),
                                          width: 0.5,
                                        ),
                                        borderRadius: BorderRadius.circular(18),
                                      ),
                                      child: Container(
                                        margin: const EdgeInsets.all(8),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(18),
                                          border: Border.all(
                                            color: isLocked
                                                ? Colors.grey
                                                : const Color(0xff762f07),
                                            width: 2,
                                          ),
                                        ),
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(18),
                                          child: ColorFiltered(
                                            colorFilter: ColorFilter.mode(
                                              Colors.grey,
                                              isLocked
                                                  ? BlendMode.color
                                                  : BlendMode.dst,
                                            ),
                                            child: Image.asset(
                                              "assets/images/splash_bg_4.jpeg",
                                              fit: BoxFit.fitWidth,
                                              height: 150,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                )),
                          ),
                      ]),
                ),
              ),
            ),
          ],
        ),
      ),
      bottomSheet: Container(
        height: size.height / 14,
        width: double.infinity,
        decoration: const BoxDecoration(
          color: Color(0xff673323),
          border: Border(top: BorderSide(color: Color(0xff7b401e), width: 6)),
        ),
        // child: AppAds(adsType: AdsType.banner,),
      ),
    );
  }
}
