import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
// import 'package:google_fonts/google_fonts.dart';

import '../../../../animations/coin_animation.dart';
import '../../../../animations/confetti_animation.dart';
import '../../../../animations/custom_clipeer.dart';
import '../../audio/audio_controller.dart';
import '../../audio/sounds.dart';
import '../../provider/widget_provider.dart';
import '../../setting/setting_controller.dart';
import '../../setting/setting_dialog.dart';
import '../coin_buy/coin_buy_screen.dart';
import '../dashboard/widgets/game_appbar.dart';
import '../widgets/game_coin_widget.dart';

class MyAnimatedImage extends StatefulWidget {
  const MyAnimatedImage({super.key});

  @override
  _MyAnimatedImageState createState() => _MyAnimatedImageState();
}

class _MyAnimatedImageState extends State<MyAnimatedImage>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  final GlobalKey _coinStartKey = GlobalKey();
  final GlobalKey _starStartKey = GlobalKey();
  final GlobalKey _coinEndKey = GlobalKey();
  final GlobalKey _starEndKey = GlobalKey();
  Offset _coinStartOffset = Offset.zero;
  Offset _starStartOffset = Offset.zero;
  Offset _coinEndOffset = Offset.zero;
  Offset _starEndOffset = Offset.zero;
  bool showAnimation = true;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 2), // Adjust duration as needed
      vsync: this,
    )..repeat();
    WidgetsBinding.instance.addPostFrameCallback((c) {
      _coinStartOffset =
          (_coinStartKey.currentContext?.findRenderObject() as RenderBox)
              .localToGlobal(Offset.zero);
      _coinEndOffset =
          (_coinEndKey.currentContext?.findRenderObject() as RenderBox)
              .localToGlobal(Offset.zero);
      _starStartOffset =
          (_starStartKey.currentContext?.findRenderObject() as RenderBox)
              .localToGlobal(Offset.zero);
      _starEndOffset =
          (_starEndKey.currentContext?.findRenderObject() as RenderBox)
              .localToGlobal(Offset.zero);
    });
    coinAnimationStart(true);
  }

  coinAnimationStart(bool isCoin) async {
    final audioController = context.read<AudioController>();

    Timer(const Duration(milliseconds: 300), () async {
      audioController.playSfx(Sounds.win);
    });

    Timer(const Duration(seconds: 3), () async {
      var provider = context.read<WidgetsProvider>();
      showAnimation = false;
      setState(() {});
      for (int i = 0; i < 5; i++) {
        if (i == 4) {
          for (int i = 0; i < 3; i++) {
            await Future.delayed(const Duration(milliseconds: 500));
            _addAnimationWidgets(false);
            provider.setCoinAnimation(false);
          }
        } else {
          _addAnimationWidgets(true);
          provider.setCoinAnimation(true);
        }
        await Future.delayed(const Duration(milliseconds: 150));
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  _addAnimationWidgets(bool isCoin) {
    final audioController = context.read<AudioController>();
    audioController.playSfx(Sounds.coin);
    OverlayEntry? overlayEntry = OverlayEntry(builder: (_) {
      return AtoBAnimation(
        startPosition:
            isCoin ? _coinStartOffset + const Offset(50, 10) : _starStartOffset,
        endPosition: isCoin ? _coinEndOffset : _starEndOffset,
        dxCurveAnimation: 0,
        dyCurveAnimation: 0,
        duration: const Duration(milliseconds: 800),
        opacity: 1,
        child: isCoin
            ? Image.asset(
                "assets/images/coin.png",
                height: 80,
                width: 80,
                fit: BoxFit.fill,
              )
            : Transform.scale(
                scale: 3.5,
                child: Lottie.asset(
                  'assets/images/star_animation.json',
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                  repeat: true,
                ),
              ),
      );
    });
    // Show Overlay
    final contexts = _coinStartKey.currentContext;
    if (contexts != null) {
      Overlay.of(contexts).insert(overlayEntry);
      Future.delayed(const Duration(milliseconds: 1000), () {
        overlayEntry?.remove();
        overlayEntry = null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var viewPadding = MediaQuery.of(context).viewPadding;
    final audioController = context.read<AudioController>();

    return PopScope(
      canPop: false,
      onPopInvoked: (didPop) {
        if (Platform.isAndroid) {
          showAnimateDialog(context, const QuiteDialog());
        }
      },
      child: Container(
        padding: Platform.isAndroid
            ? EdgeInsets.only(top: viewPadding.top != 0 ? viewPadding.top : 35)
            : null,
        color: const Color(0xff002a6a),
        child: Scaffold(
          appBar: gameAppBar(context,
              coinKey: _coinEndKey,
              starKey: _starEndKey,
              coinTap: () => openCustomDialog(context)),
          backgroundColor: Colors.transparent,
          // bottomNavigationBar: Container(height: 70),
          body: Container(
            width: size.width,
            height: size.height,
            decoration: const BoxDecoration(
              gradient: RadialGradient(
                tileMode: TileMode.clamp,
                center: Alignment.center,
                radius: 0.8,
                colors: [
                  Color(0xff0275bc),
                  Color(0xff002a6a),
                ],
              ),
            ),
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(height: 20),
                      SizedBox(
                        height: 2,
                        key: _starStartKey,
                      ),
                      Stack(
                        children: [
                          for (int i = 0; i < 3; i++)
                            Padding(
                              padding: EdgeInsets.only(
                                  left: i == 0 ? 0 : (i * 90),
                                  top: i == 1 ? 0 : 30),
                              child: Transform.scale(
                                scale: 2.5,
                                child: Lottie.asset(
                                  'assets/images/star_animation.json',
                                  width: 80,
                                  height: 80,
                                  fit: BoxFit.fill,
                                  repeat: true,
                                ),
                              ),
                            ),
                        ],
                      ),
                      Stack(
                        alignment: Alignment.center,
                        children: [
                          const Positioned(
                            top: 30,
                            child: Text(
                              'Level 23',
                              style: TextStyle(
                                fontSize: 30,
                                letterSpacing: 4,
                                fontWeight: FontWeight.w900,
                                color: Colors.yellow,
                                fontFamily: "Permanent Marker",
                                shadows: [
                                  Shadow(
                                    color: Colors.deepOrange,
                                    offset: Offset(1.0, 2.0),
                                    blurRadius: 0,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: AnimatedBuilder(
                              animation: _controller,
                              builder: (context, child) {
                                return Transform.rotate(
                                  angle: _controller.value * 200,
                                  child: Image.asset(
                                    "assets/images/sun_ray_bg_1.png",
                                    height: 400,
                                    width: 400,
                                    fit: BoxFit.fill,
                                    color: Colors.yellow,
                                  ),
                                );
                              },
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              padding: const EdgeInsets.all(30),
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.yellow,
                                    spreadRadius: 20,
                                    blurRadius: 70,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Image.asset(
                                "assets/images/w_bg.png",
                                key: _coinStartKey,
                                height: 150,
                                width: 150,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GradientShadowButton(
                            gradientColors: GameColors.orangeGradient,
                            onTap: () {
                              audioController.playSfx(Sounds.buttonTap);
                              context.pop();
                            },
                            child: Image.asset(
                              "assets/images/refresh_bg.png",
                              height: 30,
                              width: 30,
                              color: Colors.white,
                            ),
                          ),
                          GradientShadowButton(
                            text: "NEXT",
                            gradientColors: GameColors.greenGradient,
                            onTap: () {
                              showAnimateDialog(
                                  context, QuiteDialog(onQuit: () {}));
                              // context.pushReplacement('/dashBoardScreen');
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                if (showAnimation) ...[
                  Padding(
                    padding: const EdgeInsets.only(top: 80.0),
                    child:
                        Lottie.asset("assets/json/win_1.json", animate: true),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 80.0),
                    child:
                        Lottie.asset("assets/json/win_3.json", animate: true),
                  ),
                  for (int i = 0; i < 5; i++)
                    Lottie.asset("assets/json/win_2.json", animate: true),
                ],
                // Confetti(),
              ],
            ),
          ),
          bottomNavigationBar: Container(
            height: size.height / 14,
            width: double.infinity,
            decoration: const BoxDecoration(
              color: Color(0xff673323),
              border:
              Border(top: BorderSide(color: Color(0xff7b401e), width: 6)),
            ),
            // child: AppAds(adsType: AdsType.banner,),
          ),
        ),
      ),
    );
  }
}

void openCustomDialog(BuildContext context) {
  showGeneralDialog(
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: const Duration(milliseconds: 300),
      barrierDismissible: true,
      barrierLabel: '',
      context: context,
      pageBuilder: (context, animation1, animation2) {
        return const CoinBuyScreen(
          level: "Level ${"1"}",
        );
      });
}
