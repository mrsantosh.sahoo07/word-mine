import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../audio/audio_controller.dart';
import '../../audio/sounds.dart';
import '../../setting/setting_controller.dart';
import '../../utils/enums.dart';
class CoinWidget extends StatefulWidget {
  final Widget? child;
  final Function()? onTap;
  final WidgetType widgetType;

  const CoinWidget({
    super.key,
    this.child,
    this.onTap,
    this.widgetType = WidgetType.none,
  });

  @override
  State<CoinWidget> createState() => _CoinWidgetState();
}

class _CoinWidgetState extends State<CoinWidget> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    BaseProvider provider = getProvider();

    animationText(String text) => AnimatedBuilder(
      animation: provider.textAnimation,
      builder: (context, child) {
        return Transform.scale(
          scale: provider.textAnimation.value,
          child: child,
        );
      },
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: const TextStyle(
          fontSize: 16,
          color: Color(0xff8c3f21),
          fontFamily: "ArchiveBlack",
        ),
      ),
    );

    return GestureDetector(
      onTap: () {
        if (widget.onTap != null) {
          widget.onTap!();
        }
      },
      child: AnimatedBuilder(
        animation: provider.widgetAnimation,
        builder: (context, child) {
          return Transform.scale(
            scale: provider.widgetAnimation.value,
            child: Stack(
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 10, left: 18, top: 2),
                  padding: const EdgeInsets.all(1.5),
                  width: size.width / 5,
                  decoration: BoxDecoration(
                    color: const Color(0xffb67b52),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Container(
                    padding:
                    const EdgeInsets.symmetric(horizontal: 4, vertical: 1),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      gradient: const LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color(0xffffebbc),
                          Color(0xffe0c389),
                        ],
                      ),
                    ),
                    child: ValueListenableBuilder<int>(
                      valueListenable: getValueNotifier(context),
                      builder: (context, value, child) =>
                          animationText(value.toString()),
                    ),
                  ),
                ),
                Transform.scale(
                  scale: 1.1,
                  child: Image.asset(
                    getImagePath(),
                    height: 30,
                    width: 30,
                    fit: BoxFit.fill,
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  BaseProvider getProvider() {
    switch (widget.widgetType) {
      case WidgetType.coin:
        return context.watch<CoinProvider>();
      case WidgetType.live:
        return context.watch<LiveProvider>();
      case WidgetType.help:
        return context.watch<HelpProvider>();
      default:
        return context.watch<CoinProvider>();
    }
  }

  ValueNotifier<int> getValueNotifier(BuildContext context) {
    switch (widget.widgetType) {
      case WidgetType.coin:
        return context.watch<SettingsController>().coin;
      case WidgetType.live:
        return context.watch<SettingsController>().live;
      case WidgetType.help:
        return context.watch<SettingsController>().help;
      case WidgetType.level:
        return context.watch<SettingsController>().level;
      default:
        return context.watch<SettingsController>().coin;
    }
  }

  String getImagePath() {
    switch (widget.widgetType) {
      case WidgetType.coin:
        return "assets/images/coin3dd.png";
      case WidgetType.live:
        return "assets/images/heart.png";
      case WidgetType.help:
        return "assets/images/wHelper.png";
      case WidgetType.level:
        return "assets/images/level.png"; // Add an image path for level if needed
      default:
        return "assets/images/coin3dd.png";
    }
  }
}
abstract class BaseProvider with ChangeNotifier {
  late AnimationController _widgetController;
  late Animation<double> _widgetAnimation;
  late Animation<double> _textAnimation;

  BaseProvider(TickerProvider vsync) {
    _widgetController = AnimationController(
      vsync: vsync,
      duration: const Duration(milliseconds: 200),
    );

    _widgetAnimation = Tween<double>(begin: 0.95, end: 1.05)
        .chain(CurveTween(curve: Curves.elasticIn))
        .animate(_widgetController)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _widgetController.reverse();
        }
      });

    _textAnimation = Tween<double>(
      begin: 1.0,
      end: 1.3,
    ).animate(CurvedAnimation(
      parent: _widgetController,
      curve: Curves.bounceInOut,
    ));

    _widgetController.addListener(() {
      notifyListeners();
    });
  }

  Animation<double> get widgetAnimation => _widgetAnimation;

  Animation<double> get textAnimation => _textAnimation;

  @override
  void dispose() {
    _widgetController.dispose();
    super.dispose();
  }

  void startAnimation(BuildContext context,
      {WidgetType widgetType = WidgetType.none}) {
    _widgetController.forward();
    final audioController = context.read<AudioController>();
    final settingsController = context.read<SettingsController>();
    audioController.playSfx(Sounds.coin);
    switch (widgetType) {
      case WidgetType.coin:
        int coin = settingsController.coin.value;
        settingsController.setCoin(coin + 1);
        break;
      case WidgetType.live:
        int live = settingsController.live.value;
        settingsController.setLive(live + 1);
        break;
      case WidgetType.help:
        int help = settingsController.help.value;
        settingsController.setHelp(help + 1);
        break;
      case WidgetType.level:
      // Add logic for level if needed
        break;
      default:
        break;
    }
  }
}

class CoinProvider extends BaseProvider {
  CoinProvider(super.vsync);
}

class LiveProvider extends BaseProvider {
  LiveProvider(super.vsync);
}

class HelpProvider extends BaseProvider {
  HelpProvider(super.vsync);
}
