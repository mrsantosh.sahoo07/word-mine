import 'dart:io';

import 'package:flutter/material.dart' hide BoxDecoration, BoxShadow;
import 'package:flutter/services.dart';
import 'package:flutter_inset_shadow/flutter_inset_shadow.dart';
import 'package:go_router/go_router.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:intl/intl.dart' as D;
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wordly/games/word_game/setting/setting_dialog.dart';
import '../../../../animations/custom_clipeer.dart';
import '../../../../animations/scal_animation_button.dart';
import '../../audio/audio_controller.dart';
import '../../audio/sounds.dart';

class StartScreen extends StatefulWidget {
  const StartScreen({super.key});

  @override
  State<StartScreen> createState() => _SplashLoaderState();
}

class _SplashLoaderState extends State<StartScreen> {
  @override
  void initState() {
    super.initState();
  }

  bool isPress = true;

  @override
  Widget build(BuildContext context) {
    var colors = const Color(0xffe7ecef);
    var size = MediaQuery.of(context).size;
    return PopScope(
      canPop: false,
      onPopInvoked: (didPop) {
        showAnimateDialog(context, QuiteDialog(
          onQuit: () {
            if (Platform.isAndroid) {
              SystemNavigator.pop();
            } else if (Platform.isIOS) {
              exit(0);
            }
          },
        ));
      },
      child: Scaffold(
        backgroundColor: colors,
        body: Stack(
          children: [
            /// background image
            Image.asset(
              "assets/images/word_mine_play.jpeg",
              height: size.height,
              width: size.width,
              fit: BoxFit.fitHeight,
            ),

            /// Setting button
            Align(
              alignment: Alignment.bottomLeft,
              child: SafeArea(
                child: ScaleAnimationButton(
                  onTap: () async {
                    //   final InAppReview inAppReview = InAppReview.instance;
                    //   if (await inAppReview.isAvailable()) {
                    //     inAppReview.requestReview();
                    // }
                    showSettingDialog(context);
                    },
                  child: Container(
                    margin: const EdgeInsets.only(left: 10),
                    decoration: BoxDecoration(
                      color: const Color(0xff002a6a),
                      border: Border.all(color: Colors.pink.shade900, width: 2),
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: Stack(
                      children: [
                        Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              boxShadow: const [
                                BoxShadow(
                                  color: Color(0xff0275bc),
                                  spreadRadius: -5,
                                  blurRadius: 20,
                                ),
                              ],
                            ),
                            child: const Icon(
                              Icons.settings,
                              size: 35,
                              color: Colors.white,
                            )),
                      ],
                    ),
                  ),
                ),
              ),
            ),

            /// play button
            SafeArea(
              child: Align(
                alignment: const Alignment(0, 0.8),
                child: ScaleAnimationButton(
                  onTap: () {
                    // final audioController = context.read<AudioController>();
                    // audioController.playSfx(Sounds.buttonTap);
                    context.push('/dashBoardScreen');
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.deepOrange,
                      border: Border.all(color: Colors.pink.shade900, width: 2),
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: Stack(
                      children: [
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 50, vertical: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            boxShadow: const [
                              BoxShadow(
                                color: Colors.orange,
                                spreadRadius: -5,
                                blurRadius: 20,
                              ),
                            ],
                          ),
                          child: const Text(
                            "PLAY",
                            style: TextStyle(
                                fontSize: 30,
                                letterSpacing: 6,
                                color: Colors.white,
                                fontFamily: "Permanent Marker",
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class GameSlider extends StatefulWidget {
  const GameSlider({super.key});

  @override
  State<GameSlider> createState() => _GameSliderState();
}

class _GameSliderState extends State<GameSlider> {
  double valueData = 0.5;

  @override
  Widget build(BuildContext context) {
    return GradientSlider(
        sliderRadius: 10,
        height: 10,
        gradient: const LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment(-0.8, -0.3),
          stops: [0.0, 0.5, 0.5, 1],
          colors: [
            Color(0xff2eb7f9),
            Color(0xff2eb7f9),
            Color(0xff07608b),
            Color(0xff07608b),
            // Color(0xfffed81d),
            // Color(0xfffed81d),
            // Color(0xffd70a67),
            // Color(0xffd70a67),
          ],
          tileMode: TileMode.repeated,
        ),
        sliderValue: valueData,
        onChanged: (v) => setState(() => valueData = v),
        thumbColor: Colors.blueAccent);
  }
}

class GameSwitch extends StatefulWidget {
  final Function(bool) onchange;
  final bool switchValue;

  const GameSwitch(
      {super.key, required this.onchange, this.switchValue = false});

  @override
  State<GameSwitch> createState() => _GameSwitchState();
}

class _GameSwitchState extends State<GameSwitch> {
  late bool isPress;

  Offset distance = const Offset(10, 10);
  double blur = 5.0;

  @override
  void initState() {
    isPress = widget.switchValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() => isPress = !isPress);
        widget.onchange(isPress);
      },
      child: Container(
        height: 40,
        width: 140,
        decoration: BoxDecoration(
          color: const Color(0xffbb480b).withOpacity(0.7),
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(
              blurRadius: blur,
              color: const Color(0xffbb480b),
              offset: -distance,
              inset: true,
            ),
            BoxShadow(
              blurRadius: blur,
              color: const Color(0xffbb480b),
              offset: distance,
              inset: true,
            ),
          ],
        ),
        child: Stack(
          children: [
            AnimatedAlign(
              alignment:
                  !isPress ? Alignment.centerLeft : Alignment.centerRight,
              duration: const Duration(milliseconds: 500),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: Text(
                  !isPress ? "ON" : "OFF",
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w900,
                      color: !isPress
                          ? const Color(0xffcbdc2f)
                          : const Color(0xffffe984)),
                ),
              ),
            ),
            AnimatedAlign(
              alignment: isPress ? Alignment.centerLeft : Alignment.centerRight,
              duration: const Duration(milliseconds: 500),
              child: Container(
                height: 50,
                width: 65,
                margin: const EdgeInsets.all(4),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: const Alignment(-0.8, 0),
                    stops: const [0.0, 0.5, 0.5, 1],
                    colors: [
                      if (isPress) ...[
                        const Color(0xff2eb7f9),
                        const Color(0xff2eb7f9),
                        const Color(0xff07608b),
                        const Color(0xff07608b),
                      ] else ...[
                        const Color(0xff50be0d),
                        const Color(0xff50be0d),
                        const Color(0xFF149623),
                        const Color(0xFF149623),
                      ]
                    ],
                    tileMode: TileMode.repeated,
                  ),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Stack(
                  children: [
                    Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 30, vertical: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: [
                          BoxShadow(
                            color: (isPress
                                    ? const Color(0xff0275bc)
                                    : const Color(0xff50be0d))
                                .withOpacity(0.5),
                            spreadRadius: -5,
                            blurRadius: 10,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ShadowButton extends StatefulWidget {
  const ShadowButton({super.key});

  @override
  State<ShadowButton> createState() => _ShadowButtonState();
}

class _ShadowButtonState extends State<ShadowButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.orange,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          boxShadow: const [
            BoxShadow(
              color: Colors.amber,
              spreadRadius: -5,
              blurRadius: 20,
            ),
          ],
        ),
      ),
    );
  }
}

class LiteRollingSwitch extends StatefulWidget {
  @required
  final bool value;
  final double width;

  @required
  final Function(bool) onChanged;
  final String textOff;
  final Color textOffColor;
  final String textOn;
  final Color textOnColor;
  final Color colorOn;
  final Color colorOff;
  final double textSize;
  final Duration animationDuration;
  final IconData iconOn;
  final IconData iconOff;
  final Function onTap;
  final Function onDoubleTap;
  final Function onSwipe;

  const LiteRollingSwitch({
    super.key,
    this.value = false,
    this.width = 130,
    this.textOff = "OFF",
    this.textOn = "ON",
    this.textSize = 14.0,
    this.colorOn = Colors.green,
    this.colorOff = Colors.red,
    this.iconOff = Icons.flag,
    this.iconOn = Icons.check,
    this.animationDuration = const Duration(milliseconds: 600),
    this.textOffColor = Colors.white,
    this.textOnColor = Colors.black,
    required this.onTap,
    required this.onDoubleTap,
    required this.onSwipe,
    required this.onChanged,
  });

  @override
  _RollingSwitchState createState() => _RollingSwitchState();
}

class _RollingSwitchState extends State<LiteRollingSwitch>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController;
  late Animation<double> animation;
  late bool turnState;

  double value = 0.0;

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        vsync: this,
        lowerBound: 0.0,
        upperBound: 1.0,
        duration: widget.animationDuration);
    animation =
        CurvedAnimation(parent: animationController, curve: Curves.easeInOut);
    animationController.addListener(() {
      setState(() {
        value = animation.value;
      });
    });
    turnState = widget.value;

    // Executes a function only one time after the layout is completed.
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        if (turnState) {
          animationController.forward();
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    //Color transition animation
    Color? transitionColor = Color.lerp(widget.colorOff, widget.colorOn, value);

    return GestureDetector(
      onDoubleTap: () {
        _action();
        widget.onDoubleTap();
      },
      onTap: () {
        _action();
        widget.onTap();
      },
      onPanEnd: (details) {
        _action();
        widget.onSwipe();
      },
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 3, vertical: 2),
        width: widget.width,
        decoration: BoxDecoration(
            color: transitionColor, borderRadius: BorderRadius.circular(5)),
        child: Stack(
          children: <Widget>[
            Transform.translate(
              offset: isRTL(context)
                  ? Offset(-10 * value, 0)
                  : Offset(10 * value, 0), //original
              child: Opacity(
                opacity: (1 - value).clamp(0.0, 1.0),
                child: Container(
                  padding: isRTL(context)
                      ? const EdgeInsets.only(left: 10)
                      : const EdgeInsets.only(right: 10),
                  alignment: isRTL(context)
                      ? Alignment.centerLeft
                      : Alignment.centerRight,
                  height: 30,
                  child: Text(
                    widget.textOff,
                    style: TextStyle(
                        color: widget.textOffColor,
                        fontWeight: FontWeight.bold,
                        fontSize: widget.textSize),
                  ),
                ),
              ),
            ),
            Transform.translate(
              offset: isRTL(context)
                  ? Offset(-10 * (1 - value), 0)
                  : Offset(10 * (1 - value), 0), //original
              child: Opacity(
                opacity: value.clamp(0.0, 1.0),
                child: Container(
                  padding: isRTL(context)
                      ? const EdgeInsets.only(right: 5)
                      : const EdgeInsets.only(left: 5),
                  alignment: isRTL(context)
                      ? Alignment.centerRight
                      : Alignment.centerLeft,
                  height: 30,
                  child: Text(
                    widget.textOn,
                    style: TextStyle(
                        color: widget.textOnColor,
                        fontWeight: FontWeight.bold,
                        fontSize: widget.textSize),
                  ),
                ),
              ),
            ),
            Transform.translate(
              offset: isRTL(context)
                  ? Offset((-widget.width + 50) * value, 0)
                  : Offset((widget.width - 50) * value, 0),
              child: Transform.rotate(
                angle: 0,
                child: Container(
                  height: 30,
                  width: 50,
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle, color: Colors.white),
                  child: Stack(
                    children: <Widget>[
                      Center(
                        child: Opacity(
                            opacity: (1 - value).clamp(0.0, 1.0),
                            child: const ShadowButton()),
                      ),
                      Center(
                        child: Opacity(
                          opacity: value.clamp(0.0, 1.0),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.orange,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                boxShadow: const [
                                  BoxShadow(
                                    color: Colors.amber,
                                    spreadRadius: -5,
                                    blurRadius: 20,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _action() {
    _determine(changeState: true);
  }

  _determine({bool changeState = false}) {
    setState(() {
      if (changeState) turnState = !turnState;
      (turnState)
          ? animationController.forward()
          : animationController.reverse();

      widget.onChanged(turnState);
    });
  }
}

bool isRTL(BuildContext context) {
  return D.Bidi.isRtlLanguage(Localizations.localeOf(context).languageCode);
}

class GradientSlider extends StatelessWidget {
  final double sliderRadius;
  final double height;
  final Gradient gradient;
  final Color thumbColor;
  final double sliderValue;
  final ValueChanged<double>? onChanged;

  const GradientSlider(
      {super.key,
      required this.sliderRadius,
      required this.gradient,
      required this.sliderValue,
      this.onChanged,
      this.height = 20,
      required this.thumbColor});

  @override
  Widget build(BuildContext context) {
    Offset distance = const Offset(10, 10);
    double blur = 5.0;
    return LayoutBuilder(builder: (context, constraints) {
      return IntrinsicHeight(
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              width: constraints.maxWidth,
              height: height,
              decoration: BoxDecoration(
                color: const Color(0xffbb480b).withOpacity(0.7),
                boxShadow: [
                  BoxShadow(
                    blurRadius: blur,
                    color: const Color(0xffbb480b),
                    offset: -distance,
                    inset: true,
                  ),
                  BoxShadow(
                    blurRadius: blur,
                    color: const Color(0xffbb480b),
                    offset: distance,
                    inset: true,
                  ),
                ],
                borderRadius: BorderRadius.circular(sliderRadius),
              ),
            ),

            ShaderMask(
              shaderCallback: (bounds) {
                return gradient.createShader(bounds);
              },
              child: Container(
                height: height,
                width: constraints.maxWidth,
                alignment: Alignment.centerLeft,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(sliderRadius),
                  ),
                  width:
                      (constraints.maxWidth - 2 * sliderRadius) * sliderValue +
                          2 * sliderRadius,
                ),
              ),
            ),

            //build active track
            SliderTheme(
                data: SliderThemeData(
                  trackHeight: 10,
                  overlayShape: const RoundSliderOverlayShape(overlayRadius: 0),
                  thumbShape: const SliderThumbShape(),
                  // thumbShape: RoundSliderThumbShape(
                  //   enabledThumbRadius: sliderRadius,
                  //   elevation: 0,
                  //   pressedElevation: 0,
                  // ),
                  activeTrackColor: Colors.transparent,
                  thumbColor: thumbColor,
                  inactiveTrackColor: Colors.transparent,
                ),
                child: Slider(
                  value: sliderValue,
                  onChanged: onChanged,
                )),
          ],
        ),
      );
    });
  }
}

class SliderThumbShape extends SliderComponentShape {
  const SliderThumbShape({
    this.enabledThumbRadius = 10.0,
    this.disabledThumbRadius,
    this.elevation = 1.0,
    this.pressedElevation = 6.0,
  });

  final double enabledThumbRadius;
  final double? disabledThumbRadius;

  double get _disabledThumbRadius => disabledThumbRadius ?? enabledThumbRadius;
  final double elevation;
  final double pressedElevation;

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size.fromRadius(
        isEnabled == true ? enabledThumbRadius : _disabledThumbRadius);
  }

  @override
  void paint(
    PaintingContext context,
    Offset center, {
    Animation<double>? activationAnimation,
    required Animation<double>? enableAnimation,
    bool? isDiscrete,
    TextPainter? labelPainter,
    RenderBox? parentBox,
    required SliderThemeData? sliderTheme,
    TextDirection? textDirection,
    double? value,
    double? textScaleFactor,
    Size? sizeWithOverflow,
  }) {
    assert(enableAnimation != null);
    assert(sliderTheme != null);
    assert(sliderTheme?.disabledThumbColor != null);
    assert(sliderTheme?.thumbColor != null);
    assert(!sizeWithOverflow!.isEmpty);

    final Canvas canvas = context.canvas;
    final Tween<double> radiusTween = Tween<double>(
      begin: _disabledThumbRadius,
      end: enabledThumbRadius,
    );
    final double radius = radiusTween.evaluate(enableAnimation!);
    Paint paint = Paint()..color = const Color(0xff07608b);
    paint.strokeWidth = 16;
    paint.style = PaintingStyle.stroke;
    canvas.drawCircle(
      center,
      radius,
      paint,
    );

    Paint paint2 = Paint()..color = const Color(0xff2eb7f9);
    paint2.style = PaintingStyle.fill;
    canvas.drawCircle(
      center,
      8,
      paint2,
    );
  }
}

class AppSliderShape extends SliderComponentShape {
  final double thumbRadius;

  const AppSliderShape({required this.thumbRadius});

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size.fromRadius(thumbRadius);
  }

  @override
  void paint(
    PaintingContext context,
    Offset center, {
    required Animation<double> activationAnimation,
    required Animation<double> enableAnimation,
    required bool isDiscrete,
    required TextPainter labelPainter,
    required RenderBox parentBox,
    required SliderThemeData sliderTheme,
    required TextDirection textDirection,
    required double value,
    required double textScaleFactor,
    required Size sizeWithOverflow,
  }) {
    final Canvas canvas = context.canvas;

    final paint = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.white;

    // draw icon with text painter
    const iconData = Icons.drag_handle;
    final TextPainter textPainter =
        TextPainter(textDirection: TextDirection.rtl);
    textPainter.text = TextSpan(
        text: String.fromCharCode(iconData.codePoint),
        style: TextStyle(
          fontSize: thumbRadius * 2,
          fontFamily: iconData.fontFamily,
          color: sliderTheme.thumbColor,
        ));
    textPainter.layout();

    final Offset textCenter = Offset(center.dx - (textPainter.width / 2),
        center.dy - (textPainter.height / 2));
    const cornerRadius = 4.0;

    // draw the background shape here..
    canvas.drawRRect(
      RRect.fromRectXY(Rect.fromCenter(center: center, width: 30, height: 20),
          cornerRadius, cornerRadius),
      paint,
    );

    textPainter.paint(canvas, textCenter);
  }
}

// class DottedDiamondShapeBorder extends ShapeBorder {
//   const DottedDiamondShapeBorder();
//
//   @override
//   EdgeInsetsGeometry get dimensions => EdgeInsets.all(0);
//
//   @override
//   Path getInnerPath(Rect rect, {TextDirection? textDirection}) =>
//       getOuterPath(rect, textDirection: textDirection);
//
//   @override
//   Path getOuterPath(Rect rect, {TextDirection? textDirection}) {
//     return Path()
//       ..moveTo(rect.centerLeft.dx, rect.centerLeft.dy)
//       ..lineTo(rect.topCenter.dx, rect.topCenter.dy)
//       ..lineTo(rect.centerRight.dx, rect.centerRight.dy)
//       ..lineTo(rect.bottomCenter.dx, rect.bottomCenter.dy)
//       ..close();
//   }
//
//   @override
//   void paint(Canvas canvas, Rect rect, {TextDirection? textDirection}) {
//     final Paint paint = Paint()
//       ..color = Colors.black
//       ..style = PaintingStyle.stroke
//       ..strokeWidth = 2.0;
//
//     final double dashWidth = 5;
//     final double dashSpace = 3;
//     double currentX = rect.centerLeft.dx;
//     double currentY = rect.centerLeft.dy;
//
//     final Path path = Path();
//     path.moveTo(currentX, currentY);
//
//     while (currentX < rect.topCenter.dx) {
//       currentX += dashWidth;
//       path.lineTo(currentX, currentY);
//       currentX += dashSpace;
//       path.moveTo(currentX, currentY);
//     }
//
//     currentY = rect.topCenter.dy;
//     while (currentY < rect.centerRight.dy) {
//       currentY += dashWidth;
//       path.lineTo(currentX, currentY);
//       currentY += dashSpace;
//       path.moveTo(currentX, currentY);
//     }
//
//     currentX = rect.centerRight.dx;
//     while (currentX > rect.bottomCenter.dx) {
//       currentX -= dashWidth;
//       path.lineTo(currentX, currentY);
//       currentX -= dashSpace;
//       path.moveTo(currentX, currentY);
//     }
//
//     currentY = rect.bottomCenter.dy;
//     while (currentY > rect.centerLeft.dy) {
//       currentY -= dashWidth;
//       path.lineTo(currentX, currentY);
//       currentY -= dashSpace;
//       path.moveTo(currentX, currentY);
//     }
//
//     canvas.drawPath(path, paint);
//   }
//
//   @override
//   ShapeBorder scale(double t) => this;
// }

// Column(
//   children: [
//
//     Container(
//       margin: const EdgeInsets.symmetric(horizontal: 20),
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(50),
//         border: Border.all(color: Colors.white, width: 6),
//       ),
//       child: Container(
//         margin: EdgeInsets.only(
//           // right: ((size.width)).abs(),
//         ),
//         height: 30,
//         decoration: BoxDecoration(
//           // color: Colors.deepOrange,
//             // color: colors,
//             gradient:  const LinearGradient(
//               begin: Alignment.topLeft,
//               end: Alignment(-0.8, 0),
//               stops: [0.0, 0.5, 0.5, 1],
//               colors: [
//                   Colors.red,
//                   Colors.red,
//                   Colors.orange,
//                   Colors.orange,
//               ],
//               tileMode: TileMode.repeated,
//             ),
//           borderRadius: BorderRadius.circular(50),
//         ),
//         child: Stack(
//           children: [
//             Container(
//               padding: const EdgeInsets.symmetric(
//                   horizontal: 30, vertical: 5),
//               decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(15),
//                 boxShadow:  [
//                   BoxShadow(
//                     color: Colors.orange.shade400,
//                     spreadRadius: -5,
//                     blurRadius: 10,
//                   ),
//                 ],
//               ),
//             ),
//             Positioned(
//               right: 0,
//               child: Transform.scale(
//                 scale: 1.5,
//                 child: Image.asset(
//                   "assets/images/candy_loader.png",
//                   height: 30,
//                   width: 30,
//                   fit: BoxFit.fill,
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     )
//   ],
// )

///

// Container(
//   decoration: BoxDecoration(
//     gradient: LinearGradient(
//       begin: Alignment(-0.9,-0.7),
//       end: Alignment(-0.8, -0.8),
//       ///
//       // begin: Alignment(-0.9,-0.8),
//       // end: Alignment(-0.8, -0.8),
//       ///
//       // begin: Alignment.topLeft,
//       // end: Alignment(-0.5, -0.8),
//       ///
//       // begin: Alignment(0,0.9),
//       // end: Alignment(0,0.8),
//       stops: [0.0, 0.5, 0.5, 1],
//       colors: [
//         Colors.red,
//         Colors.red,
//         Colors.orange,
//         Colors.orange,
//
//       ],
//       tileMode: TileMode.repeated,
//     ),
//   ),
//   height: size.height,
//   width: size.width,
// )
