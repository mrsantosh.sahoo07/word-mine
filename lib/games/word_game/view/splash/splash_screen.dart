import 'dart:async';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:wordly/games/word_game/utils/ui_util.dart';

import '../../../../word-mine/view/word_game.dart';

class SplashLoader extends StatefulWidget {
  const SplashLoader({super.key});

  @override
  State<SplashLoader> createState() => _SplashLoaderState();
}

class _SplashLoaderState extends State<SplashLoader>
    with TickerProviderStateMixin {
  late AnimationController loaderController;
  late Animation<double> loaderAnimation;

  @override
  void initState() {
    loaderController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    loaderAnimation = Tween(begin: 75.0, end: UiUtils.size.size.width)
        .animate(loaderController);
    loaderController.forward();
    Timer(const Duration(milliseconds: 1500),
        () => context.pushReplacementNamed("start"));
    super.initState();
  }

  @override
  void deactivate() {
    loaderController.dispose();
    loaderAnimation.removeListener(() {});
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Image.asset(
            "assets/images/word_mine_loading.jpeg",
            height: size.height,
            width: size.width,
            fit: BoxFit.fitHeight,
          ),
          SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                AnimatedBuilder(
                    animation: loaderController,
                    builder: (buildContext, child) {
                      return
                        Container(
                        margin: const EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          border: Border.all(color: Colors.white, width: 6),
                        ),
                        child: Container(
                          margin: EdgeInsets.only(
                            right:
                                ((size.width) - (loaderAnimation.value)).abs(),
                          ),
                          height: 30,
                          decoration: BoxDecoration(
                            color: Colors.pink,
                            borderRadius: BorderRadius.circular(50),
                            gradient: const LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment(-0.8, 0),
                              stops: [0.0, 0.5, 0.5, 1],
                              colors: [
                                Colors.amber,
                                Colors.amber,
                                Colors.pink,
                                Colors.pink,
                              ],
                              tileMode: TileMode.repeated,
                            ),
                          ),
                          child: Stack(
                            children: [
                              Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 30, vertical: 5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.amber.withOpacity(0.8),
                                      spreadRadius: -5,
                                      blurRadius: 10,
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                right: 0,
                                child: Transform.scale(
                                  scale: 1.5,
                                  child: Transform.rotate(
                                    angle: loaderController.value * 10,
                                    child: Image.asset(
                                      "assets/images/candy_loader.png",
                                      height: 30,
                                      width: 30,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }),
                const Text(
                  "Loading...",
                  style: TextStyle(
                      fontSize: 40,
                      color: Colors.white,
                      fontWeight: FontWeight.w900),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
