import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:wordly/games/word_game/setting/setting_controller.dart';
import 'package:wordly/games/word_game/setting/setting_dialog.dart';

import '../view/dashboard/dashboard_screen.dart';

class SettingScreen extends StatefulWidget {
  const SettingScreen({super.key});

  @override
  State<SettingScreen> createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen>
    with TickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 2), // Adjust duration as needed
      vsync: this,
    )..repeat();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final settings = context.read<SettingsController>();
    return Scaffold(
      body: Container(
        height: size.height,
        width: size.width,
        decoration: const BoxDecoration(
          gradient: RadialGradient(
            tileMode: TileMode.clamp,
            center: Alignment.center,
            radius: 0.8,
            colors: [
              Color(0xff175395),
              Color(0xff0d2655),
            ],
          ),
        ),
        child: Column(
          children: [
            Container(
              decoration: const BoxDecoration(
                gradient: RadialGradient(
                  tileMode: TileMode.clamp,
                  center: Alignment.bottomCenter,
                  radius: 1.5,
                  colors: [
                    Color(0xff015898),
                    Color(0xff013382),
                  ],
                ),
              ),
              height: size.height / 7.5,
              width: double.infinity,
              alignment: const Alignment(0.0, 0.8),
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0, bottom: 8),
                      child: GradientShadowButton(
                        strokeWidth: 0.4,
                        borderRadius: 8,
                        height: 40,
                        width: 40,

                        padding: const EdgeInsets.all(8),
                        gradientColors: GameColors.redGradient,
                        onTap: () {
                          context.pop();
                        },
                        child: Image.asset('assets/images/close_icon.png'),
                      ),
                    ),
                  ),
                  const SGText(
                    "Setting",
                    fontSize: 35,
                    shadowWith: 2,
                    letterSpacing: 1,
                  ),
                ],
              ),
            ),
            Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  tileMode: TileMode.clamp,
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xffb56540),
                    Color(0xff995337),
                    Color(0xff673926),
                  ],
                ),
              ),
              height: 10,
              width: double.infinity,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: AnimationLimiter(
                  child: ListView(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    padding: EdgeInsets.only(
                        top: 16,
                        bottom: 180,
                        left: size.width / 8,
                        right: size.width / 8),
                    children: [
                      const SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GradientShadowButton(
                            strokeWidth: 0.5,
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            gradientColors: GameColors.greenGradient,
                            onTap: () {},
                            child: const Icon(Icons.vibration,
                                color: Colors.white),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          ValueListenableBuilder<bool>(
                              valueListenable:
                                  context.watch<SettingsController>().musicOn,
                              builder: (context, musicOn, child) =>
                                  GradientShadowButton(
                                    strokeWidth: 0.5,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 10),
                                    gradientColors: musicOn
                                        ? GameColors.greenGradient
                                        : GameColors.redGradient,
                                    onTap: () {
                                      settings.toggleMusicOn();
                                    },
                                    child: Icon(
                                        musicOn
                                            ? Icons.music_note
                                            : Icons.music_off,
                                        color: Colors.white),
                                  )),
                          const SizedBox(width: 10),
                          ValueListenableBuilder<bool>(
                            valueListenable:
                                context.watch<SettingsController>().soundsOn,
                            builder: (context, soundsOn, child) =>
                                GradientShadowButton(
                              strokeWidth: 0.5,
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 10),
                              gradientColors: soundsOn
                                  ? GameColors.greenGradient
                                  : GameColors.redGradient,
                              onTap: () {
                                settings.toggleSoundsOn();
                              },
                              child: Icon(
                                  soundsOn ? Icons.volume_up : Icons.volume_off,
                                  color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 15),
                      // GradientShadowButton(
                      //   text: "Language",
                      //   fontSize: 25,
                      //   strokeWidth: 0.7,
                      //   shadowWith: 2,
                      //   padding: const EdgeInsets.symmetric(vertical: 8),
                      //   textGradiantColors: GameColors.textGreenGradient,
                      //   shadowColors: const Color(0xff1b6b00),
                      //   gradientColors: GameColors.greenGradient,
                      //   onTap: () {},
                      // ),
                      // const SizedBox(height: 15),
                      // GradientShadowButton(
                      //   text: "Support",
                      //   fontSize: 25,
                      //   strokeWidth: 0.7,
                      //   shadowWith: 2,
                      //   padding: const EdgeInsets.symmetric(vertical: 8),
                      //   textGradiantColors: GameColors.textGreenGradient,
                      //   shadowColors: const Color(0xff1b6b00),
                      //   gradientColors: GameColors.orangeGradient,
                      //   onTap: () {},
                      // ),
                      // const SizedBox(height: 15),
                      // GradientShadowButton(
                      //   text: "Restore",
                      //   fontSize: 25,
                      //   strokeWidth: 0.7,
                      //   shadowWith: 2,
                      //   padding: const EdgeInsets.symmetric(vertical: 8),
                      //   textGradiantColors: GameColors.textGreenGradient,
                      //   shadowColors: const Color(0xff1b6b00),
                      //   gradientColors: GameColors.greenGradient,
                      //   onTap: () {
                      //     context.pop();
                      //   },
                      // ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: const ButtonNavigationAdsWidgets(),
    );
  }
}
