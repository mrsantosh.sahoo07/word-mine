import 'dart:convert';

// import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wordly/word-mine/view/word_game.dart';

import 'settings_persistence.dart';

class LocalStorageSettingsPersistence extends SettingsPersistence {
  final Future<SharedPreferences> instanceFuture =
      SharedPreferences.getInstance();

  // final  instanceFuture =
  // GetStorage("wordmine");
  @override
  Future<bool> getMusicOn() async {
    final prefs = await instanceFuture;
    // return instanceFuture.read('musicOn') ?? true;
    return prefs.getBool('musicOn') ?? true;
  }

  @override
  Future<bool> getMuted({required bool defaultValue}) async {
    final prefs = await instanceFuture;
    // return instanceFuture.read('mute') ?? defaultValue;
    return prefs.getBool('mute') ?? defaultValue;
  }

  @override
  Future<String> getPlayerName() async {
    final prefs = await instanceFuture;
    // return instanceFuture.read('playerName') ?? 'Player';
    return prefs.getString('playerName') ?? 'Player';
  }

  @override
  Future<bool> getSoundsOn() async {
    final prefs = await instanceFuture;
    // return instanceFuture.read('soundsOn') ?? true;
    return prefs.getBool('soundsOn') ?? true;
  }

  @override
  Future<void> saveMusicOn(bool value) async {
    final prefs = await instanceFuture;
    await prefs.setBool('musicOn', value);
    // await instanceFuture.write('musicOn', value);
  }

  @override
  Future<void> saveMuted(bool value) async {
    final prefs = await instanceFuture;
    // await instanceFuture.write('mute', value);
    await prefs.setBool('mute', value);
  }

  @override
  Future<void> savePlayerName(String value) async {
    final prefs = await instanceFuture;
    // await prefs.setString('playerName', value);
    await prefs.setString('playerName', value);
  }

  @override
  Future<void> saveSoundsOn(bool value) async {
    final prefs = await instanceFuture;
    await prefs.setBool('soundsOn', value);
  }

  @override
  Future<int> getCoin() async {
    final prefs = await instanceFuture;
    return prefs.getInt('coin') ?? 0;
  }

  @override
  Future<int> getLevel() async {
    final prefs = await instanceFuture;
    return prefs.getInt('star') ?? 0;
  }

  @override
  Future<void> saveCoin(int value) async {
    final prefs = await instanceFuture;
    await prefs.setInt('coin', value);
  }

  @override
  Future<void> saveLevel(int value) async {
    final prefs = await instanceFuture;
    await prefs.setInt('star', value);
  }

  @override
  Future<void> saveGameLevel(List<WordLevelBox> value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> jsonStringList =
        value.map((item) => json.encode(item.toJson())).toList();
    await prefs.setStringList('gameLevel', jsonStringList);
  }

  @override
  Future<List<WordLevelBox>> getGameLevel() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String>? jsonStringList = prefs.getStringList('gameLevel');
    if (jsonStringList == null) {
      return [];
    }
    return jsonStringList
        .map((item) => WordLevelBox.fromJson(json.decode(item)))
        .toList();
  }

  @override
  Future<int> getHelp() async {
    final prefs = await instanceFuture;
    return prefs.getInt('help') ?? 0;
  }

  @override
  Future<int> getLive() async {
    final prefs = await instanceFuture;
    return prefs.getInt('live') ?? 0;
  }

  @override
  Future<void> saveHelp(int value) async {
    final prefs = await instanceFuture;
    await prefs.setInt('help', value);
  }

  @override
  Future<void> saveLive(int value) async {
    final prefs = await instanceFuture;
    await prefs.setInt('live', value);
  }
}


/*import 'dart:convert';

import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wordly/word-mine/view/word_game.dart';

import 'settings_persistence.dart';

class LocalStorageSettingsPersistence extends SettingsPersistence {
  // final Future<SharedPreferences> instanceFuture =
  //     SharedPreferences.getInstance();

  final  instanceFuture =
  GetStorage();
  @override
  Future<bool> getMusicOn() async {
    // final prefs =  instanceFuture;
    return instanceFuture.read('musicOn') ?? true;
    // return prefs.getBool('musicOn') ?? true;
  }

  @override
  Future<bool> getMuted({required bool defaultValue}) async {
    // final prefs = await instanceFuture;
    return instanceFuture.read('mute') ?? defaultValue;
    // return prefs.getBool('mute') ?? defaultValue;
  }

  @override
  Future<String> getPlayerName() async {
    // final prefs = await instanceFuture;
    return instanceFuture.read('playerName') ?? 'Player';
    // return prefs.getString('playerName') ?? 'Player';
  }

  @override
  Future<bool> getSoundsOn() async {
    // final prefs = await instanceFuture;
    return instanceFuture.read('soundsOn') ?? true;
    // return prefs.getBool('soundsOn') ?? true;
  }

  @override
  Future<void> saveMusicOn(bool value) async {
    // final prefs = await instanceFuture;
    // await prefs.setBool('musicOn', value);
    await instanceFuture.write('musicOn', value);
  }

  @override
  Future<void> saveMuted(bool value) async {
    // final prefs = await instanceFuture;
    await instanceFuture.write('mute', value);
    // await prefs.setBool('mute', value);
  }

  @override
  Future<void> savePlayerName(String value) async {
    // final prefs = await instanceFuture;
    await instanceFuture.write('playerName', value);
    // await prefs.setString('playerName', value);
  }

  @override
  Future<void> saveSoundsOn(bool value) async {
    // final prefs = await instanceFuture;
    await instanceFuture.write('soundsOn', value);
    // await prefs.setBool('soundsOn', value);
  }

  @override
  Future<int> getCoin() async {
    // final prefs = await instanceFuture;
    return instanceFuture.read('coin') ?? 0;
    // return prefs.getInt('coin') ?? 0;
  }

  @override
  Future<int> getLevel() async {
    // final prefs = await instanceFuture;
    return instanceFuture.read('star') ?? 0;
    // return prefs.getInt('star') ?? 0;
  }

  @override
  Future<void> saveCoin(int value) async {
    // final prefs = await instanceFuture;
    await instanceFuture.write('coin', value);
    // await prefs.setInt('coin', value);
  }

  @override
  Future<void> saveLevel(int value) async {
    // final prefs = await instanceFuture;
    await instanceFuture.write('star', value);
    // await prefs.setInt('star', value);
  }

  @override
  Future<void> saveGameLevel(List<WordLevelBox> value) async {
    // final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> jsonStringList =
        value.map((item) => json.encode(item.toJson())).toList();
    await instanceFuture.write('gameLevel', value);
    // await prefs.setStringList('gameLevel', jsonStringList);
  }

  @override
  Future<List<WordLevelBox>> getGameLevel() async {
    // final SharedPreferences prefs = await SharedPreferences.getInstance();
    // List<String> jsonStringList = instanceFuture.read('gameLevel');
   return instanceFuture.read('gameLevel');
    // if (jsonStringList == null) {
    //   return [];
    // }
    // return jsonStringList
    //     .map((item) => WordLevelBox.fromJson(json.decode(item)))
    //     .toList();
  }

  @override
  Future<int> getHelp() async {
    // final prefs = await instanceFuture;
    return instanceFuture.read('help') ?? 0;
    // return prefs.getInt('help') ?? 0;
  }

  @override
  Future<int> getLive() async {
    // final prefs = await instanceFuture;
    return instanceFuture.read('live') ?? 0;
    // return prefs.getInt('live') ?? 0;
  }

  @override
  Future<void> saveHelp(int value) async {
    // final prefs = await instanceFuture;
    await instanceFuture.write('help', value);
    // await prefs.setInt('help', value);
  }

  @override
  Future<void> saveLive(int value) async {
    // final prefs = await instanceFuture;
    await instanceFuture.write('live', value);
    // await prefs.setInt('live', value);
  }
}
*/
