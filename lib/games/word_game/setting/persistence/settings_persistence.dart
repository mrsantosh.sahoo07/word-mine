import '../../../../word-mine/view/word_game.dart';

abstract class SettingsPersistence {
  Future<int> getLevel();

  Future<int> getCoin();

  Future<int> getHelp();

  Future<int> getLive();

  Future<bool> getMusicOn();

  Future<bool> getMuted({required bool defaultValue});

  Future<String> getPlayerName();

  Future<bool> getSoundsOn();

  Future<void> saveMusicOn(bool value);

  Future<void> saveMuted(bool value);

  Future<void> savePlayerName(String value);

  Future<void> saveSoundsOn(bool value);

  Future<void> saveLevel(int value);

  Future<void> saveCoin(int value);

  Future<void> saveHelp(int value);

  Future<void> saveLive(int value);

  Future<void> saveGameLevel(List<WordLevelBox> value);

  Future<List<WordLevelBox>> getGameLevel();
}
