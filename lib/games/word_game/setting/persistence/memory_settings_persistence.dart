import '../../../../word-mine/view/word_game.dart';
import 'settings_persistence.dart';

class MemoryOnlySettingsPersistence implements SettingsPersistence {
  bool musicOn = true;

  bool soundsOn = true;

  bool muted = false;
  int coin = 0;
  int level = 0;
  int help = 0;
  int live = 0;
  List<WordLevelBox> wordLevelBoxList = [];
  String playerName = 'Player';

  @override
  Future<int> getCoin() async => coin;

  @override
  Future<int> getLevel() async => level;
  @override
  Future<int> getHelp() async => help;
  @override
  Future<int> getLive() async => live;

  @override
  Future<bool> getMusicOn() async => musicOn;

  @override
  Future<bool> getMuted({required bool defaultValue}) async => muted;

  @override
  Future<String> getPlayerName() async => playerName;

  @override
  Future<bool> getSoundsOn() async => soundsOn;

  @override
  Future<void> saveMusicOn(bool value) async => musicOn = value;

  @override
  Future<void> saveMuted(bool value) async => muted = value;

  @override
  Future<void> savePlayerName(String value) async => playerName = value;

  @override
  Future<void> saveSoundsOn(bool value) async => soundsOn = value;

  @override
  Future<void> saveCoin(int value) async => coin = value;

  @override
  Future<void> saveHelp(int value) async => help = value;

  @override
  Future<void> saveLive(int value) async => live = value;

  @override
  Future<void> saveLevel(int value) async => level = value;

  @override
  Future<void> saveGameLevel(List<WordLevelBox> value) async =>
      wordLevelBoxList = value;

  @override
  Future<List<WordLevelBox>> getGameLevel() async => wordLevelBoxList;
}
