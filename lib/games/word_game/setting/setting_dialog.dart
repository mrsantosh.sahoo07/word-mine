import 'dart:async';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:go_router/go_router.dart';
import 'package:lottie/lottie.dart';
import 'package:neopop/utils/constants.dart';
import 'package:provider/provider.dart';

import '../../../animations/custom_clipeer.dart';
import '../audio/audio_controller.dart';
import '../audio/sounds.dart';
import '../view/dashboard/dashboard_screen.dart';
import '../view/start_view/start_screen.dart';
import 'setting_controller.dart';

showWinDialog(BuildContext context,bool isSuccess) {
  final audioController = context.read<AudioController>();
  audioController.playSfx(Sounds.win);
  showGeneralDialog(
      barrierColor: Colors.black.withOpacity(0.0),
      transitionDuration: const Duration(milliseconds: 300),
      barrierDismissible: true,
      barrierLabel: '',
      context: context,
      pageBuilder: (context, animation1, animation2) {
        return  WinDialogWidgets(isSuccess: isSuccess,);
      });
}

showAnimatedDialog(BuildContext context, Widget child) {
  showGeneralDialog(
      barrierColor: Colors.black.withOpacity(0.5),
      transitionBuilder: (context, a1, a2, widget) {
        return Transform.scale(
          scale: a1.value,
          child: Opacity(opacity: a1.value, child: child),
        );
      },
      transitionDuration: const Duration(milliseconds: 300),
      barrierDismissible: true,
      barrierLabel: '',
      context: context,
      pageBuilder: (context, animation1, animation2) {
        return Transform.scale(
          scale: animation1.value,
          child: Opacity(opacity: animation1.value, child: child),
        );
      });
}

showSettingDialog(BuildContext context) {
  final audioController = context.read<AudioController>();
  audioController.playSfx(Sounds.dialog);
  var size = MediaQuery.of(context).size;
  final settings = context.read<SettingsController>();
  showGeneralDialog(
    context: context,
    barrierDismissible: true,
    barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
    barrierColor: Colors.black.withOpacity(0.5),
    transitionDuration: const Duration(milliseconds: 300),
    transitionBuilder: (context, animation, secondaryAnimation, child) {
      return SlideTransition(
        position: Tween(begin: const Offset(0.0, -1.0), end: Offset.zero)
            .animate(animation),
        child: child,
      );
    },
    pageBuilder: (context, animation, secondaryAnimation) {
      return StatefulBuilder(builder: (context, setState) {
        return GestureDetector(
          onTap: () {
            audioController.playSfx(Sounds.dialog);
            context.pop();
          },
          child: Material(
            color: Colors.transparent,
            child: GestureDetector(
              onTap: () {},
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Stack(
                    children: [
                      ClipPath(
                        clipper: MyCustomClipper(),
                        child: Container(
                          padding: const EdgeInsets.all(10),
                          height: size.height / 2,
                          width: size.width,
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              colors: [
                                Color(0xfffefac7),
                                Color(0xfff8e79c),
                              ],
                              tileMode: TileMode.repeated,
                            ),
                          ),
                          child: ClipPath(
                            clipper: MyCustomClipper(),
                            child: Container(
                              height: size.height / 2,
                              width: size.width,
                              decoration: const BoxDecoration(
                                gradient: LinearGradient(
                                  tileMode: TileMode.clamp,
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  colors: [
                                    Color(0xfff7c316),
                                    Color(0xfff09b22),
                                  ],
                                ),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 42.0, vertical: 16),
                                child: SingleChildScrollView(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        "assets/images/setting.png",
                                        height: 80,
                                        width: 200,
                                      ),
                                      const SizedBox(height: 20),
                                      // Text(
                                      //   "SETTING",
                                      //   style: TextStyle(
                                      //       color: Colors.white,
                                      //       fontWeight:
                                      //           FontWeight.bold,
                                      //       fontSize: 40),
                                      // ),
                                      ValueListenableBuilder<bool>(
                                        valueListenable: context
                                            .watch<SettingsController>()
                                            .musicOn,
                                        builder: (context, musicOn, child) =>
                                            Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Icon(
                                              musicOn
                                                  ? Icons.music_note
                                                  : Icons.music_off,
                                              size: 50,
                                              color: const Color(0xffbb480b),
                                            ),
                                            const SizedBox(width: 10),
                                            GameSwitch(
                                              switchValue: !musicOn,
                                              onchange: (value) {
                                                audioController
                                                    .playSfx(Sounds.switched);
                                                settings.toggleMusicOn();
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(height: 5),
                                      ValueListenableBuilder<bool>(
                                        valueListenable: context
                                            .watch<SettingsController>()
                                            .soundsOn,
                                        builder: (context, soundsOn, child) =>
                                            Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Icon(
                                              soundsOn
                                                  ? Icons.volume_up
                                                  : Icons.volume_off,
                                              size: 50,
                                              color: const Color(0xffbb480b),
                                            ),
                                            const SizedBox(width: 10),
                                            GameSwitch(
                                              switchValue: !soundsOn,
                                              onchange: (value) {
                                                audioController
                                                    .playSfx(Sounds.switched);
                                                settings.toggleSoundsOn();
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(height: 5),
                                      const Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.music_note,
                                            size: 50,
                                            color: Color(0xffbb480b),
                                          ),
                                          SizedBox(width: 15),
                                          Expanded(
                                            child: GameSlider(),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 5),
                                      const Row(
                                        children: [
                                          Icon(
                                            Icons.volume_up,
                                            size: 50,
                                            color: Color(0xffbb480b),
                                          ),
                                          SizedBox(width: 15),
                                          Expanded(
                                            child: GameSlider(),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        top: 0,
                        right: 0,
                        child: GestureDetector(
                          child: Container(
                            decoration: const BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  colors: [
                                    Color(0xfffefac7),
                                    Color(0xfff8e79c),
                                  ],
                                  tileMode: TileMode.repeated,
                                ),
                                shape: BoxShape.circle),
                            padding: const EdgeInsets.all(8),
                            child: Image.asset(
                              "assets/images/close_bg.png",
                              height: 35,
                              width: 35,
                            ),
                          ),
                          onTap: () {
                            audioController.playSfx(Sounds.dialog);
                            context.pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      });
    },
  );
}

showAnimateDialog(BuildContext context, Widget child) {
  final audioController = context.read<AudioController>();
  audioController.playSfx(Sounds.dialog);
  showGeneralDialog(
    context: context,
    barrierDismissible: true,
    barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
    barrierColor: Colors.black.withOpacity(0.5),
    transitionDuration: const Duration(milliseconds: 300),
    transitionBuilder: (context, animation, secondaryAnimation, child) {
      return SlideTransition(
        position: Tween(begin: const Offset(0.0, -1.0), end: Offset.zero)
            .animate(animation),
        child: child,
      );
    },
    pageBuilder: (context, animation, secondaryAnimation) {
      return child;
    },
  );
}

class WinDialogWidgets extends StatefulWidget {
  final bool isSuccess;

  const WinDialogWidgets({super.key,this.isSuccess=false});

  @override
  State<WinDialogWidgets> createState() => _WinDialogWidgetsState();
}

class _WinDialogWidgetsState extends State<WinDialogWidgets> {



  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return
      Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          Container(
            color: Colors.black87,
            height: size.height,
            width: size.width,
            child: Column(
              children: [
                const SizedBox(height: 50),
                Image.asset(
                  "assets/images/game-win-popup-banner.png",
                  height: size.height / 4,
                  width: size.height / 4,
                ),
                Padding(
                  padding: EdgeInsets.only(top: size.height / 12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SGText(
                            "Rewards:",
                            fontSize: 24,
                            shadowWith: 1.5,
                          ),
                          const SizedBox(height: 20),
                          Row(
                            children: [
                              Image.asset(
                                "assets/images/coin3dd.png",
                                height: 40,
                                width: 40,
                              ),
                              const SizedBox(width: 10),
                              const Text(
                                "5",
                                style: TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.w900,
                                    color: Colors.white),
                              )
                            ],
                          ),
                        ],
                      ),
                      Transform.scale(
                        scale: 1.5,
                        child: Image.asset(
                          "assets/images/win_boy.png",
                          height: size.height / 3,
                          fit: BoxFit.fitHeight,
                          // width: size.width / 3,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: const EdgeInsets.only(top: 5),
              decoration: const BoxDecoration(
                color: Color(0xff58200a),
              ),
              child: Container(
                padding: const EdgeInsets.only(top: 8),
                decoration: const BoxDecoration(
                  color: Color(0xffb25d35),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      padding: const EdgeInsets.only(top: 7),
                      width: size.width,
                      decoration: const BoxDecoration(
                        color: Color(0xff01348d),
                      ),
                      child: Container(
                        width: size.width,
                        padding: const EdgeInsets.symmetric(vertical: 32),
                        decoration: const BoxDecoration(
                          color: Color(0xff144a99),
                        ),
                        alignment: Alignment.center,
                        child: GradientShadowButton(
                          text: "Next",
                          fontSize: 30,
                          shadowWith: 2,
                          strokeWidth: 0.5,
                          width: size.width / 2.5,
                          textGradiantColors: GameColors.textGreenGradient,
                          shadowColors: const Color(0xff1b6b00),
                          gradientColors: GameColors.greenGradient,
                          onTap: () {
                            context.pop();
                            context.pushReplacement('/dashBoardScreen',
                                extra: widget.isSuccess);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Align(
            alignment: const Alignment(0, -0.6),
            child: Lottie.asset("assets/json/win_1.json", repeat: false),
          ),
          // Align(
          //     alignment: Alignment.bottomCenter,
          //     child: Lottie.asset("assets/json/win_3.json", animate: true)),
        ],
      ),
      bottomNavigationBar: const ButtonNavigationAdsWidgets(),
    );
  }
}

class QuiteDialog extends StatefulWidget {
  final Function()? onQuit;

  const QuiteDialog({super.key, this.onQuit});

  @override
  State<QuiteDialog> createState() => _QuiteDialogState();
}

class _QuiteDialogState extends State<QuiteDialog> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Material(
      color: Colors.transparent,
      child: Center(
        child: Container(
          padding: const EdgeInsets.only(top: 5, bottom: 8),
          decoration: const BoxDecoration(
            color: Color(0xff58200a),
          ),
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 15),
            decoration: const BoxDecoration(
              color: Color(0xffb25d35),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 7, bottom: 5),
                  width: size.width,
                  decoration: const BoxDecoration(
                    color: Color(0xff01348d),
                  ),
                  child: Container(
                    width: size.width,
                    decoration: const BoxDecoration(
                      color: Color(0xff144a99),
                    ),
                    alignment: Alignment.center,
                    child: const Text(
                      "Quit Level?",
                      style: TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                          fontFamily: "Permanent Marker",
                          fontWeight: FontWeight.w900),
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 5, bottom: 5),
                  width: size.width,
                  decoration: const BoxDecoration(
                    color: Color(0xfff1c791),
                  ),
                  child: Container(
                    width: size.width,
                    decoration: const BoxDecoration(
                      color: Color(0xfffcebbb),
                    ),
                    padding: const EdgeInsets.symmetric(vertical: 50),
                    alignment: Alignment.center,
                    child: const Text(
                      "Are you sure to quit level?",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 20,
                          color: Color(0xff18498b),
                          fontFamily: "Permanent Marker",
                          fontWeight: FontWeight.w900),
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 7, bottom: 7),
                  width: size.width,
                  decoration: const BoxDecoration(
                    color: Color(0xff01348d),
                  ),
                  child: Container(
                    width: size.width,
                    decoration: const BoxDecoration(
                      color: Color(0xff154b98),
                    ),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 5, vertical: 30),
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          // width: size.width / 2.5,
                          child: GradientShadowButton(
                            text: "Quit",
                            fontSize: 20,
                            shadowWith: 1.5,
                            strokeWidth: .7,
                            width: (size.width / 2) - 10,
                            height: size.height / 12,
                            shadowColors: Colors.deepOrange,
                            textGradiantColors: GameColors.textGreenGradient,
                            gradientColors: GameColors.orangeGradient,
                            onTap: () {
                              final setting = context.read<SettingsController>();
                              int live= setting.live.value;
                              setting.setLive(live-1);

                              context.pop();
                              if (widget.onQuit != null) {
                                widget.onQuit!();
                              } else {
                                context.pushReplacement('/dashBoardScreen');
                              }
                            },
                          ),
                        ),
                        SizedBox(
                          // width: size.width / 2.5,
                          child: GradientShadowButton(
                            text: "Continue",
                            fontSize: 20,
                            shadowWith: 1.5,
                            strokeWidth: .7,
                            width: (size.width / 2) - 10,
                            height: size.height / 12,
                            shadowColors: GameColors.shadowGreen,
                            textGradiantColors: GameColors.textGreenGradient,
                            gradientColors: GameColors.greenGradient,
                            onTap: () {
                              context.pop();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PlayDialog extends StatelessWidget {
  final Function()? onTap;
  final String level;

  const PlayDialog({super.key, this.onTap, required this.level});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Material(
      color: Colors.transparent,
      child: Center(
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8, right: 8, top: 8),
              child: GradientBorder(
                borderRadius: 12,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        padding: const EdgeInsets.only(top: 7, bottom: 5),
                        width: size.width,
                        decoration: const BoxDecoration(
                          color: Color(0xff01348d),
                        ),
                        child: Container(
                          width: size.width,
                          decoration: const BoxDecoration(
                            color: Color(0xff144a99),
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            "Level $level",
                            style: const TextStyle(
                                fontSize: 30,
                                color: Colors.white,
                                fontFamily: "ArchiveBlack",
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(top: 5, bottom: 5),
                        width: size.width,
                        decoration: const BoxDecoration(
                          color: Color(0xfff1c791),
                        ),
                        child: Container(
                          width: size.width,
                          decoration: const BoxDecoration(
                            color: Color(0xfffcebbb),
                          ),
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          alignment: Alignment.center,
                          child: const Text(
                            "Play and learn words",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 20,
                                color: Color(0xff18498b),
                                fontFamily: "Permanent Marker",
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(top: 7, bottom: 7),
                        width: size.width,
                        decoration: const BoxDecoration(
                          color: Color(0xff01348d),
                        ),
                        child: Container(
                          width: size.width,
                          decoration: const BoxDecoration(
                            color: Color(0xff154b98),
                          ),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 15),
                          alignment: Alignment.center,
                          child: GradientShadowButton(
                            text: "Play",
                            fontSize: 24,
                            shadowWith: 1.5,
                            strokeWidth: .5,
                            width: size.width / 2,
                            shadowColors: GameColors.shadowGreen,
                            textGradiantColors: GameColors.textGreenGradient,
                            gradientColors: GameColors.greenGradient,
                            onTap: () {
                              if (onTap != null) {
                                onTap!();
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              top: 0,
              right: 0,
              child: GradientShadowButton(
                strokeWidth: 0.4,
                borderRadius: 8,
                height: 35,
                width: 35,
                padding: const EdgeInsets.all(8),
                gradientColors: GameColors.redGradient,
                onTap: () {
                  context.pop();
                },
                child: Image.asset('assets/images/close_icon.png'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class GradientShadowButton extends StatefulWidget {
  final String? text;
  final Widget? child;
  final TextStyle? textStyle;
  final Function() onTap;
  final List<Color> gradientColors;
  final String fontFamily;
  final double? fontSize;
  final bool showShimmer;
  final bool isEnable;
  final double shadowWith;
  final double? width;
  final double? height;
  final double strokeWidth;
  final EdgeInsets? padding;
  final EdgeInsets? margin;
  final double letterSpacing;
  final double borderRadius;
  final List<Color> textGradiantColors;
  final Color shadowColors;

  const GradientShadowButton({
    super.key,
    this.text,
    this.child,
    required this.gradientColors,
    this.textStyle,
    required this.onTap,
    this.textGradiantColors = GameColors.textYellowGradient,
    this.shadowColors = GameColors.brownBold,
    this.fontFamily = "ArchiveBlack",
    this.fontSize,
    this.width,
    this.showShimmer = false,
    this.isEnable = true,
    this.strokeWidth = 1,
    this.borderRadius = 12,
    this.padding,
    this.margin,
    this.height,
    this.letterSpacing = 4,
    this.shadowWith = 3,
  });

  @override
  State<GradientShadowButton> createState() => _GradientShadowButtonState();
}

class _GradientShadowButtonState extends State<GradientShadowButton> {
  @override
  Widget build(BuildContext context) {
    final audioController = context.read<AudioController>();
    return GestureDetector(
      onTap: () {
        audioController.playSfx(Sounds.buttonTap);
        widget.onTap();
      },
      child: SizedBox(
        width: widget.width,
        height: widget.height,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(widget.borderRadius),
          child: Stack(
            children: [
              GradientBorder(
                borderRadius: widget.borderRadius,
                strokeWidth: widget.strokeWidth,
                child: Container(
                    padding: widget.padding ??
                        const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                    margin: widget.margin ?? EdgeInsets.zero,
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: const Color(0xff89690e),
                          width: widget.strokeWidth),
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: widget.gradientColors,
                        tileMode: TileMode.repeated,
                      ),
                      borderRadius: BorderRadius.circular(widget.borderRadius),
                    ),
                    alignment: Alignment.center,
                    child: widget.text != null
                        ? SGText(
                            widget.text!,
                            textStyle: widget.textStyle,
                            shadowWith: widget.shadowWith,
                            shadowColors: widget.shadowColors,
                            fontSize: widget.fontSize,
                            fontFamily: widget.fontFamily,
                            letterSpacing: widget.letterSpacing,
                            key: widget.key,
                            textGradiantColors: widget.textGradiantColors,
                          )
                        : widget.child),
              ),
              if (widget.showShimmer)
                Positioned.fill(
                  child: Shimmer(
                    gradient: const LinearGradient(
                      begin: Alignment(-0.5, 1),
                      end: Alignment.topRight,
                      colors: <Color>[
                        Colors.white10,
                        Colors.white10,
                        Colors.white,
                        Colors.white10,
                        Colors.white10
                      ],
                      stops: <double>[0.0, 0.35, 0.5, 0.65, 1.0],
                    ),
                    child: Container(color: Colors.white30),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}

// class GradientShadowButton extends StatefulWidget {
//   final String? text;
//   final Widget? widget;
//   final TextStyle? textStyle;
//   final Function() onTap;
//   final List<Color> gradientColors;
//
//   const GradientShadowButton({
//     super.key,
//     this.text,
//     this.widget,
//     required this.gradientColors,
//     this.textStyle,
//     required this.onTap,
//   });
//
//   @override
//   State<GradientShadowButton> createState() => _GradientShadowButtonState();
// }
//
// class _GradientShadowButtonState extends State<GradientShadowButton> {
//   @override
//   Widget build(BuildContext context) {
//     Color borderColor = const Color(0xffdb8a44);
//     Color borderShadowColor = const Color(0xff973b18);
//     var size = MediaQuery.of(context).size;
//     double borderRadius = 12;
//     return GestureDetector(
//       onTap: () {
//         widget.onTap();
//       },
//       child: Container(
//         width: size.width / 2.5,
//         height: 70,
//         padding: const EdgeInsets.only(bottom: 4, left: 1, right: 1),
//         decoration: BoxDecoration(
//           color: const Color(0xff6a260c),
//           borderRadius: BorderRadius.circular(borderRadius),
//         ),
//         child: Container(
//           decoration: BoxDecoration(
//             gradient: RadialGradient(
//               tileMode: TileMode.clamp,
//               center: Alignment.topCenter,
//               radius: 15,
//               colors: [
//                 for (int i = 0; i < 4; i++) ...[
//                   borderColor,
//                   borderShadowColor,
//                 ]
//               ],
//             ),
//             borderRadius: BorderRadius.circular(borderRadius),
//           ),
//           child: Container(
//             margin: const EdgeInsets.all(5),
//             // padding: const EdgeInsets.only(left: 10, right: 10, bottom: 8, top: 4),
//             decoration: BoxDecoration(
//               border: Border.all(color: const Color(0xff762f07), width: 1.5),
//               gradient: LinearGradient(
//                 begin: Alignment.topCenter,
//                 end: Alignment.bottomCenter,
//                 colors: widget.gradientColors,
//                 tileMode: TileMode.repeated,
//               ),
//               borderRadius: BorderRadius.circular(borderRadius),
//             ),
//             child: Stack(
//               alignment: Alignment.center,
//               children: [
//                 if (widget.text != null) ...[
//                   Text(
//                     widget.text!,
//                     textAlign: TextAlign.center,
//                     style: widget.textStyle ??
//                         const TextStyle(
//                           fontSize: 25,
//                           color: Color(0xff1a7301),
//                           fontFamily: "Permanent Marker",
//                           fontWeight: FontWeight.w900,
//                           letterSpacing: 2,
//                           shadows: [
//                             Shadow(
//                               color: Color(0xff1a7301),
//                               offset: Offset(2.0, 2.0),
//                               blurRadius: 0,
//                             ),
//                           ],
//                         ),
//                   ),
//                   GradientText(
//                     widget.text!,
//                     gradientType: GradientType.linear,
//                     gradientDirection: GradientDirection.ttb,
//                     colors: const [
//                       Color(0xfffdfbe6),
//                       Color(0xfffee7a1),
//                     ],
//                     style: widget.textStyle ??
//                         const TextStyle(
//                           fontSize: 25,
//                           fontFamily: "Permanent Marker",
//                           fontWeight: FontWeight.w900,
//                           letterSpacing: 2,
//                           shadows: [
//                             Shadow(
//                               color: Color(0xff1a7301),
//                               offset: Offset(2.0, 2.0),
//                               blurRadius: 0,
//                             ),
//                           ],
//                         ),
//                   ),
//                 ] else
//                   (widget.widget ?? const SizedBox()),
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

class GameColors {
  static const List<Color> orangeGradient = [
    Color(0xffffbf21),
    Color(0xffffa000),
    Color(0xffff9701),
    Color(0xfffe8800),
    Color(0xffe06d00)
  ];
  static const List<Color> greenGradient = [
    Color(0xffa6eb00),
    Color(0xff69d201),
    Color(0xff55c501),
    Color(0xff3eb900),
    Color(0xff29a101),
  ];
  static const List<Color> redGradient = [
    Color(0xffea4848),
    Color(0xffd62930),
    Color(0xffc0171d),
    Color(0xffb20d11),
    Color(0xff850506),
  ];

  static const List<Color> appBackgroundGradient = [
    Color(0xff0275bc),
    Color(0xff002a6a),
  ];
  static const List<Color> whiteGradient = [
    Colors.white,
    Colors.white,
  ];
  static const List<Color> greyGradient = [
    Colors.grey,
    Colors.grey,
  ];
  static const List<Color> textYellowGradient = [
    Color(0xfff9e410),
    Color(0xfffebf07),
  ];
  static const List<Color> textGreenGradient = [
    Color(0xfff7f3e0),
    Color(0xfff2db95),
  ];
  static const Color brownBold = Color(0xff811400);
  static const Color shadowGreen = Color(0xff1b6b00);
}

class GradientTexts extends StatelessWidget {
  const GradientTexts(
    this.text, {
    required this.gradient,
    this.style,
  });

  final String text;
  final TextStyle? style;
  final Gradient gradient;

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      blendMode: BlendMode.srcIn,
      shaderCallback: (bounds) => gradient.createShader(
        Rect.fromLTWH(0, 0, bounds.width, bounds.height),
      ),
      child: Text(text, style: style),
    );
  }
}

class GradientText extends StatelessWidget {
  final List<Color> colors;

  final GradientDirection? gradientDirection;

  final GradientType gradientType;

  final TextOverflow? overflow;

  final double radius;

  final TextStyle? style;

  final String text;

  final TextAlign? textAlign;

  final double? textScaleFactor;

  final int? maxLines;

  final List<double>? stops;

  const GradientText(
    this.text, {
    required this.colors,
    this.gradientDirection = GradientDirection.ltr,
    this.gradientType = GradientType.linear,
    super.key,
    this.overflow,
    this.radius = 1.0,
    this.style,
    this.textAlign,
    this.stops,
    this.textScaleFactor,
    this.maxLines,
  }) : assert(
          colors.length >= 2,
          'Colors list must have at least two colors',
        );

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (Rect bounds) {
        switch (gradientType) {
          case GradientType.linear:
            final Map<String, Alignment> map = {};
            switch (gradientDirection) {
              case GradientDirection.rtl:
                map['begin'] = Alignment.centerRight;
                map['end'] = Alignment.centerLeft;
              case GradientDirection.ttb:
                map['begin'] = Alignment.topCenter;
                map['end'] = Alignment.bottomCenter;
              case GradientDirection.btt:
                map['begin'] = Alignment.bottomCenter;
                map['end'] = Alignment.topCenter;
              default:
                map['begin'] = Alignment.centerLeft;
                map['end'] = Alignment.centerRight;
            }
            return LinearGradient(
              begin: map['begin']!,
              colors: colors,
              stops: stops,
              end: map['end']!,
            ).createShader(bounds);
          case GradientType.radial:
            return RadialGradient(
              colors: colors,
              radius: radius,
            ).createShader(bounds);
        }
      },
      child: Text(
        text,
        overflow: overflow,
        style: style != null
            ? style?.copyWith(color: Colors.white)
            : const TextStyle(color: Colors.white),
        textAlign: textAlign,
        maxLines: maxLines,
      ),
    );
  }
}

enum GradientType {
  linear,
  radial,
}

enum GradientDirection {
  btt,
  ltr,
  rtl,
  ttb,
}

// SizedBox(
//   height: h,
//   width: width,
//   child: ClipRRect(
//     borderRadius: BorderRadius.circular(20),
//     child: Stack(
//       // alignment: Alignment.center,
//       children: [
//         Align(
//           alignment: Alignment.topCenter,
//           child: Container(
//             height: w,
//             color: borderColor,
//             child: Container(
//               padding: const EdgeInsets.symmetric(
//                   horizontal: 5, vertical: 5),
//               decoration: BoxDecoration(
//                 boxShadow: [
//                   BoxShadow(
//                     color: borderShadowColor,
//                     spreadRadius: -5,
//                     blurRadius: 10,
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//         Align(
//           alignment: Alignment.bottomCenter,
//           child: Container(
//             height: w,
//             color: borderColor,
//             child: Container(
//               padding: const EdgeInsets.symmetric(
//                   horizontal: 5, vertical: 5),
//               decoration: BoxDecoration(
//                 boxShadow: [
//                   BoxShadow(
//                     color: borderShadowColor,
//                     spreadRadius: -5,
//                     blurRadius: 10,
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//         Align(
//           alignment: Alignment.centerLeft,
//           child: Container(
//             height: h,
//             width: w,
//             color: borderColor,
//             margin: EdgeInsets.symmetric(vertical: margin),
//             child: Container(
//               padding: const EdgeInsets.symmetric(
//                   horizontal: 5, vertical: 5),
//               decoration: BoxDecoration(
//                 boxShadow: [
//                   BoxShadow(
//                     color: borderShadowColor,
//                     spreadRadius: -5,
//                     blurRadius: 10,
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//         Align(
//           alignment: Alignment.centerRight,
//           child: Container(
//             height: h,
//             width: w,
//             color: borderColor,
//             margin: EdgeInsets.symmetric(vertical: margin),
//             child: Container(
//               padding: const EdgeInsets.symmetric(
//                   horizontal: 5, vertical: 5),
//               decoration: BoxDecoration(
//                 boxShadow: [
//                   BoxShadow(
//                     color: borderShadowColor,
//                     spreadRadius: -5,
//                     blurRadius: 10,
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//         //
//         Align(
//           // alignment: Alignment.center,
//           child: Container(
//             margin: const EdgeInsets.all(10),
//             // alignment: Alignment.center,
//             decoration: BoxDecoration(
//               border: Border.all(color: const Color(0xff762f07), width: 2),
//               gradient: const LinearGradient(
//                 begin: Alignment.topCenter,
//                 end: Alignment.bottomCenter,
//                 colors: [
//                   Color(0xffa6eb00),
//                   Color(0xff69d201),
//                   Color(0xff55c501),
//                   Color(0xff3eb900),
//                   Color(0xff29a101),
//                 ],
//                 tileMode: TileMode.repeated,
//               ),
//               borderRadius: BorderRadius.circular(20),
//             ),
//             child: Container(
//                 height: h,
//                 alignment: Alignment.center,
//                 decoration: const BoxDecoration(
//                   boxShadow: [
//                     BoxShadow(
//                       color: Color(0xffa6ec00),
//                       spreadRadius: -40,
//                       blurRadius: 80,
//                     ),
//                   ],
//                 ),
//                 child: Stack(
//                   alignment: Alignment.center,
//                   children: [
//                     const Text(
//                       "Cat",
//                       textAlign: TextAlign.center,
//                       style: TextStyle(
//                         fontSize: 55,
//                         color: Color(0xff1a7301),
//                         fontFamily: "Permanent Marker",
//                         fontWeight: FontWeight.w900,
//                         letterSpacing: 5,
//                         shadows: [
//                           Shadow(
//                             color: Color(0xff1a7301),
//                             offset: Offset(2.0, 2.0),
//                             blurRadius: 0,
//                           ),
//                         ],
//                       ),
//                     ),
//                     GradientText(
//                       "cat",
//                       gradientType: GradientType.linear,
//                       gradientDirection: GradientDirection.ttb,
//                       colors: const [
//                         Color(0xfffdfbe6),
//                         Color(0xfffee7a1),
//                       ],
//                       style: const TextStyle(
//                         fontSize: 50,
//                         color: Color(0xffffe9a2),
//                         fontFamily: "Permanent Marker",
//                         fontWeight: FontWeight.w900,
//                         letterSpacing: 5,
//                         shadows: [
//                           Shadow(
//                             color: Color(0xff1a7301),
//                             offset: Offset(2.0, 2.0),
//                             blurRadius: 0,
//                           ),
//                         ],
//                       ),
//                     ),
//                   ],
//                 )),
//           ),
//         ),
//       ],
//     ),
//   ),
// ),
