import 'dart:math';

import 'package:flutter/foundation.dart';

import '../../../word-mine/view/word_game.dart';
import 'persistence/settings_persistence.dart';

class SettingsController {
  final SettingsPersistence _persistence;

  ValueNotifier<bool> muted = ValueNotifier(false);

  ValueNotifier<String> playerName = ValueNotifier("");

  ValueNotifier<bool> soundsOn = ValueNotifier(false);

  ValueNotifier<bool> musicOn = ValueNotifier(false);

  ValueNotifier<int> coin = ValueNotifier(0);

  ValueNotifier<int> level = ValueNotifier(0);

  ValueNotifier<int> live = ValueNotifier(0);

  ValueNotifier<int> help = ValueNotifier(0);

  ValueNotifier<List<WordLevelBox>> wordLevelBoxList = ValueNotifier([]);

  SettingsController({required SettingsPersistence persistence})
      : _persistence = persistence;

  Future<void> loadStateFromPersistence() async {
    await Future.wait([
      _persistence
          .getMuted(defaultValue: kIsWeb)
          .then((value) => muted.value = value),
      _persistence.getSoundsOn().then((value) => soundsOn.value = value),
      _persistence.getMusicOn().then((value) => musicOn.value = value),
      _persistence.getPlayerName().then((value) => playerName.value = value),
      _persistence.getLevel().then((value) => level.value = (value)),
      _persistence.getHelp().then((value) => help.value = value),
      _persistence.getLive().then((value) => live.value = value),
      _persistence.getCoin().then((value) => coin.value = value),
      _persistence
          .getGameLevel()
          .then((value) => wordLevelBoxList.value = value),
    ]);
    if (wordLevelBoxList.value.isEmpty) {
    List<WordLevelBox> list = await generateWordBoxes();
    {
      saveGameLevel(list);
    }
    }
  }

  void saveGameLevel(List<WordLevelBox> data) {
    wordLevelBoxList.value = data;
    _persistence.saveGameLevel(wordLevelBoxList.value);
  }

  void setPlayerName(String name) {
    playerName.value = name;
    _persistence.savePlayerName(playerName.value);
  }

  void setGameLevel(int data) {
    level.value = data;
    _persistence.saveLevel(level.value);
  }

  void setCoin(int data) {
    coin.value = data;
    _persistence.saveCoin(data);
    _persistence.getCoin();
  }

  void setHelp(int data) {
    help.value = data;
    _persistence.saveHelp(help.value);
  }

  void setLive(int data) {
    live.value = data;
    _persistence.saveLive(live.value);
    _persistence.getLive();
  }

  void gwtLevel() {
    _persistence.getLevel().then((value) => level.value = (value));
  }

  void toggleMusicOn() {
    musicOn.value = !musicOn.value;
    _persistence.saveMusicOn(musicOn.value);
  }

  void toggleMuted() {
    muted.value = !muted.value;
    _persistence.saveMuted(muted.value);
  }

  void toggleSoundsOn() {
    soundsOn.value = !soundsOn.value;
    _persistence.saveSoundsOn(soundsOn.value);
  }

  Map<Map<String, int>, List<int>> findIndices(List<List<String>> value) {
    Map<Map<String, int>, List<int>> indicesMap = {};
    for (int i = 0; i < value.length; i++) {
      String horizontalWord = '';
      List<int> horizontalWordIndex = [];
      for (int j = 0; j < value[i].length; j++) {
        horizontalWord += value[i][j];
        if (value[i][j] != " ") {
          horizontalWordIndex.add(i * value[i].length + j);
        }
      }
      indicesMap
          .putIfAbsent({horizontalWord.trim(): 0}, () => horizontalWordIndex);
    }
    for (int i = 0; i < value[0].length; i++) {
      String column = '';
      List<int> horizontalWordIndex = [];
      for (int j = 0; j < value.length; j++) {
        column += value[j][i];
        if (value[j][i] != " ") {
          horizontalWordIndex.add(j * value[0].length + i);
        }
      }
      indicesMap.putIfAbsent({column.trim(): 1}, () => horizontalWordIndex);
    }
    return indicesMap;
  }

  Future<List<WordLevelBox>> generateWordBoxes() async {
    List<WordLevelBox> wordLeveList = [];
    wordLeveList.clear();
    final Map<List<String>, List<List<String>>> list = {
      ["CAT", "ACT"]: [
        ["C", "A", "T"],
        [" ", "C", " "],
        [" ", "T", " "]
      ],
      ["DOG", "GOD"]: [
        ["G", " ", " "],
        ["O", " ", " "],
        ["D", "O", "G"]
      ],
      ["BAT", "TAB"]: [
        [" ", "B", " "],
        ["T", "A", "B"],
        [" ", "T", " "],
      ],
      ["RAT", "ART"]: [
        ["A", "R", "T"],
        [" ", "A", " "],
        [" ", "T", " "]
      ],
      ["POT", "TOP"]: [
        [" ", "P", " "],
        ["T", "O", "P"],
        [" ", "T", " "],
      ],
      ["EAT", "TEA"]: [
        ["T", "E", "A"],
        [" ", "A", " "],
        [" ", "T", " "]
      ],
      ["HOT", "THAT"]: [
        ["T", "H", "A", "T"],
        [" ", "O", " ", " "],
        [" ", "T", " ", " "]
      ],
      ["JOY", "TOY"]: [
        [" ", "J", " "],
        ["T", "O", "Y"],
        [" ", "Y", " "]
      ],
      ["FUN", "RUN"]: [
        [" ", "F", " "],
        ["R", "U", "N"],
        [" ", "N", " "]
      ],

      ["CHAT", "THAT"]: [
        [" ", "C", " ", " "],
        ["T", "H", "A", "T"],
        [" ", "A", " ", " "],
        [" ", "T", " ", " "]
      ],
      ["MINE", "LINE"]: [
        [" ", "M", " ", " "],
        ["L", "I", "N", "E"],
        [" ", "N", " ", " "],
        [" ", "E", " ", " "]
      ],

      ["WIND", "MIND"]: [
        [" ", " ", " ", "W"],
        [" ", " ", " ", "I"],
        [" ", " ", " ", "N"],
        ["M", "I", "N", "D"],
      ],
      ["PLAN", "CLAN"]: [
        [" ", "P", " ", " "],
        ["C", "L", "A", "N"],
        [" ", "A", " ", " "],
        [" ", "N", " ", " "]
      ],
      ["FISH", "DISH"]: [
        [" ", "F", " ", " "],
        ["D", "I", "S", "H"],
        [" ", "S", " ", " "],
        [" ", "H", " ", " "]
      ],

      ["ROAD", "TOAD"]: [
        [" ", "R", " ", " "],
        ["T", "O", "A", "D"],
        [" ", "A", " ", " "],
        [" ", "D", " ", " "]
      ],
      ["COLD", "BOLD"]: [
        [" ", "C", " ", " "],
        ["B", "O", "L", "D"],
        [" ", "L", " ", " "],
        [" ", "D", " ", " "]
      ],
      ["KING", "RING"]: [
        [" ", "K", " ", " "],
        ["R", "I", "N", "G"],
        [" ", "N", " ", " "],
        [" ", "G", " ", " "]
      ],
      ["RAIN", "GAIN"]: [
        [" ", "R", " ", " "],
        ["G", "A", "I", "N"],
        [" ", "I", " ", " "],
        [" ", "N", " ", " "]
      ],

      ["TALK", "WALK"]: [
        [" ", "T", " ", " "],
        ["W", "A", "L", "K"],
        [" ", "L", " ", " "],
        [" ", "K", " ", " "]
      ],

      ["FOUR", "TOUR"]: [
        [" ", "F", " ", " "],
        ["T", "O", "U", "R"],
        [" ", "U", " ", " "],
        [" ", "R", " ", " "]
      ],

      /// 3

      ["OWN", "WON", "NOW"]: [
        ["O", " ", " "],
        ["W", "O", "N"],
        ["N", " ", "O"],
        [" ", " ", "W"]
      ],
      ["SAND", "LAND", "AND"]: [
        [" ", "S", " ", " "],
        ["L", "A", "N", "D"],
        ["A", "N", "D", " "],
        [" ", "D", " ", " "]
      ],
      ["ARE", "EAR", "ERA"]: [
        ["A", " ", " "],
        ["R", " ", "E"],
        ["E", "A", "R"],
        [" ", " ", "A"]
      ],
      ["DARE", "DEAR", "READ"]: [
        [" ", " ", " ", "D"],
        ["D", "A", "R", "E"],
        [" ", " ", "E", "A"],
        [" ", " ", "A", "R"],
        [" ", " ", "D", " "],
      ],

      ["TYPE", "YET", "PET"]: [
        [" ", " ", " ", "P"],
        ["T", "Y", "P", "E"],
        [" ", "E", " ", "T"],
        [" ", "T", " ", " "],
      ],
      ["PLAY", "PAY", "LAY"]: [
        [" ", " ", "L", " "],
        ["P", "L", "A", "Y"],
        ["A", " ", "Y", " "],
      ],
      ["GAME", "AGE", "AM"]: [
        [" ", " ", "A", " "],
        ["G", "A", "M", "E"],
        [" ", "G", " ", " "],
        [" ", "E", " ", " "],
      ],
      ["KITE", "TAKE", "TIE"]: [
        ["K", "I", "T", "E"],
        [" ", " ", "A", " "],
        [" ", " ", "K", " "],
        ["T", "I", "E", " "],
      ],

      ["SHIP", "SLIP", "LIPS"]: [
        ["L", "I", "P", "S", " "],
        [" ", " ", " ", "H", " "],
        [" ", "S", "L", "I", "P"],
        [" ", " ", " ", "P", " "],
      ],

      ["TIME", "ITEM", "MET"]: [
        ["I", " ", " ", "M"],
        ["T", "I", "M", "E"],
        ["E", " ", " ", "T"],
        ["M", " ", " ", " "],
      ],

      ["GOLD", "SOLD", "OLD"]: [
        [" ", " ", " ", "O"],
        [" ", "G", " ", "L"],
        ["S", "O", "L", "D"],
        [" ", "L", " ", " "],
        [" ", "D", " ", " "]
      ],
      ["BEST", "BET", "SET"]: [
        ["B", "E", "S", "T"],
        [" ", " ", "E", " "],
        ["B", "E", "T", " "]
      ],
      ["BUSY", "BUS", "BUY"]: [
        [" ", " ", " ", "B"],
        [" ", "B", " ", "U"],
        ["B", "U", "S", "Y"],
        [" ", "S", " ", " "]
      ],
      ["WARM", "FARM", "FAR"]: [
        [" ", "W", " ", " "],
        ["F", "A", "R", "M"],
        ["A", "R", " ", " "],
        ["R", "M", " ", " "]
      ],
      ["BEAM", "TEAM", "MET"]: [
        ["M", "", " ", " "],
        ["E", "B", " ", " "],
        ["T", "E", "A", "M"],
        [" ", "A", " ", " "],
        [" ", "M", " ", " "]
      ],
      ["HOME", "COME", "ECHO"]: [
        [" ", "H", " ", " "],
        ["C", "O", "M", "E"],
        [" ", "M", " ", "C"],
        [" ", "E", " ", "H"],
        [" ", " ", " ", "O"]
      ],

      /// 4

      ["LAMP", "PALM", "MAP", "LAP"]: [
        [" ", " ", "L", " ", " "],
        [" ", "P", "A", "L", "M"],
        [" ", " ", "M", " ", "A"],
        ["L", "A", "P", " ", "P"]
      ],
      ["WATCH", "CHAT", "WHAT", "ACT"]: [
        [" ", "C", " ", "", " "],
        [" ", "H", " ", "A", " "],
        ["W", "A", "T", "C", "H"],
        ["H", "T", " ", "T", " "],
        ["A", " ", " ", " ", " "],
        ["T", " ", " ", " ", " "]
      ],
      ["ZEBRA", "BEAR", "ARE", "BARE"]: [
        [" ", "B", " ", " ", "B"],
        ["Z", "E", "B", "R", "A"],
        [" ", "A", " ", " ", "R"],
        ["A", "R", "E", " ", "E"],
      ],
      ["HAND", "BAND", "OLD", "DO"]: [
        [" ", " ", "D", "O"],
        [" ", "H", " ", "L"],
        ["B", "A", "N", "D"],
        [" ", "N", " ", " "],
        [" ", "D", " ", " "]
      ],
      ["NIGHT", "THING", "HINT", "HIT"]: [
        [" ", "T", " ", " ", " "],
        [" ", "H", " ", "", " "],
        ["N", "I", "G", "H", "T"],
        [" ", "N", " ", "I", " "],
        [" ", "G", " ", "T", " "],
      ],
      ["MANGO", "AMONG", "AGO", "MAN"]: [
        ["M", "A", "N", "G", "O"],
        [" ", "M", " ", " ", " "],
        [" ", "O", " ", " ", "M"],
        [" ", "N", " ", " ", "A"],
        ["A", "G", "O", " ", "N"],
      ],
      ["FIRE", "WIRE", "WIFE", "FEW"]: [
        [" ", "F", " ", " "],
        ["W", "I", "R", "E"],
        ["I", "R", " ", " "],
        ["F", "E", "W", " "],
        ["E", " ", " ", " "],
      ],
      ["FAIR", "AIR", "FAR", "IF"]: [
        [" ", "F", "A", "I", "R"],
        ["I", " ", "I", " ", " "],
        ["F", "A", "R", " ", " "]
      ],
      ["TEAM", "SEAM", "MEAT", "EAT"]: [
        [" ", "T", " ", " "],
        ["S", "E", "A", "M"],
        ["E", "A", "T", " "],
        [" ", "M", " ", " "]
      ],
      ["SMART", "STAR", "ART"]: [
        ["S", "M", "A", "R", "T"],
        ["T", " ", "R", " ", "R"],
        ["A", " ", "M", " ", "A"],
        ["R", " ", " ", " ", "M"],
      ],
      ["REAL", "EAR", "ARE", "ERA"]: [
        [" ", " ", "E", " "],
        [" ", " ", "R", " "],
        ["R", "E", "A", "L"],
        [" ", "A", " ", " "],
        ["A", "R", "E", " "],
      ],

      ["MONKEY", "MONEY", "ONE", "MEN"]: [
        [" ", " ", "M", " ", " ", " "],
        [" ", " ", "O", " ", "M", " "],
        ["M", "O", "N", "K", "E", "Y"],
        [" ", "N", "E", " ", "N", " "],
        [" ", "E", "Y", " ", " ", " "],
      ],

      ["MOTHER", "METRO", "HOME", "HERO"]: [
        [" ", " ", "M", " ", " ", "H"],
        [" ", " ", "E", " ", " ", "E"],
        ["M", "O", "T", "H", "E", "R"],
        [" ", " ", "R", " ", " ", "O"],
        [" ", "H", "O", "M", "E", " "],
      ],
      ["FATHER", "HATER", "HEAR", "AFTER", "EARTH", "HER"]: [
        [" ", " ", " ", "H", " ", "H"],
        [" ", " ", " ", "E", " ", "A"],
        ["H", "E", "A", "R", " ", "T"],
        [" ", " ", "F", " ", " ", "E"],
        ["F", "A", "T", "H", "E", "R"],
        [" ", " ", "E", " ", " ", " "],
        ["E", "A", "R", "T", "H", " "],
      ],
      ["MASTER", "STREAM", "SMART", "TEAM", "SAME", "ARM", "ART"]: [
        ["A", " ", " ", "A", " ", " "],
        ["R", " ", " ", "R", " ", " "],
        ["M", "A", "S", "T", "E", "T"],
        [" ", " ", "M", " ", " ", "E"],
        [" ", " ", "A", " ", "S", "A"],
        ["S", "T", "R", "E", "A", "M"],
        [" ", " ", "T", " ", "M", " "],
        [" ", " ", " ", " ", "E", " "],
      ],
    };

    for (int i = 0; i < list.length; i++) {
      List<WordBox> wordBoxListTemp = [];
      List<WordAnswer> wordAnsListTemp = [];
      List<String> dragWords = [];
      var data = list.values.elementAt(i);
      int crossAxisCount = data.first.length;
      int paddingCount = max(data.first.length, data.length);
      var listKey = list.keys.elementAt(i);
      Map<Map<String, int>, List<int>> indicesMap = findIndices(data);
      indicesMap.forEach((key, value) {
        if (listKey.contains(key.keys.first)) {
          WordAnswer wordAnswer = WordAnswer(
            word: key.keys.first,
            wordIndex: value,
            rotateQuery: key.values.first,
          );
          wordAnsListTemp.add(wordAnswer);
        }
      });
      for (int j = 0; j < data.length; j++) {
        var data2 = data.elementAt(j);
        for (int k = 0; k < data2.length; k++) {
          String letter = data2[k];
          if (letter != " " && letter != "" && !dragWords.contains(letter)) {
            dragWords.add(letter.trim());
          }
          int index = j * data2.length + k;
          WordBox wordBox = WordBox(
            index: index,
            name: letter,
            showBox: letter != " ",
            isCorrect: letter == " ",
          );
          wordBoxListTemp.add(wordBox);
        }
      }
      dragWords.shuffle();
      WordLevelBox wordLevelBox = WordLevelBox();
      wordLevelBox.wordAnsList = wordAnsListTemp;
      wordLevelBox.wordBoxList = wordBoxListTemp;
      wordLevelBox.dragWordList = dragWords;
      wordLevelBox.paddingCount = paddingCount;
      wordLevelBox.crossAxisCount = crossAxisCount;
      wordLeveList.add(wordLevelBox);
      // wordBoxList = wordBoxListTemp;
      // wordAnsList = wordAnsListTemp;
    }
    return wordLeveList;
    // randomName.addAll(dragWords);
    // randomName.shuffle();
  }
}
