
import 'package:flutter/material.dart';

class DragAllWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Stack(
        // alignment: Alignment.topLeft,
        children: [
          ...List.generate(
            12,
                (index) => Padding(
              padding: EdgeInsets.only(
                left: index % 4 * 105,
                // Adjust the left padding based on the item's position in each row
                top: (index ~/ 4) *
                    105, // Calculate the top padding based on the number of rows completed
              ),
              child: DragTargetItem(),
            ),
          ),
          ...List.generate(
            12,
                (index) => Padding(
              padding: EdgeInsets.only(
                left:  index % 4 * 105,
                top: (index ~/ 4) * 105,
              ),
              child: index == 2?null: DraggableItem(
                width: index == 1 ? 205 : 100,
                height: 100,
                itemName: 'Item $index',
                itemColor: index == 1 ? Colors.pink : Colors.blue,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DraggableItem extends StatelessWidget {
  final String itemName;
  final Color itemColor;
  final double height;
  final double width;

  const DraggableItem({
    required this.itemName,
    required this.itemColor,
    required this.height,
    required this.width,
  });

  @override
  Widget build(BuildContext context) {
    return Draggable(
      data: itemName,
      feedback: Material(
        child: Container(
          height: height,
          width: width,
          color: itemColor.withOpacity(0.5),
          child: Center(
            child: Text(itemName),
          ),
        ),
      ),
      onDragStarted: () {
        // print("onDragStarted");
      },
      childWhenDragging: Container(),
      child: Material(
        child: Container(
          height: height,
          width: width,
          color: itemColor,
          child: Center(
            child: Text(itemName),
          ),
        ),
      ), // Hide the original item when dragging
    );
  }
}

class DragTargetItem extends StatefulWidget {
  @override
  _DragTargetItemState createState() => _DragTargetItemState();
}

class _DragTargetItemState extends State<DragTargetItem> {
  String? draggedItem;

  @override
  Widget build(BuildContext context) {
    return DragTarget<String>(
      builder: (BuildContext context, List<String?> candidateData,
          List<dynamic> rejectedData) {
        return Container(
          height: 100,
          width: 100,
          color: draggedItem != null ? Colors.blue : Colors.grey,
          child: Center(
            child: Text(
              draggedItem != null ? 'Drop Here: $draggedItem' : 'Drop Here',
              textAlign: TextAlign.center,
            ),
          ),
        );
      },
      onAcceptWithDetails: (data) {
        print(data.data);
        setState(() {
          draggedItem = data.data;
        });
      },
      onLeave: (data) {
        // Handle when an item leaves the drag target
      },
    );
  }
}