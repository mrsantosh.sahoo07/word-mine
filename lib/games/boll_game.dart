import 'package:flutter/material.dart';
import 'package:sensors_plus/sensors_plus.dart';

class Ball extends StatelessWidget {
  final double ballSize;
  final Color ballColor;

  Ball({required this.ballSize, required this.ballColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ballSize,
      height: ballSize,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: ballColor,
      ),
    );
  }
}

class SensorBallScreen extends StatefulWidget {
  @override
  _SensorBallScreenState createState() => _SensorBallScreenState();
}

class _SensorBallScreenState extends State<SensorBallScreen> {
  SensorDirection? currentDirection;
  double ballPositionX = 0;
  double ballPositionY = 0;

  @override
  void initState() {
    super.initState();
    accelerometerEventStream().listen((AccelerometerEvent event) {
      setState(() {
        // Update ball position based on accelerometer data
        ballPositionX += (event.x);
        ballPositionY += (event.y);

        // Check for sensor direction
        if (event.x < -2.0) {
          currentDirection = SensorDirection.left;
        } else if (event.x > 2.0) {
          currentDirection = SensorDirection.right;
        } else if (event.y < -2.0) {
          currentDirection = SensorDirection.top;
        } else if (event.y > 2.0) {
          currentDirection = SensorDirection.bottom;
        } else {
          currentDirection = SensorDirection.center;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sensor Ball'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Current Direction: ${currentDirection.toString()}'),
            SizedBox(height: 20),
            Transform.translate(
              offset: Offset(ballPositionX, ballPositionY),
              child: Ball(
                ballSize: 50,
                ballColor: Colors.blue,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
enum SensorDirection { center, left, right, top, bottom }

//
// import 'dart:math';
//
// import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';
// import 'package:sensors_plus/sensors_plus.dart';
//
// enum SensorDirection { center, left, right, top, bottom }
//
// void main() {
//   runApp(MyApp());
// }
//
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Automatic Scroll List',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: AutomaticScrollList(),
//     );
//   }
// }
//
// class AutomaticScrollList extends StatefulWidget {
//   @override
//   _AutomaticScrollListState createState() => _AutomaticScrollListState();
// }
//
// class _AutomaticScrollListState extends State<AutomaticScrollList> {
//   SensorDirection currentDirection = SensorDirection.center;
//   ScrollController scrollController = ScrollController();
//
//   @override
//   void initState() {
//     super.initState();
//     accelerometerEventStream().listen((event) {
//       setState(() {
//         detectSensorDirection(event);
//       });
//     });
//   }
//
//   void detectSensorDirection(AccelerometerEvent event) {
//     double x = event.x;
//     double y = event.y;
//
//     if (x < -1.0) {
//       currentDirection = SensorDirection.left;
//       scrollList(currentDirection);
//     } else if (x > 1.0) {
//       currentDirection = SensorDirection.right;
//       scrollList(currentDirection);
//     } else if (y < -1.0) {
//       currentDirection = SensorDirection.bottom;
//       scrollList(currentDirection);
//     } else if (y > 1.0) {
//       currentDirection = SensorDirection.top;
//       scrollList(currentDirection);
//     } else {
//       currentDirection = SensorDirection.center;
//     }
//   }
//
//   void scrollList(SensorDirection direction) {
//     switch (direction) {
//       case SensorDirection.left:
//       // scrollController.jumpTo(scrollController.offset - 50.0);
//         break;
//       case SensorDirection.right:
//       // scrollController.jumpTo(scrollController.offset + 50.0);
//         break;
//       case SensorDirection.top:
//         if (scrollController.offset > 0) {
//           scrollController.jumpTo(scrollController.offset - 50);
//         }
//         break;
//       case SensorDirection.bottom:
//         if (scrollController.offset < 4350) {
//           scrollController.jumpTo(scrollController.offset + 50);
//         }
//         break;
//       case SensorDirection.center:
//       // Do nothing for center
//         break;
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     // print(scrollController.offset);
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Automatic Scroll List'),
//       ),
//       body: ListView.builder(
//         controller: scrollController,
//         itemCount: 50,
//         itemBuilder: (context, index) {
//           return SizedBox(
//             height: 100, // Set the desired height here
//             child: ListTile(
//               tileColor: index % 2 == 0 ? Colors.red : Colors.green,
//               // Background color of the ListTile
//               title: Text('item $index'),
//             ),
//           );
//         },
//       ),
//     );
//   }
//
//   @override
//   void dispose() {
//     scrollController.dispose();
//     super.dispose();
//   }
// }
//
// class AdjustableScrollController extends ScrollController {
//   AdjustableScrollController([int extraScrollSpeed = 40]) {
//     super.addListener(() {
//       ScrollDirection scrollDirection = super.position.userScrollDirection;
//       if (scrollDirection != ScrollDirection.idle) {
//         double scrollEnd = super.offset +
//             (scrollDirection == ScrollDirection.reverse
//                 ? extraScrollSpeed
//                 : -extraScrollSpeed);
//         scrollEnd = min(super.position.maxScrollExtent,
//             max(super.position.minScrollExtent, scrollEnd));
//         jumpTo(scrollEnd);
//       }
//     });
//   }
// }


